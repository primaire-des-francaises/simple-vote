module.exports = {
  type: 'better-sqlite3',
  database: './data/simple-vote.db',
  entities: ['dist/**/**.entity{.ts,.js}'],

  // not sure we want this or not :-/
  // subscribers are loaded by Nest App ex: ScheduleVotePollsSubscriber
  subscribers: ['dist/**/**.subscriber{.ts,.js}'],

  migrations: ['migrations/*.ts'],
  cli: {
    migrationsDir: 'migrations',
  },
  // prepareDatabase: async (db) => {
  //   // TODO: should we use migrations for that?
  //   // console.log(db);
  //   /**
  //    * Database {
  //    *  name: '/home/antony/projects/voting/simple-vote/./data/simple-vote.db',
  //    *  open: true,
  //    *  inTransaction: false,
  //    *  readonly: false,
  //    *  memory: false
  //    * }
  //    */
  //   // PRAGMA schema.auto_vacuum = 0 | NONE | 1 | FULL | 2 | INCREMENTAL;
  //   // await db.query('PRAGMA main.incremental_vacuum(1)');
  //   // await db.query('PRAGMA main.auto_vacuum = INCREMENTAL;');
  // },
  synchronize: true,
  dropSchema: true,
  // logging: true,
};
