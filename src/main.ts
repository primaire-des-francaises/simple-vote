import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { seeder } from './seeder';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    cors: {
      // origin: 'https://web.example.org',
      origin: '*',

      // doit être activée pour que l'appel utilise les cookies
      // credentials: true,
    },
  });

  app.useGlobalPipes(
    new ValidationPipe({ whitelist: true, forbidNonWhitelisted: true }),
  );

  const swaggerOptions = new DocumentBuilder()
    .setTitle('Simple Vote API')
    .setContact(
      'Simple Vote API',
      'https://gitlab.com/primaire-des-francaises/simple-vote',
      'admin@article3.net',
    )
    .setDescription('API for a voting platform')
    .setVersion(process.env.npm_package_version)
    .addTag('Simple Vote')
    .addBearerAuth(
      { type: 'http', scheme: 'bearer', bearerFormat: 'JWT' },
      'access-token',
    )
    .build();

  const swaggerDocument = SwaggerModule.createDocument(app, swaggerOptions);
  SwaggerModule.setup('api', app, swaggerDocument);

  await app.listen(3000);

  console.log(
    'Swagger loaded: try out the api here http://localhost:3000/api/',
  );

  try {
    await seeder(app);
  } catch (error) {
    console.error('seeder', error);
  }
}
bootstrap();
