import { INestApplication } from '@nestjs/common';
import { CrudRequest } from '@nestjsx/crud';
import { RequestQueryBuilder, RequestQueryParser } from '@nestjsx/crud-request';
import { time } from 'console';
import { AuthRequestUser } from 'src/auth/interfaces/auth-request.interface';
import { UserEntity } from 'src/users/entities/user.entity';
import { UsersService } from 'src/users/users.service';
import { VotePollFptp } from 'src/vote-poll-fptps/entities/vote-poll-fptp.entity';
import { VotePollFptpsService } from 'src/vote-poll-fptps/vote-poll-fptps.service';
import { Candidate } from 'src/vote/entities/candidate.entity';
import { VotePoll } from 'src/vote/entities/vote-poll.entity';
import { SchedulesService } from 'src/vote/schedules/schedules.service';
import { VoteService } from 'src/vote/vote.service';
import { Connection, getConnection } from 'typeorm';

function randomIntFromInterval(min, max) {
  // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min);
}

export const seeder = async (app: INestApplication) => {
  const conn: Connection = getConnection();

  const usersService = app.get(UsersService);
  const votePollFptpsService = app.get(VotePollFptpsService);
  const voteService = app.get(VoteService);

  const currentTimestamp = new Date();
  const now = () => new Date(currentTimestamp);

  // we only seed an empty db
  if (await usersService.findOne()) return;

  let demoUsers: UserEntity[] = [];
  const nbUsers = 15;

  let votePoll;

  const userVote = async ({
    user,
    votePoll,
    candidate,
  }: {
    user: UserEntity;
    votePoll: VotePoll;
    candidate: Candidate;
  }) => {
    return await voteService
      .registerVote(<AuthRequestUser>(<unknown>user), {
        votePollId: votePoll.id,
        candidateId: candidate.id,
      })
      .then(() => {
        console.log('Looks we can vote');
      })
      .catch((e) => {
        console.error('Error, could not register vote.', e);
      });
  };

  const createVotePoll = async ({
    user,
  }: {
    user: UserEntity;
  }): Promise<VotePoll> => {
    // create a open votePollFptp
    const query = conn.getRepository(VotePoll).create({
      createdByUser: user,
      title: 'Demo FPTP poll',
      // slug: 'demo-fptp-poll',
      publiclyVisibleAt: now().toISOString(),
      votingStartsAt: now().toISOString(),
      // votingEndsAt: new Date(
      //   now().setMonth(now().getMonth() + 1),
      // ).toISOString(),
      votingEndsAt: new Date(new Date().getTime() + 1000 * 3),
      participates: [
        {
          createdByUser: user,
          title: 'Démo participation A',
          // slug: 'demo-participation-a',
          candidate: {
            createdByUser: user,
            title: 'Démo candidate A',
            // slug: 'demo-candidate-a',
          },
        },
        {
          createdByUser: user,
          title: 'Démo participation B',
          slug: 'démo participation b',
          candidate: {
            createdByUser: user,
            title: 'Demo candidate B',
            // slug: 'demo-candidate-b',
          },
        },
        {
          createdByUser: user,
          title: 'Demo participation C',
          slug: 'demo-participation-c',
          candidate: {
            createdByUser: user,
            title: 'Demo candidate C',
            slug: 'demo-candidate-c',
          },
        },
      ],
    });

    return query.save();
  };

  for (var i = 0; i < nbUsers; i++) {
    const user = await usersService
      .register({
        username: `demo${i}`,
        password: 'user123',
      })
      .then(async (user) => {
        demoUsers.push(user);
        console.log(`Created user ${user.username} [${user.id}]`);

        if (user.username === 'demo0') {
          votePoll = await createVotePoll({ user }).then(async (votePoll) => {
            console.log(
              `Created VotePoll owned by ${votePoll.createdByUser.username}`,
            );

            return votePoll;
          });
        }
      });
  }

  for (const voter of demoUsers) {
    console.log(`userVote ${voter.username}`);
    await userVote({
      user: voter,
      votePoll: votePoll,
      candidate: votePoll.participates[randomIntFromInterval(0, 2)].candidate,
    });
  }

  // const qb = RequestQueryBuilder.create();

  // const query = RequestQueryBuilder.create({
  //   fields: ["name", "email"],
  //   search: { isActive: true },
  //   join: [{ field: "company" }],
  //   sort: [{ field: "id", order: "DESC" }],
  //   page: 1,
  //   limit: 25,
  //   resetCache: true
  // }).query();

  // create a vote poll that has ended

  /*
  const votePollFptpDone = await conn
    .getRepository(VotePollFptp)
    .create({
      votePoll: {
        createdByUser: demoUsers[1],
        title: 'Ended Demo FPTP poll',
        slug: 'ended-demo-fptp-poll',
        publiclyVisibleAt: new Date(
          now().setMonth(now().getMonth() - 3),
        ).toISOString(),
        votingStartsAt: new Date(
          now().setMonth(now().getMonth() - 2),
        ).toISOString(),
        votingEndsAt: new Date(
          now().setMonth(now().getMonth() - 1),
        ).toISOString(),
        votedCount: 362,
        ballotBoxBallotsCount: 362,
        ballotBoxBlankBallotsCount: 12,
        participates: [
          {
            createdByUser: demoUsers[1],
            title: 'Ended participation A',
            slug: 'ended-participation-a',
            votesCount: 200,
            candidate: {
              createdByUser: demoUsers[1],
              title: 'Ended candidate A',
              slug: 'ended-candidate-a',
            },
          },
          {
            createdByUser: demoUsers[1],
            title: 'Ended participation B',
            slug: 'ended-participation-b',
            votesCount: 100,
            candidate: {
              createdByUser: demoUsers[1],
              title: 'Ended candidate B',
              slug: 'ended-candidate-b',
            },
          },
          {
            createdByUser: demoUsers[1],
            title: 'Ended participation C',
            slug: 'ended-participation-c',
            votesCount: 50,
            candidate: {
              createdByUser: demoUsers[1],
              title: 'Ended candidate C',
              slug: 'ended-candidate-c',
            },
          },
        ],
      },
    })
    .save();

  // demo user votes for demo candidate b

  try {
    console.log('See if we can vote...');
    voteService
      .registerVote(<AuthRequestUser>(<unknown>demoUsers[1]), {
        votePollId: votePollFptpLive.votePollId,
        candidateId: votePollFptpLive.votePoll.participates[0].candidateId,
      })
      .then(() => {
        console.log('Looks we can vote');
      })
      .catch((e) => {
        console.error('Error, could not register vote.', e);
      });

    voteService
      .registerVote(<AuthRequestUser>(<unknown>demoUsers[1]), {
        votePollId: votePollFptpLive.votePollId,
        candidateId: votePollFptpLive.votePoll.participates[1].candidateId,
      })
      .then(() => {
        console.warn('OOOOUUUUUPS!!!! we could vote twice ??!!!');
      })
      .catch((e) => {
        console.log(
          `Good, we can't vote second time, even in async/simultaneous vote (${e.message})`,
        );
      })
      .finally(async () => {
        await voteService
          .registerVote(<AuthRequestUser>(<unknown>demoUsers[1]), {
            votePollId: votePollFptpLive.votePollId,
            candidateId: votePollFptpLive.votePoll.participates[1].candidateId,
          })
          .then(() => {
            console.warn('OOOOUUUUUPS!!!! we could vote twice ??!!! (3)');
          })
          .catch((e) => {
            console.log(`Good, we can't vote at later time (${e.message})`);
          });
      });
  } catch (error) {
    console.error(error);
  }
  */
};
