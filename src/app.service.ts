import { Injectable } from '@nestjs/common';
import { LocalAuthGuard } from './auth/local-auth.guard';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }
}
