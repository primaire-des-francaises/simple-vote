import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from './auth.service';
import { JWT_SECRET } from 'src/jwtconfig';
import {
  AuthJwtPayload,
  AuthRequestUser,
} from './interfaces/auth-request.interface';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      // ignoreExpiration: false,
      secretOrKey: JWT_SECRET,
    });
  }

  async validate(
    jwtPayload: AuthJwtPayload,
    done: Function,
  ): Promise<AuthRequestUser> {
    // here we can fill up req.user from a db lookup

    const user = await this.authService.validateUserToken(jwtPayload);

    if (user) {
      const userReqWithJwtPayload: AuthRequestUser = {
        id: user.id,
        username: user.username,
        email: user.email,
        role: user.role,
        // isActive: user.isActive,
        jwtPayload,
      };

      return userReqWithJwtPayload;
    }

    throw new UnauthorizedException();
  }
}
