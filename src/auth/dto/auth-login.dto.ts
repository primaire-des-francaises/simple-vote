import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsDefined, IsNotEmpty, IsOptional, Max, Min } from 'class-validator';
import {
  authExpiresInDefault,
  authExpiresInMaximum,
} from '../auth-tokens/dto/user-auth-token.dto';

export class AuthLoginDto {
  @ApiProperty()
  @IsDefined()
  @IsNotEmpty()
  readonly username!: string;

  @ApiProperty()
  @IsDefined()
  @IsNotEmpty()
  readonly password!: string;

  @ApiPropertyOptional({
    description: `maximum value ${authExpiresInMaximum}s or null for non expire (aka permanent)`,
    minimum: 0,
    maximum: authExpiresInMaximum,
    default: authExpiresInDefault,
  })
  @IsOptional()
  @Min(0)
  @Max(authExpiresInMaximum)
  readonly expiresIn?: number;
}
