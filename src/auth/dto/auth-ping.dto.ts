import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsInt, IsOptional, Max, Min } from 'class-validator';
import {
  authExpiresInDefault,
  authExpiresInMaximum,
} from '../auth-tokens/dto/user-auth-token.dto';

export class AuthPingDto {
  @ApiPropertyOptional({
    minimum: -1,
    maximum: authExpiresInMaximum,
    default: authExpiresInDefault,
    nullable: true,
  })
  @IsOptional()
  @IsInt()
  @Min(-1)
  @Max(authExpiresInMaximum)
  readonly expiresIn?: number;
}
