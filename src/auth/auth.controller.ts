import {
  Controller,
  UseGuards,
  HttpStatus,
  Response,
  Request,
  Post,
  Body,
} from '@nestjs/common';
import { ApiBasicAuth, ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { UsersService } from 'src/users/users.service';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { LocalAuthGuard } from './local-auth.guard';
import { RealIP } from 'nestjs-real-ip';
import { AuthLoginDto } from './dto/auth-login.dto';
import { AccessToken } from './interfaces/access-token.interface';
import { JwtAuthGuard } from './jwt-auth.guard';
import { AuthPingDto } from './dto/auth-ping.dto';
import { authExpiresInDefault } from './auth-tokens/dto/user-auth-token.dto';
import { AuthRequestUser } from './interfaces/auth-request.interface';
import { UserReq } from './user-req.decorator';
import { AuthRegisterDto } from './dto/auth-register';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService, // private readonly usersService: UsersService,
  ) {}

  @Post('register')
  public async register(
    @Response() res,
    @Body() registerUser: AuthRegisterDto,
  ) {
    const result = await this.authService.register(registerUser);

    if (!result.success) {
      return res.status(HttpStatus.BAD_REQUEST).json(result);
    }

    return res.status(HttpStatus.OK).json(result);
  }

  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(
    @Body() loginDto: AuthLoginDto,
    @Request() req,
    @RealIP() ip: string,
  ): Promise<AccessToken> {
    // null means permanent, but undefined should default to 1200
    const expiresIn =
      loginDto.expiresIn === undefined
        ? authExpiresInDefault
        : loginDto.expiresIn === null
        ? null
        : loginDto.expiresIn;

    return await this.authService.login(
      req.user,
      ip,
      req.headers['user-agent'],
      expiresIn,
    );
  }

  @ApiBearerAuth('access-token')
  @UseGuards(JwtAuthGuard)
  @Post('ping')
  async extendTokenValidity(
    @Body() authPing: AuthPingDto,
    @UserReq() reqUser: AuthRequestUser,
    @Request() req,
  ) {
    return await this.authService.extendTokenValidity(
      reqUser.jwtPayload,
      reqUser.id,
      req.headers['user-agent'],
      authPing.expiresIn,
    );
  }

  @ApiBearerAuth('access-token')
  @UseGuards(JwtAuthGuard)
  @Post('logout')
  async logout(@UserReq() reqUser: AuthRequestUser) {
    return await this.authService.logout(reqUser.jwtPayload, reqUser.id);
  }
}
