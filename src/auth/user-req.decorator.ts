import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { AuthRequestUser } from 'src/auth/interfaces/auth-request.interface';

export const UserReq = createParamDecorator(
  (data: unknown, ctx: ExecutionContext): AuthRequestUser => {
    const request = ctx.switchToHttp().getRequest();
    return request.user;
  },
);
