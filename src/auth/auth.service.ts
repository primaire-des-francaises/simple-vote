import * as jwt from 'jsonwebtoken';
import {
  Injectable,
  Logger,
  NotAcceptableException,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';
import { UserEntity } from 'src/users/entities/user.entity';
import { AuthRegistrationStatus } from './interfaces/auth-registration-status.interface';
import { AccessToken } from './interfaces/access-token.interface';
import {
  AuthJwtPayload,
  AuthRequestUser,
} from './interfaces/auth-request.interface';
import { AuthTokensService } from './auth-tokens/auth-tokens.service';
import * as UAParser from 'ua-parser-js';
import { authExpiresInDefault } from './auth-tokens/dto/user-auth-token.dto';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { AuthRegisterDto } from './dto/auth-register';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly authTokensService: AuthTokensService,
    private readonly jwtService: JwtService,
  ) {}

  private readonly logger = new Logger(AuthService.name);

  async validateUser(
    username: string,
    password: string,
  ): Promise<Partial<AuthRequestUser>> {
    const user = await this.usersService.findOne(
      {
        username,
        isActive: true,
      },
      {
        select: ['id', 'username', 'password', 'role'],
      },
    );

    if (user && (await user.comparePassword(password))) {
      const { password, ...result } = user;
      return result;
    }

    return null;
  }

  async validateUserToken(jwtPayload: AuthJwtPayload): Promise<UserEntity> {
    // here we could check for revoked tokens based on jwtPayload.sub

    const authToken = await this.authTokensService.validateUserToken(
      jwtPayload,
    );

    // const user = await this.usersService.findOne({ id: authToken.userId });

    if (authToken) {
      return authToken.user;
    }

    return null;
  }

  async login(
    user: UserEntity,
    ip: string,
    httpUserAgent: string,
    expiresIn: number = authExpiresInDefault, // null = never expire
  ): Promise<AccessToken> {
    // normalize user agent
    const userAgent: UAParser.IResult = UAParser(httpUserAgent);

    // TODO: perhaps we want to store only some info, not all
    // {
    //   deviceModel: r.device.model,
    //   osName: r.os.name,
    // };

    // const expiresAt =
    //   expiresIn === null
    //     ? null
    //     : new Date(new Date().getTime() + expiresIn * 1000);

    const authToken = await this.authTokensService.createAuthToken({
      userId: user.id,
      userAgent,
      createdFromIp: ip,
      expiresIn,
    });

    const tokenId = authToken.identifiers[0].id;
    const jwtPayload: AuthJwtPayload = { sub: tokenId };

    // so we can extend token validity using database we don't want those tokens to expire on JWT side
    // const signOptions = expiresIn ? { expiresIn } : {};
    const accessToken = this.jwtService.sign(jwtPayload);

    const createdAt = new Date();

    return {
      accessToken,
      expiresIn,
      createdAt,
      expiresAt: authToken.generatedMaps[0].expiresAt,
    };
  }

  async register(user: AuthRegisterDto) {
    let status: AuthRegistrationStatus;

    try {
      const newUser = await this.usersService.register(user);

      status = {
        success: true,
        message: `User '${newUser.username}' registered.`,
      };
    } catch (err) {
      status = { success: false, message: err.message };
    }

    return status;
  }

  async logout(jwtPayload: AuthJwtPayload, userId: string) {
    await this.authTokensService.removeAuthToken(jwtPayload, userId);

    return {
      success: true,
      message: 'Logged out',
    };
  }

  async extendTokenValidity(
    jwtPayload: AuthJwtPayload,
    userId: string,
    httpUserAgent: string,
    expiresIn?: number,
  ) {
    const userAgent: UAParser.IResult = UAParser(httpUserAgent);

    if (expiresIn !== undefined && expiresIn <= 0) {
      return await this.logout(jwtPayload, userId);
    }

    const authToken = await this.authTokensService.extendValidity(
      jwtPayload,
      userId,
      userAgent,
      expiresIn,
    );

    if (authToken) {
      return {
        success: true,
        message: 'Updated authToken expiration.',
        updatedAt: authToken.updatedAt,
        expiresAt: authToken.expiresAt,
      };
    }
  }
}
