import { IsDefined, IsInt, Max, Min } from 'class-validator';
import { authExpiresInMaximum, AuthTokenDto } from './user-auth-token.dto';

export class UpdateAuthTokenDto extends AuthTokenDto {
  @IsInt()
  @Min(-1)
  @Max(authExpiresInMaximum)
  expiresIn!: number;
}
