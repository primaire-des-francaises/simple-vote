import { Type } from 'class-transformer';
import {
  IsDefined,
  IsInt,
  IsIP,
  IsNotEmpty,
  IsOptional,
  IsUUID,
  Max,
  Min,
} from 'class-validator';
import { authExpiresInMaximum } from './user-auth-token.dto';

export class CreateAuthTokenDto {
  @IsDefined()
  @IsNotEmpty()
  @IsUUID()
  userId!: string;

  @IsDefined()
  @IsNotEmpty()
  userAgent!: Object;

  @IsDefined()
  @IsNotEmpty()
  @IsIP()
  createdFromIp!: string;

  @IsOptional()
  @IsInt()
  @Min(0)
  @Max(authExpiresInMaximum)
  expiresIn?: number | null;
}
