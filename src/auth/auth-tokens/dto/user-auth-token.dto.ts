import { Optional } from '@nestjs/common';
import {
  IsDefined,
  IsInt,
  IsNotEmpty,
  IsUUID,
  Max,
  Min,
} from 'class-validator';

export const authExpiresInDefault = 1200; // 20min.
export const authExpiresInMaximum = 3600; // 1h

export class AuthTokenDto {
  @IsDefined()
  @IsNotEmpty()
  @IsUUID()
  id!: string;

  @Optional()
  @IsUUID()
  userId?: string;

  @Optional()
  @IsInt()
  @Min(0)
  @Max(authExpiresInMaximum)
  expiresIn?: number;
}
