import {
  Injectable,
  NotAcceptableException,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateAuthTokenDto } from './dto/create-auth-token.dto';
import { UpdateAuthTokenDto } from './dto/update-auth-token.dto';
import {
  authExpiresInDefault,
  authExpiresInMaximum,
} from './dto/user-auth-token.dto';
import { isMatch } from 'lodash';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import { Crud, CrudRequest } from '@nestjsx/crud';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';

import { AuthToken } from './entities/auth-token.entity';
import { AuthJwtPayload } from '../interfaces/auth-request.interface';

@Injectable()
@Crud({
  model: {
    type: AuthToken,
  },
  dto: {
    create: CreateAuthTokenDto,
    update: UpdateAuthTokenDto,
  },
})
export class AuthTokensService extends TypeOrmCrudService<AuthToken> {
  constructor(
    @InjectRepository(AuthToken)
    readonly repo: Repository<AuthToken>,
  ) {
    super(repo);
  }

  async createAuthToken(createAuthToken: CreateAuthTokenDto) {
    createAuthToken = plainToClass(CreateAuthTokenDto, createAuthToken);

    const validateErrors = await validate(createAuthToken, {
      whitelist: true,
      stopAtFirstError: true,
      forbidNonWhitelisted: true,
    });

    if (validateErrors.length) {
      throw new NotAcceptableException();
    }

    let insertAuthToken: AuthToken = <AuthToken>{ ...createAuthToken };

    if (createAuthToken.expiresIn !== null) {
      const expiresIn = Math.min(
        authExpiresInMaximum,
        Math.max(createAuthToken.expiresIn, 0),
      );

      const expiresAt = new Date(new Date().getTime() + expiresIn * 1000);

      insertAuthToken.expiresAt = expiresAt;
    }

    return await this.repo.insert(insertAuthToken);
  }

  async validateUserToken(jwtPayload: AuthJwtPayload, userId?: string) {
    const validUserToken = this.repo
      .createQueryBuilder('authToken')
      .select()
      .where({
        id: jwtPayload.sub,
      })
      .andWhere(
        '( authToken.expiresAt IS NULL OR authToken.expiresAt >= CURRENT_TIMESTAMP )',
      )
      .innerJoinAndSelect('authToken.user', 'user')
      .andWhere('user.isActive');

    if (userId) {
      validUserToken.andWhere({ userId });
    }

    const result = await validUserToken.getOne();

    return result;
  }

  async extendValidity(
    jwtPayload: AuthJwtPayload,
    userId: string,
    userAgent?: object,
    expiresIn?: number,
  ) {
    let expiresAt = null;

    if (expiresIn !== null) {
      // null => permanent token
      // if not specified, then use default value
      expiresIn = expiresIn === undefined ? authExpiresInDefault : expiresIn;

      // between 0 and authMaxExpiresIn
      expiresIn = Math.max(Math.min(expiresIn, authExpiresInMaximum), 0);

      expiresAt = new Date(new Date().getTime() + expiresIn * 1000);
    }

    let token = await this.validateUserToken(jwtPayload, userId);

    if (!token) {
      throw new UnauthorizedException();
    }

    // this is a logout (-1 should work too)
    // and here one can logout even if userAgent changed.
    if (expiresIn === 0) {
      return await this.removeAuthToken(jwtPayload, userId);
    }

    // if specified, we confirm it's from same userAgent
    if (userAgent && !isMatch(userAgent, token.userAgent)) {
      throw new UnauthorizedException('UserAgent not match');
    }

    token.expiresAt = expiresAt;

    return await token.save();
  }

  async removeAuthToken(jwtPayload: AuthJwtPayload, userId: string) {
    const token = await this.validateUserToken(jwtPayload, userId);

    return this.repo.remove(token);
  }

  async deleteOne(crudRequest: CrudRequest) {
    const myEntity = await this.getOneOrFail(crudRequest);
    return this.repo.remove(myEntity);
  }

  // update(id, updateAuthTokenDto: UpdateAuthTokenDto) {
  //   return `This action updates a #${id} authToken`;
  // }
  // findAll() {
  //   return `This action returns all authTokens`;
  // }

  // findOne(id: string) {
  //   return `This action returns single token authToken id:${id}`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} authToken`;
  // }
}
