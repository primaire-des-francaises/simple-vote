import { Controller, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import {
  Crud,
  CrudAuth,
  CrudController,
  CrudRequest,
  Feature,
  Override,
  ParsedRequest,
} from '@nestjsx/crud';
import { SCondition } from '@nestjsx/crud-request';
import { AppGuards } from 'src/app.guards';
import { isAdmin } from 'src/shared/utils';
import { AuthRequestUser } from '../interfaces/auth-request.interface';
import { AuthTokensService } from './auth-tokens.service';
import { CreateAuthTokenDto } from './dto/create-auth-token.dto';
import { UpdateAuthTokenDto } from './dto/update-auth-token.dto';
import { AuthToken } from './entities/auth-token.entity';

@ApiTags('auth')
@ApiBearerAuth('access-token')
@Feature('auth/tokens')
@UseGuards(...AppGuards)
@Crud({
  model: {
    type: AuthToken,
  },
  params: {
    id: {
      field: 'id',
      type: 'uuid',
      primary: true,
    },
  },
  dto: {
    create: CreateAuthTokenDto,
    update: UpdateAuthTokenDto,
    // replace: UpdateAuthTokenDto,
  },
  routes: {
    // only: ["getOneBase", "updateOneBase"],
    exclude: ['createManyBase'],
  },
})
@CrudAuth({
  property: 'user',
  filter: (user: AuthRequestUser): SCondition => {
    /**
     * only super admins can see auth tokens of others
     */
    if (isAdmin(user, true)) {
      return;
    }

    return {
      userId: user.id,
    };
  },
})
@Controller('auth/tokens')
export class AuthTokensController implements CrudController<AuthToken> {
  constructor(public service: AuthTokensService) {}

  get base(): CrudController<AuthToken> {
    return this;
  }

  // @Override('deleteOneBase')
  // deleteOne(@ParsedRequest() req: CrudRequest) {
  //   return this.base.deleteOneBase(req);
  // }

  // @Post()
  // create(@Body() createAuthTokenDto: CreateAuthTokenDto) {
  //   return this.authTokensService.create(createAuthTokenDto);
  // }

  // @Get()
  // findAll() {
  //   return this.authTokensService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.authTokensService.findOne(id, requestUser.userId);
  // }

  // @Patch(':id')
  // update(
  //   @Param('id') id: string,
  //   @Body() updateAuthTokenDto: UpdateAuthTokenDto,
  // ) {
  //   return this.authTokensService.update(+id, updateAuthTokenDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.authTokensService.remove(+id);
  // }
}
