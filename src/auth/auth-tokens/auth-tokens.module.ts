import { Module } from '@nestjs/common';
import { AuthTokensService } from './auth-tokens.service';
import { AuthTokensController } from './auth-tokens.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthToken } from './entities/auth-token.entity';

@Module({
  controllers: [AuthTokensController],
  providers: [AuthTokensService],
  imports: [TypeOrmModule.forFeature([AuthToken])],
  exports: [AuthTokensService],
})
export class AuthTokensModule {}
