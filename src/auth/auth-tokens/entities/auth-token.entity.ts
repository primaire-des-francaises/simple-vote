import { Type } from 'class-transformer';
import { IsUUID, IsDefined, IsOptional, IsIP } from 'class-validator';
import { DbAwareColumn } from 'src/shared/db-aware-column';
import { TimestampedEntity } from 'src/shared/extended-base-entities';
import { UserEntity } from 'src/users/entities/user.entity';
import {
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
  Entity,
  Column,
} from 'typeorm';

@Entity({ name: 'auth_token', withoutRowid: true })
export class AuthToken extends TimestampedEntity {
  @PrimaryGeneratedColumn('uuid')
  @IsUUID()
  id: string;

  @IsDefined()
  @IsUUID()
  @Column()
  userId!: string;

  @IsDefined()
  @DbAwareColumn({
    type: 'json',
  })
  userAgent: Object;

  @IsDefined()
  @IsIP()
  @Column()
  createdFromIp: string;

  @IsOptional()
  @DbAwareColumn({
    type: 'timestamptz',
    nullable: true,
    default: null,
  })
  @Type(() => Date)
  expiresAt?: Date;

  @ManyToOne(() => UserEntity, (user) => user.authTokens, {
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'userId', referencedColumnName: 'id' })
  user: UserEntity;
}
