import { Type } from 'class-transformer';

export class AccessToken {
  readonly accessToken: string;

  readonly expiresIn: number;

  @Type(() => Date)
  readonly createdAt: Date;

  @Type(() => Date)
  readonly expiresAt: Date;
}
