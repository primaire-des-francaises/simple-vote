import { ApiProperty, ApiResponseProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsDefined, IsNotEmpty, IsUUID } from 'class-validator';
import { JwtPayload } from 'jsonwebtoken';
import { AppRoles } from 'src/app.roles';

// Transform((o)=> o)
export class AuthJwtPayload implements JwtPayload {
  @ApiProperty({
    type: 'string',
    format: 'uuid',
    description: `authTokenId`,
  })
  @IsDefined()
  @IsNotEmpty()
  @IsUUID()
  sub: string;
  // constructor ( ) {
  //   Object.keys(this).forEach(p => {this[p] = {@ApiProperty()this[p]}})
  // }
}

/**
 * Defining User found in req.user
 */
export class AuthRequestUser {
  @ApiProperty({
    type: 'string',
    format: 'uuid',
    description: 'userId',
  })
  id: string;

  @ApiProperty({
    type: 'string',
  })
  username: string;

  @ApiProperty({
    type: 'string',
    format: 'email',
    required: false,
    nullable: true,
  })
  email?: string;

  @ApiProperty({
    type: 'enum',
    enum: AppRoles,
    default: AppRoles.USER,
  })
  role: AppRoles;

  @ApiResponseProperty({
    type: AuthJwtPayload,
  })
  jwtPayload: AuthJwtPayload;
}

// export class AuthRequestUserWithJwtPayload extends AuthRequestUser {
//   @ApiResponseProperty({
//     type: () => AuthJwtPayload,
//     // required: true,
//     // type: () => { return class JwtPayload implements JwtPayload; },
//   })
//   jwtPayload: AuthJwtPayload;
// }

export interface AuthRequest extends Request {}
export class AuthRequest {
  @ApiProperty({
    type: AuthRequestUser,
  })
  user: AuthRequestUser;
}
