export interface AuthRegistrationStatus {
  success: boolean;
  message: string;
}
