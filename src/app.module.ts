import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AccessControlModule } from 'nest-access-control';
import { ConfigModule, ConfigModuleOptions } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { appRoles } from './app.roles';
import { VoteModule } from './vote/vote.module';
import { PublicModule } from './public/public.module';
import { CertTokensModule } from './cert/cert-tokens/cert-tokens.module';
import { SchedulesModule } from './vote/schedules/schedules.module';
import { CertModule } from './cert/cert.module';
import { LocksModule } from './locks/locks.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ['.env', '.env.development.local', '.env.development'],
      isGlobal: true,
    }),
    // Object.assign(
    //   <ConfigModuleOptions>{
    //     // envFilePath: ['.env.development.local', '.env.development'],
    //     // ignoreEnvFile: true, // to only use OS shell exports like export DATABASE_USER=test
    //   },
    //   ormconfig,
    // ),
    TypeOrmModule.forRoot(),
    UsersModule,
    AuthModule,
    AccessControlModule.forRoles(appRoles),
    VoteModule,
    PublicModule,
    CertTokensModule,
    SchedulesModule,
    CertModule,
    LocksModule,
  ],
  controllers: [AppController],
  providers: [AppService],
  exports: [],
})
export class AppModule {}
