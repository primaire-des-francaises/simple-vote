import { Module } from '@nestjs/common';
import { CertTokensService } from './cert-tokens.service';
import { CertTokensController } from './cert-tokens.controller';
import { CertToken } from '../entities/cert-token.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  controllers: [CertTokensController],
  providers: [CertTokensService],
  imports: [TypeOrmModule.forFeature([CertToken])],
})
export class CertTokensModule {}
