import { PartialType } from '@nestjs/swagger';
import { CreateCertTokenDto } from './create-cert-token.dto';

export class UpdateCertTokenDto extends PartialType(CreateCertTokenDto) {}
