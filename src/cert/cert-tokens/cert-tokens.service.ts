import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { UserEntity } from 'src/users/entities/user.entity';
import { Repository } from 'typeorm';
import { CreateCertTokenDto } from './dto/create-cert-token.dto';
import { UpdateCertTokenDto } from './dto/update-cert-token.dto';
import { CertToken } from '../entities/cert-token.entity';

@Injectable()
export class CertTokensService extends TypeOrmCrudService<CertToken> {
  constructor(
    @InjectRepository(CertToken)
    readonly repo: Repository<CertToken>,
  ) {
    super(repo);
  }

  // create(createCertTokenDto: CreateCertTokenDto) {
  //   return 'This action adds a new certToken';
  // }

  // findAll() {
  //   return `This action returns all certTokens`;
  // }

  // findOne(id: number) {
  //   return `This action returns a #${id} certToken`;
  // }

  // update(id: number, updateCertTokenDto: UpdateCertTokenDto) {
  //   return `This action updates a #${id} certToken`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} certToken`;
  // }
}
