import { Test, TestingModule } from '@nestjs/testing';
import { CertTokensController } from './cert-tokens.controller';
import { CertTokensService } from './cert-tokens.service';

describe('CertTokensController', () => {
  let controller: CertTokensController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CertTokensController],
      providers: [CertTokensService],
    }).compile();

    controller = module.get<CertTokensController>(CertTokensController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
