import { Test, TestingModule } from '@nestjs/testing';
import { CertTokensService } from './cert-tokens.service';

describe('CertTokensService', () => {
  let service: CertTokensService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CertTokensService],
    }).compile();

    service = module.get<CertTokensService>(CertTokensService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
