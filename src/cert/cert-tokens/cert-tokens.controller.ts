import { Controller, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Crud, CrudAuth, CrudController, Feature } from '@nestjsx/crud';
import { SCondition } from '@nestjsx/crud-request';
import { AppGuards } from 'src/app.guards';
import { AuthRequestUser } from 'src/auth/interfaces/auth-request.interface';
import { isAdmin } from 'src/shared/utils';
import { CertTokensService } from './cert-tokens.service';
import { CreateCertTokenDto } from './dto/create-cert-token.dto';
import { UpdateCertTokenDto } from './dto/update-cert-token.dto';
import { CertToken } from '../entities/cert-token.entity';

@ApiTags('cert')
@Feature('cert-tokens')
@Crud({
  model: {
    type: CertToken,
  },
  params: {
    id: {
      field: 'id',
      type: 'uuid',
      primary: true,
    },
  },
  query: {
    // exclude: ['password'],
  },
  dto: {
    create: CreateCertTokenDto,
    update: UpdateCertTokenDto,
    // replace: CreateCertTokenDto,
  },
  routes: {},
})
@CrudAuth({
  property: 'user',
  filter: (user: AuthRequestUser): SCondition => {
    if (isAdmin(user)) {
      return;
    }

    return;

    // return {
    //   userId: user.id,
    // };
  },
})
@ApiBearerAuth('access-token')
@UseGuards(...AppGuards)
@Controller('cert/tokens')
export class CertTokensController implements CrudController<CertToken> {
  constructor(public readonly service: CertTokensService) {}

  get base(): CrudController<CertToken> {
    return this;
  }

  // @Post()
  // create(@Body() createCertTokenDto: CreateCertTokenDto) {
  //   return this.certTokensService.create(createCertTokenDto);
  // }

  // @Get()
  // findAll() {
  //   return this.certTokensService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.certTokensService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateCertTokenDto: UpdateCertTokenDto) {
  //   return this.certTokensService.update(+id, updateCertTokenDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.certTokensService.remove(+id);
  // }
}
