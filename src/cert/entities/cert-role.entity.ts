import { ApiProperty } from '@nestjs/swagger';
import { DbAwareColumn } from 'src/shared/db-aware-column';
import { TimestampedEntity } from 'src/shared/extended-base-entities';
import { UserEntity } from 'src/users/entities/user.entity';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { CertToken } from './cert-token.entity';

export enum EnumCertRole {
  owner = 'owner',
  administrator = 'administrator',
  certifier = 'certifier',
  none = 'none',
}

@Entity({ name: 'cert_role', withoutRowid: true })
export class CertRole extends TimestampedEntity {
  @Column({
    type: 'uuid',
    primary: true,
  })
  userId: string;

  @Column({
    type: 'uuid',
    primary: true,
  })
  certTokenId: string;

  @ApiProperty({
    type: 'string',
    format: 'uuid',
    readOnly: true,
    required: false,
  })
  @Column({
    type: 'uuid',
  })
  createdByUserId: string;

  @ManyToOne(() => UserEntity, (user) => user.createdCertRoles, {
    onUpdate: 'CASCADE',
    onDelete: 'NO ACTION',
  })
  @JoinColumn({ name: 'createdByUserId' })
  createdByUser: UserEntity;

  @DbAwareColumn({
    type: 'enum',
    enum: EnumCertRole,
    default: EnumCertRole.none,
  })
  role: EnumCertRole;

  @ManyToOne(() => UserEntity, (user) => user.certifications, {
    createForeignKeyConstraints: false,
  })
  @JoinColumn({ name: 'userId' })
  user: UserEntity;

  @ManyToOne(() => CertToken, (certToken) => certToken.certifications, {
    createForeignKeyConstraints: false,
  })
  @JoinColumn({ name: 'userId' })
  certToken: CertToken;
}
