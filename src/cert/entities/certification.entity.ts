import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsOptional, IsUUID } from 'class-validator';
import { DbAwareColumn } from 'src/shared/db-aware-column';
import { TimestampedEntity } from 'src/shared/extended-base-entities';
import { UserEntity } from 'src/users/entities/user.entity';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { CertToken } from './cert-token.entity';

@Entity({ name: 'certification', withoutRowid: true })
export class Certification extends TimestampedEntity {
  @Column({
    type: 'uuid',
    primary: true,
  })
  @IsUUID()
  userId: string;

  @Column({
    type: 'uuid',
    primary: true,
  })
  @IsUUID()
  certTokenId: string;

  @ApiProperty({
    type: 'string',
    format: 'uuid',
    readOnly: true,
    required: false,
  })
  @Column({
    type: 'uuid',
  })
  createdByUserId: string;

  @ManyToOne(() => UserEntity, (user) => user.createdCertifications, {
    onUpdate: 'CASCADE',
    onDelete: 'NO ACTION',
  })
  @JoinColumn({ name: 'createdByUserId' })
  createdByUser: UserEntity;

  @IsOptional()
  @DbAwareColumn({
    type: 'timestamptz',
    nullable: true,
    default: null,
  })
  @Type(() => Date)
  expiresAt?: Date;

  @ManyToOne(() => UserEntity, (user) => user.certifications, {
    createForeignKeyConstraints: false,
  })
  @JoinColumn({ name: 'userId' })
  user: UserEntity;

  @ManyToOne(() => CertToken, (certToken) => certToken.certifications, {
    createForeignKeyConstraints: false,
  })
  @JoinColumn({ name: 'userId' })
  certToken: CertToken;
}
