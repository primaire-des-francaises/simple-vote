import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Type } from 'class-transformer';
import {
  IsUUID,
  IsDefined,
  IsOptional,
  IsIP,
  IsBoolean,
} from 'class-validator';
import { DbAwareColumn } from 'src/shared/db-aware-column';
import {
  SoftDeleteTimestampedEntity,
  TimestampedEntity,
} from 'src/shared/extended-base-entities';
import { UserEntity } from 'src/users/entities/user.entity';
import { VotePollCert } from 'src/vote/entities/vote-poll-cert.entity';
import { VotePoll } from 'src/vote/entities/vote-poll.entity';
import {
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
  Entity,
  Column,
  ManyToMany,
  JoinTable,
  Tree,
  OneToMany,
} from 'typeorm';
import { CertRole } from './cert-role.entity';
import { Certification } from './certification.entity';

@Tree('closure-table', {
  closureTableName: 'cert_token',
  ancestorColumnName: () => 'ancestorCertTokenId',
  descendantColumnName: () => 'descendantCertTokenId',
  // ancestorColumnName: (column) => 'ancestor_' + column.propertyName,
  // descendantColumnName: (column) => 'descendant_' + column.propertyName,
})
@Entity({ name: 'cert_token', withoutRowid: true })
export class CertToken extends SoftDeleteTimestampedEntity {
  @PrimaryGeneratedColumn('uuid')
  @IsUUID()
  id: string;

  @IsOptional()
  @DbAwareColumn({
    type: 'timestamptz',
    nullable: true,
    default: null,
  })
  @Type(() => Date)
  expiresAt?: Date;

  @IsOptional()
  @Column({
    type: 'int',
    unsigned: true,
    nullable: true,
    default: 1,
  })
  usableCount?: number;

  /**
   * Here we could use :serverSecret: in the salt
   */
  @Exclude()
  @Column({
    select: false,
  })
  salt: string;

  /**
   * Can this CertToken (and related Certificate) be removed
   */
  @IsBoolean()
  @Column({
    default: true,
  })
  isRevocable: boolean;

  @IsBoolean()
  @Column({
    default: true,
  })
  isEditable: boolean;

  /**
   * can CertToken owners revoke certified users (entries in Certificate)
   */
  @IsBoolean()
  @Column({
    default: true,
  })
  certificateIsRevocable: boolean;

  @ApiProperty({
    type: 'string',
    format: 'uuid',
    readOnly: true,
    required: false,
  })
  @IsUUID()
  @Column({
    type: 'uuid',
  })
  createdByUserId: string;

  @ManyToOne(() => UserEntity, (user) => user.createdCertTokens, {
    onUpdate: 'CASCADE',
    // When deleting a user, it should not by default delete any related one
    onDelete: 'NO ACTION',
  })
  @JoinColumn({ name: 'createdByUserId' })
  createdByUser: UserEntity;

  @ManyToMany(() => UserEntity, {
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinTable({
    name: 'cert_role', // table name for the junction table of this relation
    joinColumn: {
      name: 'certTokenId',
    },
    inverseJoinColumn: {
      name: 'userId',
    },
  })
  certRoles: CertRole[];

  @OneToMany(() => Certification, (certification) => certification.certToken)
  certifications: Certification[];

  @ManyToMany(() => UserEntity, {
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinTable({
    name: 'certification', // table name for the junction table of this relation
    joinColumn: {
      name: 'certTokenId',
    },
    inverseJoinColumn: {
      name: 'userId',
    },
  })
  certifiedUsers: UserEntity[];

  @OneToMany(() => VotePollCert, (votePollCert) => votePollCert.certToken)
  votePollCerts: VotePollCert[];

  @ManyToMany(() => VotePoll, (votePoll) => votePoll.certTokens)
  votePolls: VotePoll[];
}
