import { Test, TestingModule } from '@nestjs/testing';
import { CertRolesService } from './cert-roles.service';

describe('CertRolesService', () => {
  let service: CertRolesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CertRolesService],
    }).compile();

    service = module.get<CertRolesService>(CertRolesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
