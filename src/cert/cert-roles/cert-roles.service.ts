import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Crud } from '@nestjsx/crud';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { UserEntity } from 'src/users/entities/user.entity';
import { Repository } from 'typeorm';
import { CertRole } from '../entities/cert-role.entity';
import { CreateCertRoleDto } from './dto/create-cert-role.dto';
import { UpdateCertRoleDto } from './dto/update-cert-role.dto';

@Injectable()
@Crud({
  model: {
    type: CertRole,
  },
  params: {
    certTokenId: {
      field: 'certTokenId',
      type: 'uuid',
      primary: true,
    },
    userId: {
      field: 'userId',
      type: 'uuid',
      primary: true,
    },
  },
  dto: {
    create: CreateCertRoleDto,
    update: UpdateCertRoleDto,
    // replace: CreateCertRoleDto,
  },
})
export class CertRolesService extends TypeOrmCrudService<CertRole> {
  constructor(
    @InjectRepository(CertRole)
    readonly repo: Repository<CertRole>,
  ) {
    super(repo);
  }

  // create(createCertRoleDto: CreateCertRoleDto) {
  //   return 'This action adds a new certRole';
  // }

  // findAll() {
  //   return `This action returns all certRoles`;
  // }

  // findOne(id: number) {
  //   return `This action returns a #${id} certRole`;
  // }

  // update(id: number, updateCertRoleDto: UpdateCertRoleDto) {
  //   return `This action updates a #${id} certRole`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} certRole`;
  // }
}
