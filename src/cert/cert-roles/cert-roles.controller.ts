import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { Feature, Crud, CrudAuth, CrudController } from '@nestjsx/crud';
import { SCondition } from '@nestjsx/crud-request';
import { AppGuards } from 'src/app.guards';
import { AuthRequestUser } from 'src/auth/interfaces/auth-request.interface';
import { isAdmin } from 'src/shared/utils';
import { CertRole } from '../entities/cert-role.entity';
import { CertRolesService } from './cert-roles.service';
import { CreateCertRoleDto } from './dto/create-cert-role.dto';
import { UpdateCertRoleDto } from './dto/update-cert-role.dto';

@ApiTags('cert')
@Feature('cert-roles')
@Crud({
  model: {
    type: CertRole,
  },
  params: {
    certTokenId: {
      field: 'certTokenId',
      type: 'uuid',
      primary: true,
    },
    userId: {
      field: 'userId',
      type: 'uuid',
      primary: true,
    },
  },
  query: {
    // exclude: ['password'],
  },
  dto: {
    create: CreateCertRoleDto,
    update: UpdateCertRoleDto,
    // replace: CreateCertRoleDto,
  },
  routes: {},
})
@CrudAuth({
  property: 'user',
  filter: (user: AuthRequestUser): SCondition => {
    if (isAdmin(user)) {
      return;
    }

    return;

    // return {
    //   userId: user.id,
    // };
  },
})
@ApiBearerAuth('access-token')
@UseGuards(...AppGuards)
@Controller('cert/roles')
export class CertRolesController {
  constructor(public readonly service: CertRolesService) {}

  get base(): CrudController<CertRole> {
    return this;
  }

  // @Post()
  // create(@Body() createCertRoleDto: CreateCertRoleDto) {
  //   return this.certRolesService.create(createCertRoleDto);
  // }

  // @Get()
  // findAll() {
  //   return this.certRolesService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.certRolesService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateCertRoleDto: UpdateCertRoleDto) {
  //   return this.certRolesService.update(+id, updateCertRoleDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.certRolesService.remove(+id);
  // }
}
