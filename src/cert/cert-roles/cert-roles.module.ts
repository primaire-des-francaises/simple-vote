import { Module } from '@nestjs/common';
import { CertRolesService } from './cert-roles.service';
import { CertRolesController } from './cert-roles.controller';
import { CertRole } from '../entities/cert-role.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  controllers: [CertRolesController],
  providers: [CertRolesService],
  imports: [TypeOrmModule.forFeature([CertRole])],
})
export class CertRolesModule {}
