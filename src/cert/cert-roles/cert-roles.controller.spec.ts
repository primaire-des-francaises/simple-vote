import { Test, TestingModule } from '@nestjs/testing';
import { CertRolesController } from './cert-roles.controller';
import { CertRolesService } from './cert-roles.service';

describe('CertRolesController', () => {
  let controller: CertRolesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CertRolesController],
      providers: [CertRolesService],
    }).compile();

    controller = module.get<CertRolesController>(CertRolesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
