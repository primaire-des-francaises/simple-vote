import { PartialType } from '@nestjs/swagger';
import { CreateCertRoleDto } from './create-cert-role.dto';

export class UpdateCertRoleDto extends PartialType(CreateCertRoleDto) {}
