import { Module } from '@nestjs/common';
import { CertRolesModule } from './cert-roles/cert-roles.module';
import { CertificationsModule } from './certifications/certifications.module';

@Module({
  imports: [CertRolesModule, CertificationsModule],
})
export class CertModule {}
