import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { Feature, Crud, CrudAuth, CrudController } from '@nestjsx/crud';
import { SCondition } from '@nestjsx/crud-request';
import { AppGuards } from 'src/app.guards';
import { AuthRequestUser } from 'src/auth/interfaces/auth-request.interface';
import { isAdmin } from 'src/shared/utils';
import { CertRole } from '../entities/cert-role.entity';
import { Certification } from '../entities/certification.entity';
import { CertificationsService } from './certifications.service';
import { CreateCertificationDto } from './dto/create-certification.dto';
import { UpdateCertificationDto } from './dto/update-certification.dto';

@ApiTags('cert')
@Feature('certification')
@Crud({
  model: {
    type: CertRole,
  },
  params: {
    certTokenId: {
      field: 'certTokenId',
      type: 'uuid',
      primary: true,
    },
    userId: {
      field: 'userId',
      type: 'uuid',
      primary: true,
    },
  },
  query: {
    // exclude: ['password'],
  },
  dto: {
    create: CreateCertificationDto,
    update: UpdateCertificationDto,
    // replace: CreateCertRoleDto,
  },
  routes: {},
})
@CrudAuth({
  property: 'user',
  filter: (user: AuthRequestUser): SCondition => {
    if (isAdmin(user)) {
      return;
    }

    // return;

    return {
      userId: user.id,
    };
  },
})
@ApiBearerAuth('access-token')
@UseGuards(...AppGuards)
@Controller('cert/certifications')
export class CertificationsController {
  constructor(public readonly service: CertificationsService) {}

  get base(): CrudController<Certification> {
    return this;
  }

  // @Post()
  // create(@Body() createCertificationDto: CreateCertificationDto) {
  //   return this.certificationsService.create(createCertificationDto);
  // }

  // @Get()
  // findAll() {
  //   return this.certificationsService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.certificationsService.findOne(+id);
  // }

  // @Patch(':id')
  // update(
  //   @Param('id') id: string,
  //   @Body() updateCertificationDto: UpdateCertificationDto,
  // ) {
  //   return this.certificationsService.update(+id, updateCertificationDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.certificationsService.remove(+id);
  // }
}
