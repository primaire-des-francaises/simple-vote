import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Repository } from 'typeorm';
import { Certification } from '../entities/certification.entity';
import { CreateCertificationDto } from './dto/create-certification.dto';
import { UpdateCertificationDto } from './dto/update-certification.dto';

@Injectable()
export class CertificationsService extends TypeOrmCrudService<Certification> {
  constructor(
    @InjectRepository(Certification)
    readonly repo: Repository<Certification>,
  ) {
    super(repo);
  }

  // create(createCertificationDto: CreateCertificationDto) {
  //   return 'This action adds a new certification';
  // }

  // findAll() {
  //   return `This action returns all certifications`;
  // }

  // findOne(id: number) {
  //   return `This action returns a #${id} certification`;
  // }

  // update(id: number, updateCertificationDto: UpdateCertificationDto) {
  //   return `This action updates a #${id} certification`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} certification`;
  // }
}
