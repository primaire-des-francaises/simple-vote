import { Module } from '@nestjs/common';
import { CertificationsService } from './certifications.service';
import { CertificationsController } from './certifications.controller';
import { Certification } from '../entities/certification.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  controllers: [CertificationsController],
  providers: [CertificationsService],
  imports: [TypeOrmModule.forFeature([Certification])],
})
export class CertificationsModule {}
