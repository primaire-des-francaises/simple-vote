import { ApiProperty, ApiTags } from '@nestjs/swagger';
import { tr } from 'date-fns/locale';
import { TimestampedEntity } from 'src/shared/extended-base-entities';
import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { UserEntity } from './user.entity';

@ApiTags('user-profiles')
@Entity({ name: 'user_profile', withoutRowid: true })
export class UserProfile extends TimestampedEntity {
  @ApiProperty()
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ApiProperty()
  @Column({
    type: 'uuid',
  })
  userId: string;

  @ApiProperty()
  @Column()
  @Index({ unique: true })
  name: string;

  // @ApiProperty({ type: UserEntity, isArray: true })
  @ManyToOne(() => UserEntity, (user) => user.profiles, {
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'userId' })
  user: UserEntity;
}
