import {
  Entity,
  Column,
  BeforeInsert,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToMany,
} from 'typeorm';
import * as bcrypt from 'bcrypt';
import { ApiProperty, ApiTags } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';
import { IsEmail, IsNotEmpty, IsOptional, Length } from 'class-validator';
import { UseInterceptors, ClassSerializerInterceptor } from '@nestjs/common';
import { SoftDeleteTimestampedEntity } from 'src/shared/extended-base-entities';
import { AppRoles } from 'src/app.roles';
import { DbAwareColumn } from 'src/shared/db-aware-column';
import { Voter } from 'src/vote/entities/voter.entity';
import { VotePoll } from 'src/vote/entities/vote-poll.entity';
import { AuthToken } from 'src/auth/auth-tokens/entities/auth-token.entity';
import { CertToken } from 'src/cert/entities/cert-token.entity';
import { Certification } from 'src/cert/entities/certification.entity';
import { CertRole } from 'src/cert/entities/cert-role.entity';
import { UserProfile } from './user-profile.entity';
import { Candidate } from 'src/vote/entities/candidate.entity';
import { CandidateRole } from 'src/vote/entities/candidate-role.entity';
import { Participate } from 'src/vote/entities/participate.entity';
import { VotePollRole } from 'src/vote/entities/vote-poll-role.entity';

@ApiTags('users')
@Entity({ name: 'user' })
// @UseInterceptors(ClassSerializerInterceptor)
export class UserEntity extends SoftDeleteTimestampedEntity {
  @ApiProperty({
    type: 'string',
    format: 'uuid',
  })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ApiProperty()
  @Column({
    nullable: false,
    unique: true,
  })
  @IsNotEmpty({ message: 'The username is required' })
  @Length(2, 30, {
    message: 'The name must be at least 2 but not longer than 30 characters',
  })
  username!: string;

  @ApiProperty({
    type: 'string',
    format: 'email',
    required: false,
    nullable: true,
  })
  @Column({
    nullable: true,
    unique: true,
  })
  @IsOptional()
  @IsEmail({}, { message: 'Incorrect email' })
  email?: string;

  @ApiProperty({
    enum: AppRoles,
    default: AppRoles.USER,
    required: false,
  })
  @DbAwareColumn({
    type: 'simple-enum',
    enum: AppRoles,
    default: AppRoles.USER,
  })
  role: AppRoles;

  @ApiProperty({
    default: true,
    required: false,
  })
  @Column({
    default: true,
  })
  isActive: boolean;

  @Column({ select: false })
  @Exclude()
  @IsNotEmpty({ message: 'A password is required' })
  password?: string;

  @BeforeInsert()
  async hashPassword() {
    const saltOrRounds = 10;
    // const salt = await bcrypt.genSalt();

    const hash = await bcrypt.hash(this.password, saltOrRounds);
    this.password = hash;
  }

  async comparePassword(attempt: string): Promise<boolean> {
    return await bcrypt.compare(attempt, this.password);
  }

  @ApiProperty({
    type: () => UserProfile,
    isArray: true,
    required: false,
    readOnly: true,
  })
  @OneToMany(() => UserProfile, (profile) => profile.user, {
    cascade: true,
  })
  profiles: UserProfile[];

  @ApiProperty({
    type: () => Voter,
    isArray: true,
    required: false,
    readOnly: true,
  })
  @OneToMany(() => Voter, (voter) => voter.user)
  voters: Voter[];

  @ApiProperty({
    type: () => VotePoll,
    isArray: true,
    required: false,
    readOnly: true,
  })
  @ManyToMany(() => VotePoll, (votePoll) => votePoll.voterUsers)
  voterVotePolls: VotePoll[];

  @ApiProperty({
    type: () => AuthToken,
    isArray: true,
    required: false,
    readOnly: true,
  })
  @OneToMany(() => AuthToken, (authToken) => authToken.user)
  authTokens?: AuthToken[];

  @ApiProperty({
    type: () => CertToken,
    isArray: true,
    required: false,
    readOnly: true,
  })
  @OneToMany(() => CertToken, (certToken) => certToken.createdByUserId, {
    // cascade: ['insert'],
  })
  createdCertTokens?: CertToken[];

  @ApiProperty({
    type: () => Certification,
    isArray: true,
    required: false,
    readOnly: true,
  })
  @OneToMany(() => Certification, (certification) => certification.user)
  certifications?: Certification[];

  @ApiProperty({
    type: () => Certification,
    isArray: true,
    required: false,
    readOnly: true,
  })
  @OneToMany(
    () => Certification,
    (certification) => certification.createdByUser,
  )
  createdCertifications?: Certification[];

  @ApiProperty({
    type: () => CertRole,
    isArray: true,
    required: false,
    readOnly: true,
  })
  @OneToMany(() => CertRole, (certRole) => certRole.user)
  certRoles?: CertRole[];

  @ApiProperty({
    type: () => CertRole,
    isArray: true,
    required: false,
    readOnly: true,
  })
  @OneToMany(() => CertRole, (certRole) => certRole.createdByUser)
  createdCertRoles?: CertRole[];

  @ApiProperty({
    type: () => CandidateRole,
    isArray: true,
    required: false,
    readOnly: true,
  })
  @OneToMany(() => CandidateRole, (candidateRole) => candidateRole.user)
  candidateRoles: CandidateRole[];

  @ApiProperty({
    type: () => Candidate,
    isArray: true,
    required: false,
    readOnly: true,
  })
  @ManyToMany(
    () => Candidate,
    (candidateRoleCandidate) => candidateRoleCandidate.roleUsers,
  )
  candidateRoleCandidates: Candidate[];

  @ApiProperty({
    type: () => Candidate,
    isArray: true,
    required: false,
    readOnly: true,
  })
  @ManyToMany(() => Candidate, (candidate) => candidate.users)
  candidates: Candidate[];

  @ApiProperty({
    type: () => VotePoll,
    isArray: true,
    required: false,
    readOnly: true,
  })
  @OneToMany(() => VotePoll, (vp) => vp.createdByUser)
  createdVotePolls: VotePoll[];

  @ApiProperty({
    type: () => VotePollRole,
    isArray: true,
    required: false,
    readOnly: true,
  })
  @OneToMany(() => VotePollRole, (certRole) => certRole.user)
  votePollRoles?: VotePollRole[];

  @ManyToMany(() => VotePoll, (votePoll) => votePoll.roleUsers)
  votePollRoleVotePolls: VotePoll[];

  @ApiProperty({
    type: () => CertRole,
    isArray: true,
    required: false,
    readOnly: true,
  })
  @OneToMany(() => VotePollRole, (votePollRole) => votePollRole.createdByUser)
  createdVotePollRoles?: VotePollRole[];

  @ApiProperty({
    type: () => Candidate,
    isArray: true,
    required: false,
  })
  @OneToMany(() => Candidate, (candidate) => candidate.createdByUser)
  createdCandidates: Candidate[];

  @ApiProperty({
    type: () => CandidateRole,
    isArray: true,
    required: false,
  })
  @OneToMany(
    () => CandidateRole,
    (candidateRole) => candidateRole.createdByUser,
  )
  createdCandidateRoles: CandidateRole[];

  @ApiProperty({
    type: () => Participate,
    isArray: true,
    required: false,
    readOnly: true,
  })
  @OneToMany(() => Participate, (participate) => participate.createdByUser)
  createdParticipates: Participate[];
}
