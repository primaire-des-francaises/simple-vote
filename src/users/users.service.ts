import {
  Injectable,
  HttpException,
  HttpStatus,
  NotAcceptableException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Repository, DeleteResult, FindOneOptions } from 'typeorm';
import { UserEntity } from './entities/user.entity';
import { validate, validateOrReject } from 'class-validator';
import { Crud, CrudRequest, Override } from '@nestjsx/crud';
import { plainToClass } from 'class-transformer';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@Injectable()
@Crud({
  model: {
    type: UserEntity,
  },
  dto: {
    create: CreateUserDto,
    update: UpdateUserDto,
  },
})
export class UsersService extends TypeOrmCrudService<UserEntity> {
  // constructor(
  //   @InjectRepository(User)
  //   private readonly repo: Repository<UserEntity>,
  // ) {}
  constructor(
    @InjectRepository(UserEntity)
    readonly repo: Repository<UserEntity>,
  ) {
    super(repo);
  }

  // async findOne(options?: FindOneOptions<UserEntity>): Promise<UserEntity> {
  //   const user = await this.repo.findOne(options);
  //   return <UserEntity>toUserDto(user);
  // }

  // public async findAll(): Promise<UserEntity[]> {
  //   return await this.repo.find();
  // }

  public async findOneByEmail(email: string): Promise<UserEntity | null> {
    return await this.repo.findOne({ email });
  }

  public async findOneByUsername(username: string): Promise<UserEntity | null> {
    return await this.repo.findOne({ username });
  }

  public async findOneById(id: string): Promise<UserEntity> {
    return await this.repo.findOneOrFail(id);
  }

  async deleteOne(crudRequest: CrudRequest) {
    const myEntity = await this.getOneOrFail(crudRequest);
    return this.repo.softRemove(myEntity);
  }

  // public async create(user: CreateUserDto): Promise<UserEntity> {
  //   return await this.repo.save(user);
  // }

  // public async update(
  //   id: number,
  //   newValue: CreateUserDto,
  // ): Promise<User | null> {
  //   const user = await this.repo.findOneOrFail(id);
  //   if (!user.id) {
  //     // tslint:disable-next-line:no-console
  //     console.error("user doesn't exist");
  //   }
  //   await this.repo.update(id, newValue);
  //   return await this.repo.findOne(id);
  // }

  // public async delete(id: number): Promise<DeleteResult> {
  //   return await this.repo.delete(id);
  // }

  @Override('createOneBase')
  public async registerBase(
    req: CrudRequest,
    createUser: CreateUserDto,
  ): Promise<UserEntity> {
    return await this.register(createUser);
  }

  public async register(createUser: CreateUserDto): Promise<UserEntity> {
    createUser = plainToClass(CreateUserDto, createUser);

    await validateOrReject(createUser, {
      whitelist: true,
      forbidNonWhitelisted: true,
      stopAtFirstError: true,
    });

    const { username } = createUser;
    const userExists = await this.repo.findOne({ username });

    if (userExists) {
      // throw new HttpException('User already exists', HttpStatus.BAD_REQUEST);
      throw new NotAcceptableException('User already exists');
    }

    const userCreate = this.repo.create(createUser);
    return await userCreate.save();
  }
}
