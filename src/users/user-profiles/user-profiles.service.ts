import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Crud, CrudRequest } from '@nestjsx/crud';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Repository } from 'typeorm';
import { UserProfile } from '../entities/user-profile.entity';
import { CreateUserProfileDto } from './dto/create-user-profile.dto';
import { UpdateUserProfileDto } from './dto/update-user-profile.dto';

@Injectable()
@Crud({
  model: {
    type: UserProfile,
  },
  dto: {
    create: CreateUserProfileDto,
    update: UpdateUserProfileDto,
  },
})
export class UserProfilesService extends TypeOrmCrudService<UserProfile> {
  constructor(
    @InjectRepository(UserProfile)
    readonly repo: Repository<UserProfile>,
  ) {
    super(repo);
  }

  async deleteOne(crudRequest: CrudRequest) {
    const myEntity = await this.getOneOrFail(crudRequest);
    return this.repo.softRemove(myEntity);
  }
}
