import { Module } from '@nestjs/common';
import { UserProfilesService } from './user-profiles.service';
import { UserProfilesController } from './user-profiles.controller';
import { UserProfile } from '../entities/user-profile.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  controllers: [UserProfilesController],
  providers: [UserProfilesService],
  imports: [TypeOrmModule.forFeature([UserProfile])],
})
export class UserProfilesModule {}
