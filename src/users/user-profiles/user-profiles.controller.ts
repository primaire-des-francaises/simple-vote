import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { UserProfilesService } from './user-profiles.service';
import { CreateUserProfileDto } from './dto/create-user-profile.dto';
import { UpdateUserProfileDto } from './dto/update-user-profile.dto';
import { UserProfile } from '../entities/user-profile.entity';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Feature, Crud, CrudController, CrudAuth } from '@nestjsx/crud';
import { AppGuards } from 'src/app.guards';
import { SCondition } from '@nestjsx/crud-request';
import { AuthRequestUser } from 'src/auth/interfaces/auth-request.interface';
import { isAdmin } from 'src/shared/utils';

@ApiTags('users')
@Controller('user-profiles')
@Feature('user-profiles')
@Crud({
  model: {
    type: UserProfile,
  },
  dto: {
    create: CreateUserProfileDto,
    update: UpdateUserProfileDto,
  },
  params: {
    id: {
      field: 'id',
      type: 'uuid',
      primary: true,
    },
  },
  query: {
    join: {
      user: {},
    },
  },
})
@CrudAuth({
  property: 'user',
  filter: (user: AuthRequestUser): SCondition => {
    if (isAdmin(user)) {
      return;
    }

    return {
      userId: user.id,
    };
  },
})
@ApiBearerAuth('access-token')
@UseGuards(...AppGuards)
export class UserProfilesController implements CrudController<UserProfile> {
  constructor(public readonly service: UserProfilesService) {}

  get base(): CrudController<UserProfile> {
    return this;
  }
}
