import { Controller, UseGuards } from '@nestjs/common';
import { CrudController, Crud, CrudAuth, Feature } from '@nestjsx/crud';
import { SCondition } from '@nestjsx/crud-request';
import { UserEntity } from './entities/user.entity';
import { UsersService } from './users.service';
import { AppGuards } from 'src/app.guards';
import { isAdmin } from 'src/shared/utils';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AuthRequestUser } from 'src/auth/interfaces/auth-request.interface';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

// @UseRoles({
//   resource:  'video',
//   action:  'read',
//   possession:  'any',
// })

@ApiTags('users')
@ApiBearerAuth('access-token')
@Crud({
  model: {
    type: UserEntity,
  },
  params: {
    id: {
      field: 'id',
      type: 'uuid',
      primary: true,
    },
  },
  query: {
    exclude: ['password'],
  },
  dto: {
    create: CreateUserDto,
    update: UpdateUserDto,
    replace: UpdateUserDto,
  },
  routes: {},
})
@CrudAuth({
  property: 'user',
  filter: (user: AuthRequestUser): SCondition => {
    if (isAdmin(user)) {
      return;
    }

    return {
      id: user.id,
      isActive: true,
    };
  },
})
@Controller('users')
@Feature('users')
@UseGuards(...AppGuards)
export class UsersController implements CrudController<UserEntity> {
  constructor(public service: UsersService) {}

  get base(): CrudController<UserEntity> {
    return this;
  }

  // @Override('getManyBase')
  // getAll(@ParsedRequest() req: CrudRequest) {
  //   return this.base.getManyBase(req);
  // }
}
