import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { ClassConstructor } from 'class-transformer';
import {
  IsNotEmpty,
  IsEmail,
  IsOptional,
  MinLength,
  MaxLength,
  IsAlphanumeric,
  ValidateIf,
  Validate,
  ValidatorConstraintInterface,
  ValidationArguments,
  ValidatorConstraint,
  registerDecorator,
  ValidationOptions,
} from 'class-validator';
import { UserEntity } from '../entities/user.entity';

@ValidatorConstraint({ name: 'PasswordValidator', async: false })
export class PasswordValidator implements ValidatorConstraintInterface {
  validate(password: string, args: ValidationArguments) {
    // accepts lowercase and uppercase characters
    const oneLetterRegex = /[A-zÀ-ú]+/;
    const oneNumberRegex = /[0-9]+/;
    const oneSpecialCharRegex =
      /[*@!#%&()^~{}`!()`!@#$*()_+\-=\[\];':"\\|,\.<>\/?~]+/;

    return (
      oneLetterRegex.test(password) &&
      (oneNumberRegex.test(password) || oneSpecialCharRegex.test(password))
    );
  }

  defaultMessage(args: ValidationArguments) {
    return 'Password requires at lest one letter and at least one number or special char';
  }
}

export const NotSimilarTo = <T>(
  type: ClassConstructor<T>,
  username: (o: T) => any,
  validationOptions?: ValidationOptions,
) => {
  return (object: any, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [username],
      validator: NotSimilarToConstraint,
    });
  };
};

@ValidatorConstraint({ name: 'NotSimilarTo' })
export class NotSimilarToConstraint implements ValidatorConstraintInterface {
  validate(password: any, args: ValidationArguments) {
    const [fn] = args.constraints;
    const username = fn(args.object);

    return !(password.includes(username) || username.includes(password));
  }

  defaultMessage(args: ValidationArguments) {
    // const [constraintProperty]: (() => any)[] = args.constraints;
    // return `${constraintProperty} and ${args.property} does not match`;

    return `Password should not be similar to username`;
  }
}
export class CreateUserDto {
  // @ApiProperty()
  // readonly id: string;

  // @ApiProperty()
  // readonly firstName: string;

  // @ApiProperty()
  // readonly lastName: string;

  @ApiProperty()
  @IsNotEmpty()
  @MinLength(4)
  @MaxLength(64)
  @IsAlphanumeric()
  // @IsAscii()
  // @Matches(pattern: RegExp, modifiers?: string)
  readonly username: string;

  @ApiProperty()
  @IsNotEmpty()
  @MinLength(6)
  @MaxLength(64)
  @Validate(PasswordValidator)
  @NotSimilarTo(UserEntity, (u) => u.username, {})
  password: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNotEmpty()
  @IsEmail()
  readonly email?: string;
}
