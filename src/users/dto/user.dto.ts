import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsEmail } from 'class-validator';
import { UserEntity } from '../entities/user.entity';
import { CreateUserDto } from './create-user.dto';

export class UserDto {
  @IsNotEmpty()
  id: string;

  @IsNotEmpty()
  username: string;

  // @IsNotEmpty()
  @IsEmail()
  email?: string;
}

export const toUserDto = (data: UserEntity): UserDto => {
  const { id, username, email } = data;
  let userDto: UserDto = { id, username, email };
  return userDto;
};
