import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { getFeature, getAction } from '@nestjsx/crud';
import { ACGuard } from 'nest-access-control';
// import { ACGuard } from 'nest-access-control';

@Injectable()
export class AppCanActivate implements CanActivate {
  canActivate(ctx: ExecutionContext): boolean {
    const handler = ctx.getHandler();
    const controller = ctx.getClass();

    const feature = getFeature(controller);

    // TODO: On VoteController, returns 'undefined', still looking on how to fix that
    const action = getAction(handler);

    console.log(`appGuards: ${feature}-${action}`); // e.g. 'Heroes-Read-All'

    return true;
  }
}

export const AppGuards = [AuthGuard('jwt'), ACGuard, AppCanActivate];

/**
 *
 */
// enum CrudActions {
//   ReadAll = "Read-All",
//   ReadOne = "Read-One",
//   CreateOne = "Create-One",
//   CreateMany = "Create-Many",
//   UpdateOne = "Update-One",
//   ReplaceOne = "Replace-One",
//   DeleteOne = "Delete-One",
// }
