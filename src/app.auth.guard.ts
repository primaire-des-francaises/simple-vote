import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ACGuard } from 'nest-access-control';
import { JwtAuthGuard } from './auth/jwt-auth.guard';

/**
 * this is experimental... not is use !!!!
 */
@Injectable()
export class AppAuthGuard extends JwtAuthGuard implements CanActivate {
  constructor(private readonly acGuard: ACGuard) {
    super();
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    //   const req = context.switchToHttp().getRequest();
    //   const fakeUser = {
    //     roles: ['ADMIN_UPDATE_OWN_VIDEO', 'USER_CREATE_ANY_VIDEO'],
    //     username: '@fake',
    //   };
    //   req.user = fakeUser;
    return await this.acGuard.canActivate(context);
  }
}
