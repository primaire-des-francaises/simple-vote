import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { BaseEntity } from 'typeorm';
import {
  DbAwareCreateDateColumn,
  DbAwareDeleteDateColumn,
  DbAwareUpdateDateColumn,
} from './db-aware-column';

// for postgres
// const dateType = { type: 'timestamptz' }
const columnDateType = {};

export class TimestampedEntity extends BaseEntity {
  /**
   * Auto-generated at insert
   *
   * @type {Date}
   * @memberof ControlPlace
   */

  @ApiProperty({
    default: 'CURRENT_TIMESTAMP',
    readOnly: true,
    required: false,
  })
  @DbAwareCreateDateColumn({ type: 'timestamptz' })
  @Type(() => Date)
  createdAt?: Date;

  /**
   * Auto-updated at insert & update
   *
   * @type {Date}
   * @memberof ControlPlace
   */

  @ApiProperty({
    default: 'CURRENT_TIMESTAMP',
    readOnly: true,
    required: false,
  })
  @DbAwareUpdateDateColumn({ type: 'timestamptz' })
  @Type(() => Date)
  updatedAt?: Date;
}

export class SoftDeleteEntity extends BaseEntity {
  @DbAwareDeleteDateColumn({ type: 'timestamptz' })
  @Type(() => Date)
  deletedAt?: Date;
}

export class SoftDeleteTimestampedEntity extends TimestampedEntity {
  @ApiProperty({
    type: Date,
    readOnly: true,
    required: false,
    default: null,
  })
  @DbAwareDeleteDateColumn({ type: 'timestamptz' })
  @Type(() => Date)
  deletedAt?: Date;
}

// export class SoftDeleteTimestampedEntity {}

// export interface SoftDeleteTimestampedEntity
//   extends TimestampedEntity,
//     SoftDeleteEntity {}

// applyMixins(SoftDeleteTimestampedEntity, [TimestampedEntity, SoftDeleteEntity]);

// function applyMixins(derivedCtor: any, constructors: any[]) {
//   constructors.forEach(baseCtor => {
//     Object.getOwnPropertyNames(baseCtor.prototype).forEach(name => {
//       Object.defineProperty(
//         derivedCtor.prototype,
//         name,
//         Object.getOwnPropertyDescriptor(baseCtor.prototype, name) ||
//           Object.create(null),
//       );
//     });
//   });
// }
