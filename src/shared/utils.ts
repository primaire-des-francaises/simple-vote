import { getConnectionOptions, getConnection } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { Logger } from '@nestjs/common';
import { UserEntity } from 'src/users/entities/user.entity';
import { AuthRequestUser } from 'src/auth/interfaces/auth-request.interface';
import { AppRoles } from 'src/app.roles';

export const toPromise = <T>(data: T): Promise<T> => {
  return new Promise<T>((resolve) => {
    resolve(data);
  });
};

export const getDbConnectionOptions = async (
  connectionName: string = 'default',
) => {
  const options = await getConnectionOptions(
    process.env.NODE_ENV || 'development',
  );
  return {
    ...options,
    name: connectionName,
  };
};

export const getDbConnection = async (connectionName: string = 'default') => {
  return await getConnection(connectionName);
};

export const runDbMigrations = async (connectionName: string = 'default') => {
  const conn = await getDbConnection(connectionName);
  await conn.runMigrations();
};

// export const comparePasswords = async (userPassword, currentPassword) => {
//   return await bcrypt.compare(currentPassword, userPassword);
// };

export const isAdmin = (
  user: AuthRequestUser,
  isSuperAdmin: boolean = false,
): boolean => {
  const roles: AppRoles[] = [AppRoles.SUPER_ADMIN];
  const userRoles: AppRoles[] = [user.role];

  if (!isSuperAdmin) {
    roles.push(AppRoles.ADMIN);
  }

  return roles.some((r) => {
    // ES2016
    // return userRoles.includes(r);

    //ES6
    return userRoles.indexOf(r) >= 0;
  });
};

export function getEnumKeyByEnumValue<
  TEnumKey extends string,
  TEnumVal extends string | number,
>(myEnum: { [key in TEnumKey]: TEnumVal }, enumValue: TEnumVal): string {
  const keys = (Object.keys(myEnum) as TEnumKey[]).filter(
    (x) => myEnum[x] === enumValue,
  );
  return keys.length > 0 ? keys[0] : '';
}
