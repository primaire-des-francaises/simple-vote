import {
  Column,
  ColumnOptions,
  ColumnType,
  CreateDateColumn,
  DeleteDateColumn,
  UpdateDateColumn,
} from 'typeorm';

import * as ormconfig from '../../ormconfig';

export const getOrmconfig = () => ormconfig;

const postgreSqlSqliteTypeMapping: { [key: string]: ColumnType } = {
  timestamptz: 'datetime',
  enum: 'simple-enum',
  array: 'simple-array',
  json: 'simple-json',
};

export function resolveDbType(postgreSqlType: ColumnType): ColumnType {
  // const nodeEnv: string = process.env.NODE_ENV || 'development';
  const isSqlite: boolean = getOrmconfig().type == 'better-sqlite3'; // ['development'].indexOf(nodeEnv) > -1;

  if (isSqlite && postgreSqlType.toString() in postgreSqlSqliteTypeMapping) {
    return postgreSqlSqliteTypeMapping[postgreSqlType.toString()];
  }

  return postgreSqlType;
}

export function DbAwareColumn(columnOptions: ColumnOptions) {
  if (columnOptions.type) {
    columnOptions.type = resolveDbType(columnOptions.type);
  }
  return Column(columnOptions);
}

export function DbAwareCreateDateColumn(columnOptions: ColumnOptions) {
  if (columnOptions.type) {
    columnOptions.type = resolveDbType(columnOptions.type);
  }
  return CreateDateColumn(columnOptions);
}

export function DbAwareUpdateDateColumn(columnOptions: ColumnOptions) {
  if (columnOptions.type) {
    columnOptions.type = resolveDbType(columnOptions.type);
  }
  return UpdateDateColumn(columnOptions);
}

export function DbAwareDeleteDateColumn(columnOptions: ColumnOptions) {
  if (columnOptions.type) {
    columnOptions.type = resolveDbType(columnOptions.type);
  }
  return DeleteDateColumn(columnOptions);
}
