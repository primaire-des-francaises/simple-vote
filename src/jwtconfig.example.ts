import { SignOptions, VerifyOptions } from 'jsonwebtoken';

export const JWT_SECRET = 'secret-key';

// changing this one will cancel any issued accreditation token
// that has not been activated yet
export const JWT_SECRET_CERT = 'secret-key';

export const JWT_SIGN_OPTIONS: SignOptions = {
  expiresIn: 3600,
  issuer: 'localhost',
  audience: 'localhost',
};

export const JWT_VERIFY_OPTIONS: VerifyOptions = {
  issuer: 'localhost',
  audience: 'localhost',
  ignoreExpiration: true,
};
