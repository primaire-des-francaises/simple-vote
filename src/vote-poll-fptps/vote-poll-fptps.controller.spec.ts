import { Test, TestingModule } from '@nestjs/testing';
import { VotePollFptpsController } from './vote-poll-fptps.controller';
import { VotePollFptpsService } from './vote-poll-fptps.service';

describe('VotePollFptpsController', () => {
  let controller: VotePollFptpsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [VotePollFptpsController],
      providers: [VotePollFptpsService],
    }).compile();

    controller = module.get<VotePollFptpsController>(VotePollFptpsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
