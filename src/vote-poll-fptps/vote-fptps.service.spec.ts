import { Test, TestingModule } from '@nestjs/testing';
import { VotePollFptpsService } from './vote-poll-fptps.service';

describe('VotePollFptpsService', () => {
  let service: VotePollFptpsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [VotePollFptpsService],
    }).compile();

    service = module.get<VotePollFptpsService>(VotePollFptpsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
