import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CrudRequest } from '@nestjsx/crud';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Repository } from 'typeorm';
import { CreateVotePollFptpDto } from './dto/create-vote-poll-fptp.dto';
import { UpdateVotePollFptpDto } from './dto/update-vote-poll-fptp.dto';
import { VotePollFptp } from './entities/vote-poll-fptp.entity';

@Injectable()
export class VotePollFptpsService extends TypeOrmCrudService<VotePollFptp> {
  constructor(
    @InjectRepository(VotePollFptp)
    readonly repo: Repository<VotePollFptp>,
  ) {
    super(repo);
  }

  async deleteOne(crudRequest: CrudRequest) {
    const myEntity = await this.getOneOrFail(crudRequest);
    return this.repo.softRemove(myEntity);
  }

  // create(createVotePollFptpDto: CreateVotePollFptpDto) {
  //   return 'This action adds a new voteFptp';
  // }

  // findAll() {
  //   return `This action returns all voteFptps`;
  // }

  // findOne(id: number) {
  //   return `This action returns a #${id} voteFptp`;
  // }

  // update(id: number, updateVotePollFptpDto: UpdateVotePollFptpDto) {
  //   return `This action updates a #${id} voteFptp`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} voteFptp`;
  // }
}
