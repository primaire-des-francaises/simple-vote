import { UseInterceptors, ClassSerializerInterceptor } from '@nestjs/common';
import { IntersectionType } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { SoftDeleteTimestampedEntity } from 'src/shared/extended-base-entities';
import { VotePoll } from 'src/vote/entities/vote-poll.entity';
import {
  BaseEntity,
  BeforeInsert,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryColumn,
} from 'typeorm';

@Entity({ name: 'vote_poll_fptp' })
export class VotePollFptp extends SoftDeleteTimestampedEntity {
  @Column({
    type: 'uuid',
    primary: true,
  })
  votePollId: string;

  @OneToOne((type) => VotePoll, (votePoll) => votePoll.votePollFptp, {
    primary: true,
    cascade: true,
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'votePollId' })
  votePoll: VotePoll;

  // @BeforeInsert()
  // newId() {
  //   this.votePollId = this.vote.id;
  // }
}
