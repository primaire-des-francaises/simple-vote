import { PartialType } from '@nestjs/swagger';
import { CreateVotePollFptpDto } from './create-vote-poll-fptp.dto';

export class UpdateVotePollFptpDto extends PartialType(CreateVotePollFptpDto) {}
