import { Module } from '@nestjs/common';
import { VotePollFptpsService } from './vote-poll-fptps.service';
import { VotePollFptpsController } from './vote-poll-fptps.controller';
import { VotePollFptp } from './entities/vote-poll-fptp.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  controllers: [VotePollFptpsController],
  providers: [VotePollFptpsService],
  imports: [TypeOrmModule.forFeature([VotePollFptp])],
  exports: [VotePollFptpsService],
})
export class VotePollFptpsModule {}
