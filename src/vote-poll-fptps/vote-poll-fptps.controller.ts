import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { VotePollFptpsService } from './vote-poll-fptps.service';
import { CreateVotePollFptpDto } from './dto/create-vote-poll-fptp.dto';
import { UpdateVotePollFptpDto } from './dto/update-vote-poll-fptp.dto';
import { VotePollFptp } from './entities/vote-poll-fptp.entity';
import { Crud, CrudController, Feature } from '@nestjsx/crud';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AppGuards } from 'src/app.guards';

@ApiTags('vote-pool-fptps')
@Controller('vote-pool-fptps')
@Feature('vote-pool-fptps')
@Crud({
  model: {
    type: VotePollFptp,
  },
  params: {
    id: {
      field: 'id',
      type: 'uuid',
      primary: true,
    },
  },
  query: {
    join: {
      votePoll: {},
      'votePoll.candidates': {},
      'votePoll.participates': {},
      'votePoll.participates.candidates': {},
    },
  },
})
@ApiBearerAuth('access-token')
@UseGuards(...AppGuards)
export class VotePollFptpsController implements CrudController<VotePollFptp> {
  constructor(public readonly service: VotePollFptpsService) {}

  get base(): CrudController<VotePollFptp> {
    return this;
  }

  // @Post()
  // create(@Body() createVotePollFptpDto: CreateVotePollFptpDto) {
  //   return this.voteFptpsService.create(createVotePollFptpDto);
  // }

  // @Get()
  // findAll() {
  //   return this.voteFptpsService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.voteFptpsService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateVotePollFptpDto: UpdateVotePollFptpDto) {
  //   return this.voteFptpsService.update(+id, updateVotePollFptpDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.voteFptpsService.remove(+id);
  // }
}
