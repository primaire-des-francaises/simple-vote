import { Module } from '@nestjs/common';
import { PublicService } from './public.service';
import { PublicController } from './public.controller';
import { VotePoll } from 'src/vote/entities/vote-poll.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { VotePollsModule } from 'src/vote/vote-polls/vote-polls.module';

@Module({
  imports: [TypeOrmModule.forFeature([VotePoll]), VotePollsModule],
  providers: [PublicService],
  controllers: [PublicController],
})
export class PublicModule {}
