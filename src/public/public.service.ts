import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Candidate } from 'src/vote/entities/candidate.entity';
import { Participate } from 'src/vote/entities/participate.entity';
import { VotePoll, VotePollStatus } from 'src/vote/entities/vote-poll.entity';
import {
  LessThan,
  LessThanOrEqual,
  MoreThanOrEqual,
  Repository,
} from 'typeorm';
import * as LRU from 'lru-cache';
import {
  VotePollJoin,
  VotePollsService,
} from 'src/vote/vote-polls/vote-polls.service';

var cache = new LRU({
  max: 500,
  // length: function (n: any, key: any[]) {
  //   return n * 2 + key.length;
  // },
  // dispose: function (key, n) {
  //   n.close();
  // },
  maxAge: 1000 * 1,
});

@Injectable()
export class PublicService {
  constructor(private votePollsService: VotePollsService) {}

  private readonly logger = new Logger(PublicService.name);

  async getManyVotePolls(): Promise<VotePoll[]> {
    const currentTimestamp = new Date().toISOString();
    const now = () => new Date(currentTimestamp);

    let votePolls: VotePoll[];

    const cacheKey = 'allPublicVotePolls';

    votePolls = <VotePoll[]>cache.get(cacheKey);

    if (votePolls) {
      return votePolls;
    }

    // votePolls = await this.votePollsService.getVotePolls(
    //   'public',
    //   'any',
    //   // [
    //   //   VotePollStatus.completed,
    //   //   VotePollStatus.pending,
    //   //   // VotePollStatus.counting,
    //   //   VotePollStatus.open,
    //   // ],
    //   {
    //     join: [VotePollJoin.participates, VotePollJoin.candidate],
    //     orderBy: [
    //       {
    //         sort: 'VotePoll.votingEndsAt',
    //         order: 'DESC',
    //         nulls: 'NULLS LAST',
    //       },
    //       {
    //         sort: 'VotePoll.votingStartsAt',
    //         order: 'DESC',
    //       },
    //     ],
    //   },
    // );

    votePolls = await this.votePollsService.getVotePolls({
      visibility: 'public',
      status: 'any',
      // status: [
      //   VotePollStatus.completed,
      //   // VotePollStatus.pending,
      //   // VotePollStatus.counting,
      //   // VotePollStatus.open,
      // ],
      join: [VotePollJoin.participates, VotePollJoin.candidate],
      orderBy: [
        {
          sort: 'VotePoll.votingEndsAt',
          order: 'DESC',
          nulls: 'NULLS LAST',
        },
        {
          sort: 'VotePoll.votingStartsAt',
          order: 'DESC',
        },
        {
          sort: 'participates.votesCount',
          order: 'DESC',
        },
        {
          sort: 'participates.title',
          order: 'ASC',
        },
      ],
    });

    // cache.set(cacheKey, votePolls);

    return votePolls;
  }

  public async completedVotePolls(): Promise<VotePoll[]> {
    let votePolls: VotePoll[];

    votePolls = await this.votePollsService.getVotePolls({
      visibility: 'public',
      status: [
        VotePollStatus.completed,
        // VotePollStatus.pending,
        // VotePollStatus.counting,
        // VotePollStatus.open,
      ],
      join: [VotePollJoin.participates, VotePollJoin.candidate],
      orderBy: [
        {
          sort: 'VotePoll.votingEndsAt',
          order: 'DESC',
          nulls: 'NULLS LAST',
        },
        {
          sort: 'VotePoll.votingStartsAt',
          order: 'DESC',
        },
      ],
    });

    return votePolls;
  }
}
