import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { classToPlain } from 'class-transformer';
import { VotePoll } from 'src/vote/entities/vote-poll.entity';
import { PublicService } from './public.service';

@ApiTags('public')
@Controller('public')
export class PublicController {
  constructor(private readonly publicService: PublicService) {}

  @Get('vote-polls')
  async openVotePolls() {
    const votePolls = await this.publicService.getManyVotePolls();
    // return { status: votePolls[0].status };
    return classToPlain(votePolls);
  }

  @Get('completed-vote-polls')
  async completedVotePolls() {
    const completedVotePolls = await this.publicService.completedVotePolls();

    return classToPlain(completedVotePolls);
  }
}
