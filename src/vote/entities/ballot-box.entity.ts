import { mixin } from '@nestjs/common';
import { IntersectionType } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsDefined, IsInt, IsUUID, Max, Min, validate } from 'class-validator';
import {
  DbAwareColumn,
  DbAwareUpdateDateColumn,
} from 'src/shared/db-aware-column';
import {
  BaseEntity,
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Candidate } from './candidate.entity';
import { Participate } from './participate.entity';
import { VotePoll } from './vote-poll.entity';

// @Entity({ name: 'ballot_box', withoutRowid: true })
// export class BallotBox extends Mixin(BallotBoxDto, BaseEntity){

// }

@Entity()
export class BallotBox extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @IsDefined()
  @IsUUID()
  @Column({
    type: 'uuid',
  })
  // @Index({ unique: false })
  votePollId!: string;

  @IsDefined()
  @IsUUID()
  @Column({
    type: 'uuid',
  })
  // @Index({ unique: false })
  candidateId!: string;

  // @IsDefined()
  @IsInt()
  @Min(1)
  @Max(7)
  @Column({
    type: 'int',
    default: null,
    nullable: true,
  })
  weight?: number;

  // @Column({
  //   type: 'datetime',
  //   default: `datetime('now')`,
  // })
  @CreateDateColumn({
    type: 'date',
    transformer: {
      to: (value) => new Date().toDateString(),
      from: (value) => value,
    },
  })
  @Type(() => Date)
  createdDate: Date;

  // TODO: if this field is ever other than null, should cancel the vote
  // @Column({
  //   type: 'date',
  //   default: null,
  //   // select: false,
  // })
  // @UpdateDateColumn({
  //   // insert: false,
  // })
  @DbAwareColumn({
    type: 'timestamptz',
    insert: false,
    nullable: true,
    default: null,
    // default: () => `(datetime('now'))`,
  })
  @Type(() => Date)
  updatedAt: Date;

  @ManyToOne(() => VotePoll, (votePoll) => votePoll.ballotBoxes, {
    cascade: ['insert', 'update'],
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'votePollId', referencedColumnName: 'id' })
  votePoll;

  @ManyToOne(() => Candidate, {
    cascade: true,
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'candidateId', referencedColumnName: 'id' })
  candidate;

  @ManyToOne(() => Participate, (participate) => participate.ballotBoxes, {
    createForeignKeyConstraints: false,
  })
  @JoinColumn([
    { name: 'candidateId', referencedColumnName: 'candidateId' },
    { name: 'votePollId', referencedColumnName: 'votePollId' },
  ])
  participate: Participate;

  // was not called... strange, so used a transformer
  // @BeforeInsert()
  // setDateLocal() {
  //   // TODO: make this date for votePoll.local_timezone
  //   this.createdDate = new Date(new Date().toDateString());
  //   console.log('thisA', this);
  // }

  @BeforeUpdate()
  setWasUpdated() {
    // poor man's protection
    this.updatedAt = new Date(new Date().toDateString());

    // using a trigger could improve it
    // CREATE TRIGGER UpdateLastTime UPDATE OF field1, field2, fieldN ON counter
    // BEGIN
    //   UPDATE Package SET LastUpdate=CURRENT_TIMESTAMP WHERE ActionId=ActionId;
    // END;
  }
}
