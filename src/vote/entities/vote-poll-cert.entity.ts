import { CertToken } from 'src/cert/entities/cert-token.entity';
import { TimestampedEntity } from 'src/shared/extended-base-entities';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { VotePoll } from './vote-poll.entity';

@Entity({ name: 'vote_poll_cert', withoutRowid: true })
export class VotePollCert extends TimestampedEntity {
  @Column({
    type: 'uuid',
    primary: true,
  })
  votePollId: string;

  @Column({
    type: 'uuid',
    primary: true,
  })
  certTokenId: string;

  @ManyToOne(() => VotePoll, (votePoll) => votePoll.certs, {
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'votePollId' })
  votePoll: VotePoll;

  @ManyToOne(() => CertToken, (certToken) => certToken.votePollCerts, {
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'certTokenId' })
  certToken: CertToken;
}
