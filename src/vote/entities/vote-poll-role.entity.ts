import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { DbAwareColumn } from 'src/shared/db-aware-column';
import { TimestampedEntity } from 'src/shared/extended-base-entities';
import { UserEntity } from 'src/users/entities/user.entity';
import { Column, Entity, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { Candidate } from './candidate.entity';
import { VotePoll } from './vote-poll.entity';

export enum EnumVotePollRole {
  owner = 'owner',
  administrator = 'administrator',
  manager = 'manager',
  editor = 'editor',
  voter = 'voter',
}

@Entity({ name: 'vote_poll_role', withoutRowid: true })
export class VotePollRole extends TimestampedEntity {
  @ApiProperty({
    type: 'string',
    format: 'uuid',
  })
  @Column({
    type: 'uuid',
    primary: true,
  })
  votePollId: string;

  @ApiProperty({
    type: 'string',
    format: 'uuid',
  })
  @Column({
    type: 'uuid',
    primary: true,
  })
  userId: string;

  @ApiProperty({
    type: 'string',
    format: 'uuid',
    readOnly: true,
    required: false,
  })
  @Column({
    type: 'uuid',
    nullable: false,
  })
  createdByUserId: string;

  // @ApiProperty({
  //   type: () => UserEntity,
  //   readOnly: true,
  //   required: false,
  // })
  @ManyToOne(() => UserEntity, (user) => user.createdVotePollRoles)
  @JoinColumn({ name: 'createdByUserId' })
  createdByUser: UserEntity;

  @DbAwareColumn({
    type: 'timestamptz',
    nullable: true,
    default: null,
  })
  @Type(() => Date)
  expiresAt: Date;

  @DbAwareColumn({
    type: 'enum',
    enum: EnumVotePollRole,
    default: EnumVotePollRole.voter,
  })
  role: EnumVotePollRole;

  @ManyToOne(() => VotePoll, (votePoll) => votePoll.roles, {
    // createForeignKeyConstraints: false,
  })
  @JoinColumn({ name: 'votePollId' })
  votePoll: VotePoll;

  @ManyToOne(() => UserEntity, (user) => user.votePollRoles, {
    // createForeignKeyConstraints: false,
  })
  @JoinColumn({ name: 'userId' })
  user: UserEntity;
}
