import { Inject, Injectable } from '@nestjs/common';
import { NestApplication } from '@nestjs/core';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Type } from 'class-transformer';
import { IsString, IsOptional, isUUID } from 'class-validator';
import slugify from 'slugify';
import { CertToken } from 'src/cert/entities/cert-token.entity';
import { autoSlug } from 'src/shared/auto-slug';
import { DbAwareColumn } from 'src/shared/db-aware-column';
import { SoftDeleteTimestampedEntity } from 'src/shared/extended-base-entities';
import { UserEntity } from 'src/users/entities/user.entity';
import { VotePollFptp } from 'src/vote-poll-fptps/entities/vote-poll-fptp.entity';
import {
  AfterInsert,
  AfterLoad,
  AfterRemove,
  AfterUpdate,
  BaseEntity,
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { BallotBox } from './ballot-box.entity';
import { Candidate } from './candidate.entity';
import { Participate } from './participate.entity';
import { VotePollCert } from './vote-poll-cert.entity';
import { VotePollRole } from './vote-poll-role.entity';
import { Voter } from './voter.entity';

export enum VotingModules {
  fptp = 'fptp',
}

export enum VotingSchemes {
  single = 'single',
  singleWeighed3 = 'singleWeighed3',
  singleWeighed5 = 'singleWeighed5',
  singleWeighed7 = 'singleWeighed7',
  multiWeighed3 = 'multiWeighed3',
  multiWeighed5 = 'multiWeighed5',
  multiWeighed7 = 'multiWeighed7',
}

export enum VotePollStatus {
  closed = 'closed',
  pending = 'pending',
  open = 'open',
  counting = 'counting',
  completed = 'completed',
}

export class ParticipateResult {
  public candidateId: string;
  public votesCount: number;
  public votesWeight: number;
}
export class VotePollResult {
  public votesCount: number;
  public ballotBoxBallotsCount: number;
  public ballotBoxBlankBallotsCount: number;
  public participateResults: ParticipateResult[];
}

@Entity({ name: 'vote_poll', withoutRowid: true })
// @UseInterceptors(ClassSerializerInterceptor)
export class VotePoll extends SoftDeleteTimestampedEntity {
  @ApiProperty({
    type: 'string',
    format: 'uuid',
    readOnly: true,
    required: false,
  })
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ApiProperty({
    type: 'string',
    format: 'uuid',
    readOnly: true,
    required: false,
  })
  @Column({
    type: 'uuid',
    nullable: false,
  })
  createdByUserId: string;

  // @ApiProperty({
  //   type: () => UserEntity,
  //   readOnly: true,
  //   required: false,
  // })
  @ManyToOne(() => UserEntity, (user) => user.createdVotePolls)
  @JoinColumn({ name: 'createdByUserId' })
  createdByUser: UserEntity;

  @ApiProperty({
    type: () => Participate,
    isArray: true,
    readOnly: true,
    required: false,
  })
  @OneToMany(() => Participate, (participate) => participate.votePoll, {
    // this seems to brake soft-delete bu tis required for cascade insert
    cascade: ['insert', 'update'],
  })
  public participates?: Participate[];

  @ApiProperty({
    type: () => Candidate,
    isArray: true,
    readOnly: true,
    required: false,
  })
  @ManyToMany(() => Candidate, (candidate) => candidate.votePolls, {
    cascade: true, //['insert', 'update', 'soft-remove'],
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinTable({
    name: 'participate', // table name for the junction table of this relation
    joinColumn: {
      name: 'votePollId',
    },
    inverseJoinColumn: {
      name: 'candidateId',
    },
  })
  candidates?: Candidate[];

  // @ApiProperty({
  //   type: () => Voter,
  //   isArray: true,
  //   readOnly: true,
  //   required: false,
  // })
  @OneToMany(() => Voter, (voter) => voter.votePoll)
  voters?: Voter[];

  // @ApiProperty({
  //   type: () => UserEntity,
  //   isArray: true,
  //   readOnly: true,
  //   required: false,
  // })
  @ManyToMany(() => UserEntity, (user) => user.voterVotePolls, {
    cascade: ['insert', 'update'],
    onDelete: 'NO ACTION',
    onUpdate: 'CASCADE',
  })
  @JoinTable({
    name: 'voter',
    joinColumn: {
      name: 'votePollId',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'userId',
      referencedColumnName: 'id',
    },
  })
  voterUsers?: UserEntity[];

  @ApiProperty({
    type: 'string',
    format: 'uuid',
    readOnly: true,
    required: false,
    nullable: true,
    default: null,
  })
  @Column({ nullable: true, default: null })
  winnerCandidateId?: string;

  @ApiProperty({
    type: () => Candidate,
    readOnly: true,
    required: false,
    nullable: true,
  })
  @ManyToOne(() => Candidate, (candidate) => candidate.wanVotePolls, {
    onDelete: 'RESTRICT',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'winnerCandidateId', referencedColumnName: 'id' })
  winnerCandidate?: Candidate;

  @ApiProperty({
    type: () => VotePollFptp,
    readOnly: true,
    required: false,
  })
  @OneToOne(() => VotePollFptp, (votePollFptp) => votePollFptp.votePoll)
  votePollFptp?: VotePollFptp;

  @ApiProperty({
    type: 'string',
  })
  @Index({ unique: true })
  @Column()
  title: string;

  @ApiProperty({
    type: 'string',
    default: 'slugify(title)',
  })
  @Index({ unique: true })
  @Column()
  slug: string;

  // now only supported 'fptp' First Pass The Post
  @ApiProperty({
    type: 'enum',
    enum: VotingModules,
    default: VotingModules.fptp,
    required: false,
  })
  @DbAwareColumn({
    type: 'enum',
    enum: VotingModules,
    default: VotingModules.fptp,
  })
  votingModule: VotingModules;

  @ApiProperty({
    type: 'object',
    nullable: true,
    default: null,
    required: false,
  })
  @DbAwareColumn({
    type: 'json',
    nullable: true,
    default: null,
  })
  votingModuleSetting?: object;

  @ApiProperty({
    type: 'enum',
    enum: VotingSchemes,
    default: VotingSchemes.single,
    required: false,
  })
  @DbAwareColumn({
    type: 'enum',
    enum: VotingSchemes,
    default: VotingSchemes.single,
  })
  votingScheme: VotingSchemes;

  @ApiProperty({
    type: Date,
    nullable: true,
    default: null,
    required: false,
  })
  @Index()
  @DbAwareColumn({
    type: 'timestamptz',
    nullable: true,
    default: null,
  })
  @Type(() => Date)
  publiclyVisibleAt: Date;

  @ApiProperty({
    type: Date,
    nullable: true,
    default: null,
    required: false,
  })
  @DbAwareColumn({
    type: 'timestamptz',
    nullable: true,
    default: null,
  })
  @Type(() => Date)
  votingStartsAt: Date;

  @ApiProperty({
    type: Date,
    nullable: true,
    default: null,
    required: false,
  })
  @DbAwareColumn({
    type: 'timestamptz',
    nullable: true,
  })
  @Type(() => Date)
  votingEndsAt: Date;

  @ApiProperty({
    type: 'string',
    nullable: true,
    default: null,
    required: false,
  })
  @Column({
    type: 'text',
    nullable: true,
  })
  description: string;

  @ApiProperty({
    type: 'string',
    nullable: true,
    default: null,
    required: false,
  })
  @Column({
    type: 'text',
    nullable: true,
  })
  content: string;

  @ApiProperty({
    type: 'string',
    nullable: true,
    default: null,
    required: false,
  })
  @Column({
    nullable: true,
  })
  image: string;

  @ApiProperty({
    type: 'integer',
    nullable: true,
    default: null,
    required: false,
  })
  @Column({
    type: 'int',
    nullable: true,
    default: null,
  })
  minWeight?: number;

  @ApiProperty({
    type: 'integer',
    nullable: true,
    default: null,
    required: false,
  })
  @Column({
    type: 'int',
    nullable: true,
    default: null,
  })
  maxWeight?: number;

  @Exclude({ toPlainOnly: true })
  @Column({
    type: 'int',
    default: 0,
    // select: false,
  })
  _votedCount?: number;

  /**
   * This defines if the incremental VotePoll.votedCount is public
   * during all the vote process.
   */
  @ApiProperty({
    type: 'boolean',
    default: true,
    required: false,
  })
  @Column({
    type: 'boolean',
    default: true,
    // select: false,
  })
  votedCountIsPublic?: boolean;

  @ApiProperty({
    type: 'number',
    nullable: true,
    readOnly: true,
    default: null,
    required: false,
  })
  @Column({
    type: 'int',
    nullable: true,
    default: null,
    // select: false,
  })
  ballotBoxBallotsCount?: number;

  @ApiProperty({
    type: 'number',
    nullable: true,
    readOnly: true,
    default: null,
    required: false,
  })
  @Column({
    type: 'int',
    default: null,
    nullable: true,
    // select: false,
  })
  ballotBoxBlankBallotsCount?: number;

  @ApiProperty({
    type: 'number',
    nullable: true,
    readOnly: true,
    default: null,
    required: false,
  })
  @Column({
    type: 'int',
    nullable: true,
    default: null,
    // select: false,
  })
  ballotBoxSumBallotsWeight?: number;

  @ApiProperty({
    type: 'number',
    nullable: true,
    readOnly: true,
    default: null,
    required: false,
  })
  @Column({
    // type: 'int',
    nullable: true,
    default: null,
    // select: false,
  })
  ballotBoxAvgBallotsWeight?: number;

  @ApiProperty({
    type: 'number',
    nullable: true,
    readOnly: true,
    default: null,
    required: false,
  })
  @Column({
    // type: 'int',
    nullable: true,
    default: null,
    // select: false,
  })
  ballotBoxMedianBallotsWeight?: number;

  @OneToMany(() => VotePollRole, (votePollRole) => votePollRole.votePoll)
  roles: VotePollRole[];

  @ManyToMany(() => UserEntity, (roleUsers) => roleUsers.votePollRoleVotePolls)
  @JoinTable({
    name: 'vote_poll_role',
    joinColumn: { name: 'votePollId' },
    inverseJoinColumn: { name: 'userId' },
  })
  roleUsers: UserEntity[];

  @OneToMany(() => VotePollCert, (votePollCert) => votePollCert.votePoll)
  certs: VotePollCert[];

  @ManyToMany(() => CertToken, (certToken) => certToken.votePolls)
  @JoinTable({
    name: 'vote_poll_cert',
    joinColumn: { name: 'votePollId' },
    inverseJoinColumn: { name: 'certTokenId' },
  })
  certTokens: CertToken[];

  @OneToMany(() => BallotBox, (ballotBox) => ballotBox.votePoll)
  ballotBoxes: BallotBox[];

  /**
   * Hack to have computed values in entity
   * combined with @AfterLoad below
   */
  @ApiProperty({
    type: 'enum',
    enum: VotePollStatus,
    readOnly: true,
    required: false,
  })
  @IsString()
  @IsOptional()
  protected status: VotePollStatus;

  @ApiProperty({
    type: 'int',
    nullable: true,
    default: null,
    readOnly: true,
    required: false,
  })
  @IsString()
  protected votedCount: number;

  @BeforeInsert()
  @BeforeUpdate()
  async setAutoValues() {
    this.slug = autoSlug(this.slug, this.title);
  }

  @AfterLoad()
  @AfterInsert()
  @AfterUpdate()
  async computedData(): Promise<void> {
    const current_timestamp = new Date();
    const now = () => new Date(current_timestamp);

    this.status = VotePollStatus.closed;

    if (new Date(this.publiclyVisibleAt) > now()) {
      this.status = VotePollStatus.pending;
    } else if (
      new Date(this.votingEndsAt) > now() &&
      new Date(this.votingStartsAt) < now()
    ) {
      this.status = VotePollStatus.open;
    } else if (new Date(this.votingEndsAt) <= now()) {
      if (this.ballotBoxBallotsCount === null) {
        this.status = VotePollStatus.counting;
      } else {
        this.status = VotePollStatus.completed;
      }
    }

    if (this.votedCountIsPublic || this.status === VotePollStatus.completed) {
      this.votedCount = this._votedCount;
    } else {
      this.votedCount = null;
    }

    // the real counter is not public in any case
    delete this._votedCount;
  }
}
