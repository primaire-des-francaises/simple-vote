import { ApiProperty } from '@nestjsx/crud/lib/crud';
import { IsDefined, IsUUID } from 'class-validator';
import { UserEntity } from 'src/users/entities/user.entity';
import {
  BaseEntity,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryColumn,
} from 'typeorm';
import { VotePoll } from './vote-poll.entity';

@Entity({ name: 'voter', withoutRowid: true })
export class Voter {
  // @ApiProperty({
  //   type: 'string',
  //   format: 'uuid',
  //   readOnly: true,
  //   required: false,
  // })
  @IsDefined()
  @IsUUID()
  @PrimaryColumn()
  userId: string;

  @ApiProperty({
    type: 'string',
    format: 'uuid',
    readOnly: true,
    required: false,
  })
  @IsDefined()
  @IsUUID()
  @PrimaryColumn()
  votePollId: string;

  // @ApiProperty({
  //   type: () => UserEntity,
  //   readOnly: true,
  //   required: false,
  // })
  @ManyToOne(() => UserEntity, (user) => user.voters, {
    cascade: true,
    onDelete: 'NO ACTION',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'userId', referencedColumnName: 'id' })
  user?: UserEntity;

  @ApiProperty({
    type: () => VotePoll,
    isArray: true,
    readOnly: true,
    required: false,
  })
  @ManyToOne(() => VotePoll, (votePoll) => votePoll.voters, {
    cascade: true,
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'votePollId', referencedColumnName: 'id' })
  votePoll?: VotePoll;
}
