import { ApiProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';
import { autoSlug } from 'src/shared/auto-slug';
import { SoftDeleteTimestampedEntity } from 'src/shared/extended-base-entities';
import { UserEntity } from 'src/users/entities/user.entity';
import {
  BaseEntity,
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { BallotBox } from './ballot-box.entity';
import { Candidate } from './candidate.entity';
import { VotePoll } from './vote-poll.entity';

@Entity({ name: 'participate', withoutRowid: true })
export class Participate extends SoftDeleteTimestampedEntity {
  // @PrimaryGeneratedColumn()
  // public id!: number;

  @PrimaryColumn()
  public votePollId!: string;

  @PrimaryColumn()
  public candidateId!: string;

  @ApiProperty({
    type: 'string',
    format: 'uuid',
    readOnly: true,
    required: false,
  })
  @Column({
    type: 'uuid',
  })
  createdByUserId: string;

  @ManyToOne(() => UserEntity, (user) => user.createdParticipates)
  @JoinColumn({ name: 'createdByUserId' })
  createdByUser: UserEntity;

  @ManyToOne(
    () => VotePoll,
    // (vote) => vote.participates,
    {
      cascade: false, // ['insert', 'update'],
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
    },
  )
  @JoinColumn({ name: 'votePollId', referencedColumnName: 'id' })
  public votePoll!: VotePoll;

  @ManyToOne(
    () => Candidate,
    // (candidate) => candidate.participates,
    {
      cascade: ['insert', 'update'],
      onDelete: 'RESTRICT',
      onUpdate: 'CASCADE',
    },
  )
  @JoinColumn({ name: 'candidateId', referencedColumnName: 'id' })
  public candidate?: Candidate;

  @OneToMany(() => BallotBox, (ballotBox) => ballotBox.participate, {
    createForeignKeyConstraints: false,
  })
  @JoinColumn([
    { name: 'candidateId', referencedColumnName: 'candidateId' },
    { name: 'votePollId', referencedColumnName: 'votePollId' },
  ])
  ballotBoxes: BallotBox[];

  @Column()
  title: string;

  @Index({ unique: true })
  @Column()
  slug: string;

  @Column({ nullable: true })
  image: string;

  @Column({ type: 'text', nullable: true })
  description: string;

  @Column({ type: 'text', nullable: true })
  content: string;

  @Column({
    type: 'int',
    default: null,
    nullable: true,
    // select: false,
  })
  votesCount?: number;

  @Column({
    type: 'int',
    default: null,
    nullable: true,
    // select: false,
  })
  sumBallotsWeight?: number;

  @Column({
    // type: 'int',
    default: null,
    nullable: true,
    // select: false,
  })
  avgBallotsWeight?: number;

  @Column({
    // type: 'int',
    default: null,
    nullable: true,
    // select: false,
  })
  medianBallotsWeight?: number;

  @BeforeInsert()
  @BeforeUpdate()
  async setAutoValues() {
    this.slug = autoSlug(this.slug, this.title);
  }
}
