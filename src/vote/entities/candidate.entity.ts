import { ApiProperty } from '@nestjs/swagger';
import { autoSlug } from 'src/shared/auto-slug';
import { SoftDeleteTimestampedEntity } from 'src/shared/extended-base-entities';
import { UserEntity } from 'src/users/entities/user.entity';
import { VotePoll } from 'src/vote/entities/vote-poll.entity';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { BallotBox } from './ballot-box.entity';
import { CandidateRole } from './candidate-role.entity';
import { Participate } from './participate.entity';

@Entity({ name: 'candidate', withoutRowid: true })
export class Candidate extends SoftDeleteTimestampedEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ApiProperty({
    type: 'string',
    format: 'uuid',
    readOnly: true,
    required: false,
  })
  @Column({
    type: 'uuid',
  })
  createdByUserId: string;

  @ManyToOne(() => UserEntity, (user) => user.createdCandidates)
  @JoinColumn({ name: 'createdByUserId' })
  createdByUser: UserEntity;

  @Index({ unique: true })
  @Column()
  slug: string;

  @Column()
  title: string;

  @Column({ type: 'text', nullable: true })
  description: string;

  @Column({ type: 'text', nullable: true })
  content: string;

  @Column({ nullable: true })
  image: string;

  @OneToMany(() => Participate, (participate) => participate.candidate)
  public participates: Participate[];

  @ManyToMany((type) => VotePoll, (votePoll) => votePoll.candidates)
  votePolls: VotePoll[];

  @OneToMany(() => BallotBox, (ballotBox) => ballotBox.candidate)
  public ballotBoxes: BallotBox[];

  @OneToMany(() => VotePoll, (votePoll) => votePoll.winnerCandidate)
  wanVotePolls: VotePoll[];

  @OneToMany(() => CandidateRole, (candidateRole) => candidateRole.candidate)
  roles: CandidateRole[];

  @ManyToMany(() => UserEntity, (roleUser) => roleUser.candidateRoleCandidates)
  @JoinTable({
    name: 'candidate_role',
    joinColumn: {
      name: 'candidateId',
    },
    inverseJoinColumn: {
      name: 'userId',
    },
  })
  roleUsers: UserEntity;

  @ManyToMany(() => UserEntity, (user) => user.candidates)
  @JoinTable({
    name: 'candidate_role',
    joinColumn: {
      name: 'candidateId',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'userId',
      referencedColumnName: 'id',
    },
  })
  users: UserEntity[];

  @BeforeInsert()
  @BeforeUpdate()
  async setAutoValues() {
    this.slug = autoSlug(this.slug, this.title);
  }
}
