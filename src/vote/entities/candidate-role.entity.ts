import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { DbAwareColumn } from 'src/shared/db-aware-column';
import { TimestampedEntity } from 'src/shared/extended-base-entities';
import { UserEntity } from 'src/users/entities/user.entity';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { Candidate } from './candidate.entity';

export enum EnumCandidateRole {
  owner = 'owner',
  administrator = 'administrator',
  manager = 'manager',
  editor = 'editor',
}

@Entity({ name: 'candidate_role', withoutRowid: true })
export class CandidateRole extends TimestampedEntity {
  @Column({
    type: 'uuid',
    primary: true,
  })
  candidateId: string;

  @Column({
    type: 'uuid',
    primary: true,
  })
  userId: string;

  @ApiProperty({
    type: 'string',
    format: 'uuid',
    readOnly: true,
    required: false,
  })
  @Column({
    type: 'uuid',
  })
  createdByUserId: string;

  @ManyToOne(() => UserEntity, (user) => user.createdCandidateRoles)
  @JoinColumn({ name: 'createdByUserId' })
  createdByUser: UserEntity;

  @DbAwareColumn({
    type: 'timestamptz',
    nullable: true,
    default: null,
  })
  @Type(() => Date)
  expiresAt: Date;

  @DbAwareColumn({
    type: 'enum',
    enum: EnumCandidateRole,
    default: EnumCandidateRole.owner,
  })
  role: EnumCandidateRole;

  @ManyToOne(() => Candidate, (candidate) => candidate.roles, {
    // createForeignKeyConstraints: false,
  })
  @JoinColumn({ name: 'candidateId' })
  candidate: Candidate;

  @ManyToOne(() => UserEntity, (user) => user.candidateRoles, {
    // createForeignKeyConstraints: false,
  })
  @JoinColumn({ name: 'userId' })
  user: UserEntity;
}
