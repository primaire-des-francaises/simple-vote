import { Module } from '@nestjs/common';
import { BallotBoxesService } from './ballot-boxes.service';
import { BallotBox } from '../entities/ballot-box.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { VotePoll } from '../entities/vote-poll.entity';
import { Participate } from '../entities/participate.entity';
import { LocksModule } from 'src/locks/locks.module';
import { VotePollsService } from '../vote-polls/vote-polls.service';
import { VotePollsModule } from '../vote-polls/vote-polls.module';
import { VoteModule } from '../vote.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([BallotBox, VotePoll, Participate]),
    LocksModule,
  ],
  providers: [BallotBoxesService],
  exports: [BallotBoxesService],
})
export class BallotBoxesModule {}
