import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { RegisterVoteBodyDto } from 'src/vote/dto/register-vote.dto';
import { BallotBoxesService } from './ballot-boxes.service';

@Controller('vote/ballot-boxes')
export class BallotBoxesController {
  constructor(private readonly ballotBoxesService: BallotBoxesService) {}

  // @Post()
  // registerVote(@Body() registerVoteBody: RegisterVoteBodyDto) {
  //   return this.ballotBoxesService.registerVote(registerVoteBody);
  // }
}
