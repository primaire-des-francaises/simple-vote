import { Injectable, NotAcceptableException } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { validate, validateOrReject } from 'class-validator';
import {
  RegisterVoteBodyDto,
  RegisterVoteBodySingleDto,
  RegisterVoteBodySingleWeighed3Dto,
} from 'src/vote/dto/register-vote.dto';
import { Connection, EntityManager, Repository } from 'typeorm';
import { BallotBox } from '../entities/ballot-box.entity';
import { Participate } from '../entities/participate.entity';
import {
  VotePoll,
  VotePollStatus,
  VotingSchemes,
} from '../entities/vote-poll.entity';
import { NIL as NIL_UUID } from 'uuid';
import { getOrmconfig } from 'src/shared/db-aware-column';
import { LocksService } from 'src/locks/locks.service';
import { whereVotePollStatus } from '../vote-polls/vote-polls.service';
export interface VotesCount {
  votePoll: VotePoll;
  participates: Participate[];
}
@Injectable()
export class BallotBoxesService {
  constructor(
    @InjectConnection()
    private readonly connection: Connection,
    @InjectRepository(BallotBox)
    private readonly ballotBoxRepository: Repository<BallotBox>,
    @InjectRepository(VotePoll)
    private readonly votePollRepository: Repository<VotePoll>,
    @InjectRepository(Participate)
    private readonly participateRepository: Repository<Participate>,
    private readonly locksService: LocksService,
  ) {}

  public async registerVote(
    registerVote: RegisterVoteBodyDto,
    transactionManager: EntityManager,
  ): Promise<Boolean> {
    // manager: is a transaction passed from the vote service.

    const currentTimestamp = new Date().toISOString();
    const now = () => new Date(currentTimestamp);

    const votePoll = await transactionManager.connection
      .getRepository(VotePoll)
      .findOneOrFail(registerVote.votePollId, {
        where: whereVotePollStatus(VotePollStatus.open, now()),
      });

    switch (votePoll.votingScheme) {
      case VotingSchemes.single:
        let registerVoteSingle = plainToClass(
          RegisterVoteBodySingleDto,
          registerVote,
        );

        await validateOrReject(registerVoteSingle, {
          whitelist: true,
          forbidNonWhitelisted: true,
        });

        if (registerVoteSingle.candidateId === NIL_UUID) {
          registerVoteSingle = {
            votePollId: registerVoteSingle.votePollId,
            candidateId: null,
          };
        }

        const voteBallotBox = await transactionManager
          .getRepository(BallotBox)
          .insert(registerVoteSingle);

        // return voteBallotBox;
        break;
      default:
        throw new NotAcceptableException(
          'Vote schema not implemented',
          'ERROR_VOTE_SCHEMA_NOT_VALID',
        );
    }
    return true;
  }

  private async registerVoteSingleWeighted3(
    registerVote: RegisterVoteBodySingleWeighed3Dto,
    manager: EntityManager,
  ) {
    validateOrReject(registerVote, {
      whitelist: true,
      forbidNonWhitelisted: true,
    });

    const voteBallotBox = await manager
      .getRepository(BallotBox)
      .insert(registerVote);

    return voteBallotBox;
  }

  private async buildQueryMedian(
    votePollId: string,
    candidateId: string = undefined,
  ): Promise<{ sql: string; params: any[] }> {
    let where: string[] = ['NOT(weight IS NULL)'];
    let params: any[] = [];

    switch (getOrmconfig().type) {
      case 'better-sqlite3':
        where.push('"votePollId" = :votePollId');
        params.push({ votePollId, candidateId });

        if (candidateId) {
          where.push(`"candidateId" = :candidateId`);
        }

        break;
      case 'postgres':
        where.push('"votePollId" = $1');
        params.push(votePollId);
        if (candidateId) {
          where.push(`"candidateId" = $2`);
          params.push(candidateId);
        }
        break;
      default:
        throw new NotAcceptableException(
          `DB driver ${getOrmconfig().type} not supported.`,
        );
    }

    const sqlWhere = where.join(' AND ');

    const sql = `
       SELECT AVG("weight") AS "weightMedian"
       FROM (
         SELECT "weight"
         FROM ballot_box
         WHERE ${sqlWhere}
         ORDER BY "weight"
         LIMIT 2 - (
           SELECT COUNT(*)
           FROM ballot_box
           WHERE ${sqlWhere}
         ) % 2    -- odd 1, even 2
         OFFSET (
           SELECT (COUNT(*) - 1) / 2
           FROM ballot_box
           WHERE ${sqlWhere}
         )
       ) AS m
       `;

    return {
      sql,
      params,
    };
  }

  public async votesCount(votePollId): Promise<VotePoll> {
    let result: VotePoll;
    const startedAt = new Date();

    const doVotesCount = async () => {
      result = await this.connection.transaction(async (countTransaction) => {
        /**
         * Here starts the counting transaction
         */

        const votePollCountQuery = countTransaction
          .getRepository(VotePoll)
          .createQueryBuilder('votePoll')
          .where({ id: votePollId })
          .select([
            'votePoll.id',
            'votePoll._votedCount',
            'votePoll.title',
            'votePoll.votingStartsAt',
            'votePoll.votingEndsAt',
            'votePoll.ballotBoxBallotsCount',
            'votePoll.ballotBoxBlankBallotsCount',
            'votePoll.ballotBoxSumBallotsWeight',
            'votePoll.ballotBoxAvgBallotsWeight',
            'votePoll.ballotBoxMedianBallotsWeight',
          ])
          .leftJoin(
            'ballot_box',
            'ballotBoxes',
            'ballotBoxes.votePollId = votePoll.id',
          )
          .leftJoin(
            'candidate',
            'ballotBoxes_candidate',
            'ballotBoxes_candidate.id = ballotBoxes.candidateId',
          )
          .addSelect('COUNT(ballotBoxes.id)', 'votePoll_ballotBoxBallotsCount')
          .addSelect(
            'COUNT(CASE WHEN ballotBoxes_candidate.id IS NULL THEN 1 ELSE NULL END)',
            'votePoll_ballotBoxBlankBallotsCount',
          )
          .addSelect(
            'SUM(ballotBoxes.weight)',
            'votePoll_ballotBoxSumBallotsWeight',
          )
          .addSelect(
            'AVG(ballotBoxes.weight)',
            'votePoll_ballotBoxAvgBallotsWeight',
          )
          .groupBy('votePoll.id')
          .addGroupBy('votePoll.title')
          .addGroupBy('votePoll._votedCount')
          .addGroupBy('votePoll.votingStartsAt')
          .addGroupBy('votePoll.votingEndsAt');

        console.log('votePollCount');
        const votePollCount = await votePollCountQuery.getOneOrFail();

        const votePollParticipatesCountQuery = countTransaction
          .getRepository(VotePoll)
          .createQueryBuilder('votePoll')
          .where({ id: votePollId })
          .select([
            'votePoll.id',
            'votePoll._votedCount',
            'votePoll.title',
            'votePoll.votingStartsAt',
            'votePoll.votingEndsAt',
            'participates.votePollId',
            'participates.candidateId',
            'participates.votesCount',
            'participates.sumBallotsWeight',
            'participates.avgBallotsWeight',
            'participates.medianBallotsWeight',
          ])
          .innerJoin('votePoll.participates', 'participates')
          .leftJoin('participates.ballotBoxes', 'participates_ballotBoxes')

          /**
           * Participate
           */

          .addSelect(
            'COUNT(participates_ballotBoxes.candidateId)',
            'participates_votesCount',
          )
          .addSelect(
            'SUM(participates_ballotBoxes.weight)',
            'participates_sumBallotsWeight',
          )
          .addSelect(
            'AVG(participates_ballotBoxes.weight)',
            'participates_avgBallotsWeight',
          )

          .groupBy('votePoll.id')
          .addGroupBy('votePoll.title')
          .addGroupBy('votePoll._votedCount')
          .addGroupBy('votePoll.votingStartsAt')
          .addGroupBy('votePoll.votingEndsAt')
          .addGroupBy('participates.candidateId, participates.votePollId');
        // .addGroupBy('participates.votePollId');

        // .andWhere('participates.candidateId > participates.votePollId')

        // needs a postgres extension
        if (getOrmconfig().type === 'postgres' && false) {
          votePollParticipatesCountQuery
            .addSelect(
              'MEDIAN(allVotes.weight)',
              'votePoll_ballotBoxMedianBallotsWeight',
            )
            .addSelect(
              'MEDIAN(participates_ballotBoxes.weight)',
              'participates_medianBallotsWeight',
            );
        }

        const votePollParticipatesCount =
          await votePollParticipatesCountQuery.getOneOrFail();

        let { participates } = votePollParticipatesCount;

        for (const participateCount of participates) {
          // async (participateCount) => {
          // just in case cascade was activated ?
          let { ballotBoxes, ...participate } = participateCount;

          /**
           * Calculate per candidate MEDIAN(weight)
           */

          const queryMedianParticipate = await this.buildQueryMedian(
            participate.votePollId,
            participate.candidateId,
          );

          const [participateWeightMedian] = await countTransaction.query(
            queryMedianParticipate.sql,
            queryMedianParticipate.params,
          );

          participate.medianBallotsWeight =
            participateWeightMedian.weightMedian;

          // await countTransaction.getRepository(Participate).save(participate);
          await countTransaction
            .getRepository(Participate)
            .createQueryBuilder()
            .update()
            .where({
              votePollId: participate.votePollId,
              candidateId: participate.candidateId,
            })
            .set({
              votesCount: participate.votesCount,
              sumBallotsWeight: participate.sumBallotsWeight,
              avgBallotsWeight: participate.avgBallotsWeight,
              medianBallotsWeight: participate.medianBallotsWeight,
            })
            .execute();
        }

        /**
         * Calculate VotePoll MEDIAN(weight)
         */

        const queryMedianVotePoll = await this.buildQueryMedian(
          votePollCount.id,
        );

        const [votePollWeightMedian] = await countTransaction.query(
          queryMedianVotePoll.sql,
          queryMedianVotePoll.params,
        );

        votePollCount.ballotBoxMedianBallotsWeight =
          votePollWeightMedian.weightMedian;

        await countTransaction
          .getRepository(VotePoll)
          .createQueryBuilder()
          .update()
          .where({ id: votePollCount.id })
          .set({
            ballotBoxBallotsCount: votePollCount.ballotBoxBallotsCount,
            ballotBoxBlankBallotsCount:
              votePollCount.ballotBoxBlankBallotsCount,
            ballotBoxSumBallotsWeight: votePollCount.ballotBoxSumBallotsWeight,
            ballotBoxAvgBallotsWeight: votePollCount.ballotBoxAvgBallotsWeight,
            ballotBoxMedianBallotsWeight:
              votePollCount.ballotBoxMedianBallotsWeight,
          })
          .execute();

        return votePollParticipatesCount;
      });
    };

    /**
     *  when using Sqlite only one simultaneous transaction is possible on same connection
     */
    if (getOrmconfig().type === 'better-sqlite3') {
      await this.locksService.lock.acquire(
        'connection',
        async () => {
          await doVotesCount();
        },
        {
          timeout: 300 * 1000, // 5min
        },
      );
    } else {
      await doVotesCount();
    }

    const endedAt = new Date();
    const timerMs = endedAt.getTime() - startedAt.getTime();

    // console.debug(result);
    console.log(
      `Finished counting votes for VotePoll[${result.id}] "${
        result.title
      }" at: ${endedAt.toISOString()} (${timerMs / 1000}s)`,
    );
    return result;
  }
}
