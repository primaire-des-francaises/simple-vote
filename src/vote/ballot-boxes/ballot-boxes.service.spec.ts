import { Test, TestingModule } from '@nestjs/testing';
import { BallotBoxesService } from './ballot-boxes.service';

describe('BallotBoxesService', () => {
  let service: BallotBoxesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BallotBoxesService],
    }).compile();

    service = module.get<BallotBoxesService>(BallotBoxesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
