import { Test, TestingModule } from '@nestjs/testing';
import { BallotBoxesController } from './ballot-boxes.controller';
import { BallotBoxesService } from './ballot-boxes.service';

describe('BallotBoxesController', () => {
  let controller: BallotBoxesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BallotBoxesController],
      providers: [BallotBoxesService],
    }).compile();

    controller = module.get<BallotBoxesController>(BallotBoxesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
