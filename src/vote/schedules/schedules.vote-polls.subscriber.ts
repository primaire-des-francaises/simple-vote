import { Inject, Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import {
  EventSubscriber,
  EntitySubscriberInterface,
  InsertEvent,
  Connection,
  Repository,
  UpdateEvent,
  RemoveEvent,
  Not,
  IsNull,
} from 'typeorm';
import { VotePoll } from '../entities/vote-poll.entity';
import { SchedulesService } from './schedules.service';
// @Injectable({ scope: Scope.REQUEST })
@Injectable()
export class ScheduleVotePollsSubscriber
  implements EntitySubscriberInterface<VotePoll>
{
  constructor(
    @InjectConnection() private readonly connection: Connection,
    @InjectRepository(VotePoll)
    private readonly votePollRepository: Repository<VotePoll>,
    @Inject(SchedulesService)
    private readonly schedulesService: SchedulesService, // @Inject() public schedulesService: SchedulesService,
  ) {
    this.connection.subscribers.push(this);

    console.log('Loaded SchedulesVotePollsSubscriber');
  }

  listenTo() {
    return VotePoll;
  }

  async afterInsert(event: InsertEvent<VotePoll>) {
    const votePoll = event.entity;

    await this.onUpdateTrigger(votePoll);
  }

  async afterUpdate(event: UpdateEvent<VotePoll>) {
    const votePoll = event.entity;

    await this.onUpdateTrigger(votePoll);
  }

  async beforeRemove(event: RemoveEvent<VotePoll>) {
    const votePoll = event.entity;

    await this.onDeleteTrigger(votePoll);
  }

  async onInitTrigger() {
    const currentTimestamp = new Date().toISOString();
    const now = () => new Date(currentTimestamp);

    this.votePollRepository.createQueryBuilder().select().where({});
  }

  async onUpdateTrigger(votePoll) {
    /**
     * Schedule or reschedule vote counts when suitable
     */
    const currentTimestamp = new Date().toISOString();
    const now = () => new Date(currentTimestamp);

    /**
     * hack to detect it was soft deleted
     */
    const votePollIsSoftDeleted = await this.votePollRepository
      .createQueryBuilder()
      .select()
      .where({ id: votePoll.id })
      .withDeleted()
      .andWhere({ deletedAt: Not(IsNull()) })
      .getOne();

    if (
      votePoll &&
      !votePollIsSoftDeleted &&
      // we are waiting for voting to end
      (new Date(votePoll.votingEndsAt) >= now() ||
        // voting has ended but count has not update votePoll.ballotBoxBallotsCount
        (new Date(votePoll.votingEndsAt) < now() &&
          votePoll.ballotBoxBallotsCount === null))
    ) {
      // we should schedule a ballotBox count
      return await this.schedulesService.scheduleJob(votePoll);
    } else {
      return await this.schedulesService.cancelJob(votePoll);
    }
  }

  async onDeleteTrigger(votePoll) {
    return await this.schedulesService.cancelJob(votePoll);
  }
}
