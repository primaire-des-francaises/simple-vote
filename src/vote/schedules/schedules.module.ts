import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BallotBoxesModule } from '../ballot-boxes/ballot-boxes.module';
import { VotePoll } from '../entities/vote-poll.entity';
import { VotePollsModule } from '../vote-polls/vote-polls.module';
import { SchedulesService } from './schedules.service';
import { ScheduleVotePollsSubscriber } from './schedules.vote-polls.subscriber';

@Module({
  imports: [
    BallotBoxesModule,
    VotePollsModule,
    TypeOrmModule.forFeature([VotePoll]),
  ],
  providers: [SchedulesService, ScheduleVotePollsSubscriber],
  exports: [SchedulesService],
})
export class SchedulesModule {}
