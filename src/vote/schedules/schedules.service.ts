import { Injectable } from '@nestjs/common';
import * as nodeSchedule from 'node-schedule';
import { BallotBoxesService } from '../ballot-boxes/ballot-boxes.service';
import { VotePoll } from '../entities/vote-poll.entity';
import { VotePollsService } from '../vote-polls/vote-polls.service';

export class ScheduleJob {
  createdAt: Date;
  startAt: Date;
  job: any;
}

export class ScheduleJobs {
  [key: string]: ScheduleJob;
}

@Injectable()
export class SchedulesService {
  constructor(
    private readonly ballotBoxesService: BallotBoxesService,
    private readonly votePollsService: VotePollsService,
  ) {
    // console.debug('Loading SchedulesService');

    (async () => {
      await this.init();
    })();
  }

  private async init() {
    /**
     * At boot check database and add schedulers for ongoing votations
     *
     */
    const initScheduleVotePolls = await this.votePollsService.getVotePolls({
      visibility: 'any',
      status: 'scheduleCount',
    });

    console.log(
      'SchedulesService.init():',
      initScheduleVotePolls.length
        ? `Creating schedule jobs for ${initScheduleVotePolls.length} VotePoll(s)`
        : `No VotePoll need scheduled count`,
    );

    for (const votePoll of initScheduleVotePolls) {
      await this.scheduleJob(votePoll);
    }
  }

  private schedule = nodeSchedule;

  private ballotBoxCountJobs: ScheduleJobs = {}; //;ScheduleJobs;

  public async scheduleJob(
    votePoll: VotePoll,
    notBefore: Date = new Date(new Date().getTime() + 1 * 1000),
  ): Promise<ScheduleJob> {
    let result: boolean = null;

    const previousJobCanceled = await this.cancelJob(votePoll);
    if (previousJobCanceled) {
      console.debug(`Canceled job for "${votePoll.title}"`);
    }

    if (!votePoll.votingEndsAt) {
      console.debug('Not scheduling new job. (votingEndsAt is not defined)');
      return;
    }

    // if dates in the past, schedule at earliest in 10s
    const startAt = new Date(
      Math.max(votePoll.votingEndsAt.getTime(), notBefore.getTime()),
    );

    if (votePoll.votingEndsAt != startAt) {
      console.debug(
        `As scheduled in the past or too soon (${votePoll.votingEndsAt.toUTCString()}) job rescheduled at:`,
        startAt,
      );
    }

    const job = await this.schedule.scheduleJob(
      // {
      //   startTime: date,
      //   // endTime: new Date(date.getTime() + 30 * 1000),
      //   // tz: 'America/La_Paz',
      //   // rule: {
      //   //   second: null,
      //   //   minute: null,
      //   //   hour: 10,
      //   // },
      // },
      startAt,
      async () => {
        /**
         * Executing scheduled job
         */

        console.debug(
          `Trigger count for votePoll[${votePoll.id}] "${
            votePoll.title
          }" startedAt: ${new Date().toISOString()}`,
        );

        await this.ballotBoxesService
          .votesCount(votePoll.id)
          .then((countedVotePoll) => {
            delete this.ballotBoxCountJobs[votePoll.id];
            result = true;

            console.log(
              'Total ballotBoxCountJobs in queue:',
              Object.keys(this.ballotBoxCountJobs).length,
            );
          })
          .catch(async (error) => {
            console.error(error);
            console.error(
              `Error: The scheduled job ballotBoxesService.votesCount(${votePoll.id}) failed. It will be rescheduled in 60s.`,
            );

            delete this.ballotBoxCountJobs[votePoll.id];

            await this.scheduleJob(
              votePoll,
              new Date(new Date().getTime() + 60 * 1000),
            );
          });

        return result;
      },
    );

    if (job) {
      const scheduleJob: ScheduleJob = {
        createdAt: new Date(),
        startAt,
        job,
      };

      this.ballotBoxCountJobs[votePoll.id] = scheduleJob;

      console.debug(
        `Scheduled count job for votePoll "${votePoll.title}" at:`,
        scheduleJob.startAt,
      );

      console.log(
        'Total VotePoll count queued jobs:',
        Object.keys(this.ballotBoxCountJobs).length,
      );

      return scheduleJob;
    }

    return;
  }

  async cancelJob(votePoll: VotePoll) {
    if (votePoll.id in this.ballotBoxCountJobs) {
      console.debug(
        `Canceling counting job for votePollId "${votePoll.title}" at:`,
        new Date(),
      );

      this.ballotBoxCountJobs[votePoll.id].job.cancel();

      // delete key pool[votePollId] on pure array
      // this.ballotBoxCountJobs.splice(this.ballotBoxCountJobs.indexOf(votePoll.id), 1);

      delete this.ballotBoxCountJobs[votePoll.id];

      console.log(
        'Total VotePoll count queued jobs:',
        Object.keys(this.ballotBoxCountJobs).length,
      );
      return true;
    }

    // we did nothing
    return false;
  }

  async ScheduleJobs(): Promise<ScheduleJobs> {
    return await this.ballotBoxCountJobs;
  }
}
