import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Crud, CrudController, Feature } from '@nestjsx/crud';
import { AppGuards } from 'src/app.guards';
import { Candidate } from '../entities/candidate.entity';
import { CandidatesService } from './candidates.service';
import { CreateCandidateDto } from './dto/create-candidate.dto';
import { UpdateCandidateDto } from './dto/update-candidate.dto';

@ApiTags('vote')
@Controller('vote/candidates')
@Feature('candidates')
@Crud({
  model: {
    type: Candidate,
  },
  query: {
    join: {
      candidate: {},
      vote: {},
    },
  },
  params: {
    id: {
      field: 'id',
      type: 'uuid',
      primary: true,
    },
  },
})
@ApiBearerAuth('access-token')
@UseGuards(...AppGuards)
export class CandidatesController implements CrudController<Candidate> {
  constructor(public readonly service: CandidatesService) {}

  get base(): CrudController<Candidate> {
    return this;
  }

  // @Post()
  // create(@Body() createCandidateDto: CreateCandidateDto) {
  //   return this.candidatesService.create(createCandidateDto);
  // }

  // @Get()
  // findAll() {
  //   return this.candidatesService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.candidatesService.findOne(+id);
  // }

  // @Patch(':id')
  // update(
  //   @Param('id') id: string,
  //   @Body() updateCandidateDto: UpdateCandidateDto,
  // ) {
  //   return this.candidatesService.update(+id, updateCandidateDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.candidatesService.remove(+id);
  // }
}
