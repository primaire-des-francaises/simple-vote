import { Module } from '@nestjs/common';
import { CandidatesService } from './candidates.service';
import { CandidatesController } from './candidates.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Candidate } from '../entities/candidate.entity';

@Module({
  controllers: [CandidatesController],
  providers: [CandidatesService],
  imports: [TypeOrmModule.forFeature([Candidate])],
  exports: [CandidatesService],
})
export class CandidatesModule {}
