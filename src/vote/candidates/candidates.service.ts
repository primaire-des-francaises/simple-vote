import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CrudRequest } from '@nestjsx/crud';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Repository } from 'typeorm';
import { Candidate } from '../entities/candidate.entity';
import { Participate } from '../entities/participate.entity';
import { CreateCandidateDto } from './dto/create-candidate.dto';
import { UpdateCandidateDto } from './dto/update-candidate.dto';

@Injectable()
export class CandidatesService extends TypeOrmCrudService<Candidate> {
  constructor(
    @InjectRepository(Candidate)
    readonly repo: Repository<Candidate>,
  ) {
    super(repo);
  }

  async deleteOne(crudRequest: CrudRequest) {
    const myEntity = await this.getOneOrFail(crudRequest);
    return this.repo.softRemove(myEntity);
  }

  // create(createCandidateDto: CreateCandidateDto) {
  //   return 'This action adds a new candidate';
  // }

  // findAll() {
  //   return `This action returns all candidates`;
  // }

  // findOne(id: number) {
  //   return `This action returns a #${id} candidate`;
  // }

  // update(id: number, updateCandidateDto: UpdateCandidateDto) {
  //   return `This action updates a #${id} candidate`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} candidate`;
  // }
}
