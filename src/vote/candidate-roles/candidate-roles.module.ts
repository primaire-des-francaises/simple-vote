import { Module } from '@nestjs/common';
import { CandidateRolesService } from './candidate-roles.service';
import { CandidateRolesController } from './candidate-roles.controller';
import { CandidateRole } from '../entities/candidate-role.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  controllers: [CandidateRolesController],
  providers: [CandidateRolesService],
  imports: [TypeOrmModule.forFeature([CandidateRole])],
})
export class CandidateRolesModule {}
