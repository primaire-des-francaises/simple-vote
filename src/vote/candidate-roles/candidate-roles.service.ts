import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CrudRequest } from '@nestjsx/crud';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Repository } from 'typeorm';
import { CandidateRole } from '../entities/candidate-role.entity';
import { CreateCandidateRoleDto } from './dto/create-candidate-role.dto';
import { UpdateCandidateRoleDto } from './dto/update-candidate-role.dto';

@Injectable()
export class CandidateRolesService extends TypeOrmCrudService<CandidateRole> {
  constructor(
    @InjectRepository(CandidateRole)
    readonly repo: Repository<CandidateRole>,
  ) {
    super(repo);
  }

  async deleteOne(crudRequest: CrudRequest) {
    const myEntity = await this.getOneOrFail(crudRequest);
    return this.repo.softRemove(myEntity);
  }
}
