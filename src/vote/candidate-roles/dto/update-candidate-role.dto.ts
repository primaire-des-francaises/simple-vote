import { PartialType } from '@nestjs/swagger';
import { CreateCandidateRoleDto } from './create-candidate-role.dto';

export class UpdateCandidateRoleDto extends PartialType(CreateCandidateRoleDto) {}
