import { Test, TestingModule } from '@nestjs/testing';
import { CandidateRolesController } from './candidate-roles.controller';
import { CandidateRolesService } from './candidate-roles.service';

describe('CandidateRolesController', () => {
  let controller: CandidateRolesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CandidateRolesController],
      providers: [CandidateRolesService],
    }).compile();

    controller = module.get<CandidateRolesController>(CandidateRolesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
