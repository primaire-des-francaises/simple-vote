import { Test, TestingModule } from '@nestjs/testing';
import { CandidateRolesService } from './candidate-roles.service';

describe('CandidateRolesService', () => {
  let service: CandidateRolesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CandidateRolesService],
    }).compile();

    service = module.get<CandidateRolesService>(CandidateRolesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
