import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';
import { AppGuards } from 'src/app.guards';
import { CandidateRole } from '../entities/candidate-role.entity';
import { CandidateRolesService } from './candidate-roles.service';
import { CreateCandidateRoleDto } from './dto/create-candidate-role.dto';
import { UpdateCandidateRoleDto } from './dto/update-candidate-role.dto';

@ApiTags('vote')
@Controller('vote/candidate-roles')
@Crud({
  model: {
    type: CandidateRole,
  },
  params: {
    candidateId: {
      field: 'candidateId',
      type: 'uuid',
      primary: true,
    },
    userId: {
      field: 'userId',
      type: 'uuid',
      primary: true,
    },
  },
  query: {
    join: {
      candidates: {},
      users: {},
    },
  },
})
@ApiBearerAuth('access-token')
@UseGuards(...AppGuards)
export class CandidateRolesController implements CrudController<CandidateRole> {
  constructor(public readonly service: CandidateRolesService) {}

  get base(): CrudController<CandidateRole> {
    return this;
  }
}
