import {
  Injectable,
  NotAcceptableException,
  ServiceUnavailableException,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectConnection } from '@nestjs/typeorm';
import { validate } from 'class-validator';
import { AuthRequestUser } from 'src/auth/interfaces/auth-request.interface';
import { BallotBoxesService } from 'src/vote/ballot-boxes/ballot-boxes.service';
import {
  VotingSchemes,
  VotePoll,
  VotePollStatus,
} from 'src/vote/entities/vote-poll.entity';
import { Voter } from 'src/vote/entities/voter.entity';
import {
  VotePollsService,
  whereVotePollStatus,
} from 'src/vote/vote-polls/vote-polls.service';
import { VotersService } from 'src/vote/voters/voters.service';
import {
  Brackets,
  Connection,
  IsNull,
  LessThanOrEqual,
  MoreThanOrEqual,
  SelectQueryBuilder,
  UpdateQueryBuilder,
} from 'typeorm';
import { RegisterVoteBodyDto } from './dto/register-vote.dto';
import { getOrmconfig } from 'src/shared/db-aware-column';
import { LocksService } from 'src/locks/locks.service';

@Injectable()
export class VoteService {
  constructor(
    @InjectConnection()
    private readonly connection: Connection,
    private readonly votersService: VotersService,
    private readonly votePollsService: VotePollsService,
    private readonly ballotBoxesService: BallotBoxesService,
    private readonly locksService: LocksService,
  ) {}

  // public readonly openVotePollWhereQb = async (
  //   qb: SelectQueryBuilder<VotePoll> | UpdateQueryBuilder<VotePoll>,
  //   atDate: Date = new Date(),
  // ) => {
  //   const atTimestamp = atDate.toISOString().split('T').join(' ');

  //   qb.andWhere({
  //     votingStartsAt: LessThanOrEqual(atTimestamp),
  //   }).andWhere(
  //     new Brackets((whereBracket) => {
  //       whereBracket
  //         .where({
  //           votingEndsAt: MoreThanOrEqual(atTimestamp),
  //         })
  //         .orWhere({ votingEndsAt: IsNull() });
  //     }),
  //   );
  // };

  public async registerVote(
    requestUser: AuthRequestUser,
    registerVote: RegisterVoteBodyDto,
  ) {
    const currentTimestamp = new Date().toISOString();
    const now = () => new Date(currentTimestamp);

    // this only checks for presence of votePollId<uuid>
    await validate(registerVote, {
      stopAtFirstError: true,
    }).then((err) => {
      if (err.length) {
        throw new NotAcceptableException(err, 'ERROR_VOTE_NOT_VALID');
      }
    });

    const [openVotePoll] = await this.votePollsService.getVotePolls({
      votePollId: registerVote.votePollId,
      visibility: 'any',
      userId: requestUser.id,
      status: VotePollStatus.open,
    });

    if (!openVotePoll) {
      throw new NotAcceptableException(
        'No vote poll matches the vote.',
        'ERROR_VOTE_POLL_NOT_VALID',
      );
    }

    /**
     * TODO: Here we should check if user meets openVotePoll.voteRequirements
     */

    if (
      await this.votersService.hasVoted({
        userId: requestUser.id,
        votePollId: registerVote.votePollId,
      })
    ) {
      const msg = 'USER_HAS_ALREADY_VOTED';
      throw new NotAcceptableException(
        msg,
        'User has already voted fot that vote poll.',
      );
    }

    const doVoteTransaction = async () => {
      await this.connection.transaction(async (voteTransaction) => {
        /**
         * Here starts the vote transaction
         */

        /**
         * Register voter => user as hasVoted()
         */
        try {
          const registerVoter = await voteTransaction
            .createQueryBuilder()
            .insert()
            .into(Voter)
            .values({
              votePollId: registerVote.votePollId,
              userId: requestUser.id,
            })
            .execute();
        } catch (error) {
          const err = 'ERROR_FAILED_REGISTER_VOTER';

          throw new ServiceUnavailableException(
            'Failed to register the User as Voter. Please try again later.',
            err,
          );
        }

        try {
          const votedCountIncrement = voteTransaction
            .createQueryBuilder()
            .update(VotePoll)
            .set({ _votedCount: () => '"_votedCount" + 1' })
            .where({ id: registerVote.votePollId })
            .andWhere(whereVotePollStatus(VotePollStatus.open, now()));

          // Increment the VotePoll global _votedCount
          await votedCountIncrement.execute();
        } catch (error) {
          const err = 'ERROR_FAILED_INCREMENT_VOTE';
          console.error(error, err);

          throw new ServiceUnavailableException(
            'Failed to increment the total vote counter. Please try again later.',
            err,
          );
        }

        switch (openVotePoll.votingScheme) {
          case VotingSchemes.single:
            await this.ballotBoxesService.registerVote(
              registerVote,
              voteTransaction,
            );
            break;
          default:
            throw new NotAcceptableException(
              'Vote schema not implemented',
              'ERROR_VOTE_SCHEMA_NOT_VALID',
            );
        }
      });
    };

    /**
     *  when using Sqlite only one simultaneous transaction is possible on same connection
     */
    if (getOrmconfig().type === 'better-sqlite3') {
      await this.locksService.lock.acquire(
        'connection',
        async () => {
          await doVoteTransaction();
        },
        {
          timeout: 5 * 1000,
        },
      );
    } else {
      await doVoteTransaction();
    }
  }
}
