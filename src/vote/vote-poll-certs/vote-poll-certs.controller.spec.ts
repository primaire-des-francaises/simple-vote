import { Test, TestingModule } from '@nestjs/testing';
import { VotePollCertsController } from './vote-poll-certs.controller';
import { VotePollCertsService } from './vote-poll-certs.service';

describe('VotePollCertsController', () => {
  let controller: VotePollCertsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [VotePollCertsController],
      providers: [VotePollCertsService],
    }).compile();

    controller = module.get<VotePollCertsController>(VotePollCertsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
