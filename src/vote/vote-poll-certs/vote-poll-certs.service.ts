import { Injectable } from '@nestjs/common';
import { CreateVotePollCertDto } from './dto/create-vote-poll-cert.dto';
import { UpdateVotePollCertDto } from './dto/update-vote-poll-cert.dto';

@Injectable()
export class VotePollCertsService {
  create(createVotePollCertDto: CreateVotePollCertDto) {
    return 'This action adds a new votePollCert';
  }

  findAll() {
    return `This action returns all votePollCerts`;
  }

  findOne(id: number) {
    return `This action returns a #${id} votePollCert`;
  }

  update(id: number, updateVotePollCertDto: UpdateVotePollCertDto) {
    return `This action updates a #${id} votePollCert`;
  }

  remove(id: number) {
    return `This action removes a #${id} votePollCert`;
  }
}
