import { Module } from '@nestjs/common';
import { VotePollCertsService } from './vote-poll-certs.service';
import { VotePollCertsController } from './vote-poll-certs.controller';

@Module({
  controllers: [VotePollCertsController],
  providers: [VotePollCertsService]
})
export class VotePollCertsModule {}
