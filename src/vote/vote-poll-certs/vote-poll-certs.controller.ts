import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { VotePollCertsService } from './vote-poll-certs.service';
import { CreateVotePollCertDto } from './dto/create-vote-poll-cert.dto';
import { UpdateVotePollCertDto } from './dto/update-vote-poll-cert.dto';

@Controller('vote-poll-certs')
export class VotePollCertsController {
  constructor(private readonly votePollCertsService: VotePollCertsService) {}

  @Post()
  create(@Body() createVotePollCertDto: CreateVotePollCertDto) {
    return this.votePollCertsService.create(createVotePollCertDto);
  }

  @Get()
  findAll() {
    return this.votePollCertsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.votePollCertsService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateVotePollCertDto: UpdateVotePollCertDto) {
    return this.votePollCertsService.update(+id, updateVotePollCertDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.votePollCertsService.remove(+id);
  }
}
