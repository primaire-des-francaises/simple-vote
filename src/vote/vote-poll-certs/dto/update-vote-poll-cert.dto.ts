import { PartialType } from '@nestjs/swagger';
import { CreateVotePollCertDto } from './create-vote-poll-cert.dto';

export class UpdateVotePollCertDto extends PartialType(CreateVotePollCertDto) {}
