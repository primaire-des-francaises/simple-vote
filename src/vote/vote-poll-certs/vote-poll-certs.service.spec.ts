import { Test, TestingModule } from '@nestjs/testing';
import { VotePollCertsService } from './vote-poll-certs.service';

describe('VotePollCertsService', () => {
  let service: VotePollCertsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [VotePollCertsService],
    }).compile();

    service = module.get<VotePollCertsService>(VotePollCertsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
