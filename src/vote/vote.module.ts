import { Module } from '@nestjs/common';
import { LocksModule } from 'src/locks/locks.module';
import { BallotBoxesModule } from 'src/vote/ballot-boxes/ballot-boxes.module';
import { VotersModule } from 'src/vote/voters/voters.module';
import { VotePollsModule } from './vote-polls/vote-polls.module';
import { VoteController } from './vote.controller';
import { VoteService } from './vote.service';
import { VotePollRolesModule } from './vote-poll-roles/vote-poll-roles.module';
import { VotePollCertsModule } from './vote-poll-certs/vote-poll-certs.module';

@Module({
  controllers: [VoteController],
  providers: [VoteService],
  imports: [VotePollsModule, VotersModule, BallotBoxesModule, LocksModule, VotePollRolesModule, VotePollCertsModule],
  exports: [VoteService],
})
export class VoteModule {}
