import { Test, TestingModule } from '@nestjs/testing';
import { VotePollRolesService } from './vote-poll-roles.service';

describe('VotePollRolesService', () => {
  let service: VotePollRolesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [VotePollRolesService],
    }).compile();

    service = module.get<VotePollRolesService>(VotePollRolesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
