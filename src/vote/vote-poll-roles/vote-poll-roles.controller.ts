import { Controller, UseGuards } from '@nestjs/common';
import { VotePollRolesService } from './vote-poll-roles.service';
import { CreateVotePollRoleDto } from './dto/create-vote-poll-role.dto';
import { UpdateVotePollRoleDto } from './dto/update-vote-poll-role.dto';
import { ApiBearerAuth, ApiProperty, ApiTags } from '@nestjs/swagger';
import { Crud, CrudController, Feature } from '@nestjsx/crud';
import { AppGuards } from 'src/app.guards';
import { VotePollRole } from '../entities/vote-poll-role.entity';
import { CreateVotePollDto } from '../vote-polls/dto/create-vote-poll.dto';
import { UpdateVotePollDto } from '../vote-polls/dto/update-vote-poll.dto';

@Controller('vote/poll-roles')
@Feature('vote-poll-roles')
@ApiTags('vote')
@Crud({
  model: {
    type: VotePollRole,
  },
  dto: {
    create: CreateVotePollRoleDto,
    update: UpdateVotePollRoleDto,
  },
  params: {
    id: {
      field: 'id',
      type: 'uuid',
      primary: true,
    },
  },
  query: {
    join: {
      // voteFptp: {
      //   eager: true,
      // },
      votePoll: {},
      user: {},
    },
  },
})
@ApiBearerAuth('access-token')
@UseGuards(...AppGuards)
export class VotePollRolesController implements CrudController<VotePollRole> {
  constructor(public readonly service: VotePollRolesService) {}

  // @Post()
  // create(@Body() createVotePollRoleDto: CreateVotePollRoleDto) {
  //   return this.votePollRolesService.create(createVotePollRoleDto);
  // }

  // @Get()
  // findAll() {
  //   return this.votePollRolesService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.votePollRolesService.findOne(+id);
  // }

  // @Patch(':id')
  // update(
  //   @Param('id') id: string,
  //   @Body() updateVotePollRoleDto: UpdateVotePollRoleDto,
  // ) {
  //   return this.votePollRolesService.update(+id, updateVotePollRoleDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.votePollRolesService.remove(+id);
  // }
}
