import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Repository } from 'typeorm';
import { VotePollRole } from '../entities/vote-poll-role.entity';
import { CreateVotePollRoleDto } from './dto/create-vote-poll-role.dto';
import { UpdateVotePollRoleDto } from './dto/update-vote-poll-role.dto';

@Injectable()
export class VotePollRolesService extends TypeOrmCrudService<VotePollRole> {
  constructor(
    @InjectRepository(VotePollRole)
    readonly repo: Repository<VotePollRole>,
  ) {
    super(repo);
  }

  // create(createVotePollRoleDto: CreateVotePollRoleDto) {
  //   return 'This action adds a new votePollRole';
  // }

  // findAll() {
  //   return `This action returns all votePollRoles`;
  // }

  // findOne(id: number) {
  //   return `This action returns a #${id} votePollRole`;
  // }

  // update(id: number, updateVotePollRoleDto: UpdateVotePollRoleDto) {
  //   return `This action updates a #${id} votePollRole`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} votePollRole`;
  // }
}
