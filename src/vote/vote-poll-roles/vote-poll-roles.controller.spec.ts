import { Test, TestingModule } from '@nestjs/testing';
import { VotePollRolesController } from './vote-poll-roles.controller';
import { VotePollRolesService } from './vote-poll-roles.service';

describe('VotePollRolesController', () => {
  let controller: VotePollRolesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [VotePollRolesController],
      providers: [VotePollRolesService],
    }).compile();

    controller = module.get<VotePollRolesController>(VotePollRolesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
