import { PartialType } from '@nestjs/swagger';
import { CreateVotePollRoleDto } from './create-vote-poll-role.dto';

export class UpdateVotePollRoleDto extends PartialType(CreateVotePollRoleDto) {}
