import { Module } from '@nestjs/common';
import { VotePollRolesService } from './vote-poll-roles.service';
import { VotePollRolesController } from './vote-poll-roles.controller';
import { VotePollRole } from '../entities/vote-poll-role.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([VotePollRole])],
  controllers: [VotePollRolesController],
  providers: [VotePollRolesService],
})
export class VotePollRolesModule {}
