import { Body, Controller, Post, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiProperty, ApiTags } from '@nestjs/swagger';
import { Action, Feature } from '@nestjsx/crud';
import { AppGuards } from 'src/app.guards';
import { AuthRequest } from 'src/auth/interfaces/auth-request.interface';
import { UserReq } from 'src/auth/user-req.decorator';
import {
  RegisterVoteBodyDto,
  RegisterVoteBodySingleDto,
} from './dto/register-vote.dto';
import { VoteService } from './vote.service';

@ApiTags('vote')
@Controller('vote')
@Feature('vote')
@ApiBearerAuth('access-token')
@UseGuards(...AppGuards)
export class VoteController {
  constructor(private readonly service: VoteService) {}

  @ApiProperty({ type: RegisterVoteBodyDto })
  @Feature('register-vote')
  @Action('create-one')
  @Post('register')
  // @UsePipes(new ValidationPipe({ transform: true }))
  async registerVote(
    @Req() req: AuthRequest,
    @Body()
    registerVoteBody: RegisterVoteBodyDto,
  ) {
    return await this.service.registerVote(req.user, registerVoteBody);
  }

  // @ApiProperty({
  //   type: RegisterVoteBodySingleDto,
  // })
  // @Feature('vote-single')
  // @Action('createOne')
  // @Post('single')
  // async registerVoteSingle(
  //   @Req() req: AuthRequest,
  //   @Body()
  //   registerVoteBody: RegisterVoteBodySingleDto,
  // ) {
  //   return await this.service.registerVote(
  //     req.user,
  //     <RegisterVoteBodyDto>registerVoteBody,
  //   );
  // }
}
