import { Optional } from '@nestjs/common';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsNotEmpty,
  IsEmail,
  IsDefined,
  IsUUID,
  IsInstance,
  ValidateNested,
  IsArray,
  IsOptional,
  IsInt,
  Max,
  Min,
  ValidateIf,
} from 'class-validator';

export class WeighedCandidateBase {
  /**
   * The candidateId
   */
  @ApiProperty({
    type: 'string',
    format: 'uuid',
    required: true,
  })
  @IsDefined()
  @IsNotEmpty()
  @IsUUID()
  readonly candidateId: string;
}
export class WeighedCandidate3 extends WeighedCandidateBase {
  @ApiProperty({
    type: 'integer',
    required: true,
    minimum: 1,
    maximum: 3,
  })
  @IsDefined()
  @IsNotEmpty()
  @IsInt()
  @Min(1)
  @Max(3)
  readonly weight: number;
}

export class WeighedCandidate5 extends WeighedCandidateBase {
  @ApiProperty({
    type: 'integer',
    required: true,
    minimum: 1,
    maximum: 5,
  })
  @IsDefined()
  @IsNotEmpty()
  @IsInt()
  @Min(1)
  @Max(5)
  readonly weight: number;
}

export class WeighedCandidate7 extends WeighedCandidateBase {
  @ApiProperty({
    type: 'integer',
    required: true,
    minimum: 1,
    maximum: 7,
  })
  @IsDefined()
  @IsNotEmpty()
  @IsInt()
  @Min(1)
  @Max(7)
  readonly weight: number;
}

export class RegisterVoteBodyBaseDto {
  @ApiProperty({
    type: 'string',
    format: 'uuid',
    required: true,
  })
  @IsDefined()
  @IsNotEmpty()
  @IsUUID()
  readonly votePollId!: string;
}
export class RegisterVoteBodyDto extends RegisterVoteBodyBaseDto {
  @ApiProperty({
    type: 'string',
    format: 'uuid',
    nullable: true,
    required: false,
  })
  @Optional()
  @IsUUID()
  candidateId?: string;

  @ApiProperty({
    type: WeighedCandidate3,
    nullable: true,
    required: false,
  })
  @Optional()
  w3candidate?: WeighedCandidate3;

  @ApiProperty({
    type: WeighedCandidate5,
    nullable: true,
    required: false,
  })
  @Optional()
  w5candidate?: WeighedCandidate5;

  @ApiProperty({
    type: WeighedCandidate7,
    nullable: true,
    required: false,
  })
  @Optional()
  w7candidate?: WeighedCandidate7;

  @ApiProperty({
    type: WeighedCandidate3,
    isArray: true,
    nullable: true,
    required: false,
    minItems: 1,
  })
  @Optional()
  w3candidates?: WeighedCandidate3[];

  @ApiProperty({
    type: WeighedCandidate5,
    isArray: true,
    nullable: true,
    required: false,
    minItems: 1,
  })
  @Optional()
  w5candidates?: WeighedCandidate5[];

  @ApiProperty({
    type: WeighedCandidate7,
    isArray: true,
    nullable: true,
    required: false,
    minItems: 1,
  })
  @Optional()
  w7candidates?: WeighedCandidate7[];
}

export class RegisterVoteBodyWeighed3Dto extends RegisterVoteBodyBaseDto {
  @IsDefined()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => WeighedCandidate3)
  readonly w3Candidates: WeighedCandidate3[];
}

export class RegisterVoteBodyWeighed5Dto extends RegisterVoteBodyBaseDto {
  @IsDefined()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => WeighedCandidate3)
  readonly w5Candidates: WeighedCandidate5[];
}

export class RegisterVoteBodyWeighed7Dto extends RegisterVoteBodyBaseDto {
  @IsDefined()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => WeighedCandidate3)
  readonly w7Candidates: WeighedCandidate7[];
}

export class RegisterVoteBodySingleDto extends RegisterVoteBodyBaseDto {
  @ApiProperty({
    type: 'string',
    format: 'uuid',
  })
  @IsDefined({ message: 'candidateId is required.' })
  @IsUUID()
  readonly candidateId: string;
}

export class RegisterVoteBodySingleWeighed3Dto extends RegisterVoteBodyBaseDto {
  @IsDefined({
    // message: 'candidateId is required.'
  })
  @IsUUID()
  readonly w3candidate!: WeighedCandidate3;
}

export class RegisterVoteBodySingleWeighed5Dto extends RegisterVoteBodyBaseDto {
  @IsDefined({
    // message: 'candidateId is required.'
  })
  @IsUUID()
  readonly w5candidate!: WeighedCandidate5;
}

export class RegisterVoteBodySingleWeighed7Dto extends RegisterVoteBodyDto {
  @IsDefined({
    // message: 'candidateId is required.'
  })
  @IsUUID()
  readonly w7candidate!: WeighedCandidate7;
}
