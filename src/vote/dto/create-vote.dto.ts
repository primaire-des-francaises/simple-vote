import { IsDefined, IsUUID, ValidateNested } from 'class-validator';

export class CreateVoteDto {
  @IsDefined()
  @IsUUID()
  votePollId!: string;
}

// export class CreateVoteSingleDto {
//   votePollId!: string;
//   candidateId!: string;
// }

// export class CreateVoteWeighted {
//   votePollId!: string;

//   @ValidateNested()
//   candidates: WeightedCandidate[];
// }
