import { Controller, UseGuards } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { AppGuards } from 'src/app.guards';

@Controller('voters')
@ApiBearerAuth('access-token')
@UseGuards(...AppGuards)
export class VotersController {
  // constructor(public readonly votersService: VotersService) {}
}
