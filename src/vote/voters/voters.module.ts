import { Module } from '@nestjs/common';
import { VotersService } from './voters.service';
import { VotersController } from './voters.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Voter } from '../entities/voter.entity';

@Module({
  controllers: [VotersController],
  providers: [VotersService],
  imports: [TypeOrmModule.forFeature([Voter])],
  exports: [VotersService],
})
export class VotersModule {}
