import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Voter } from '../entities/voter.entity';

@Injectable()
export class VotersService {
  constructor(
    @InjectRepository(Voter)
    readonly voterRepository: Repository<Voter>,
  ) {}

  async hasVoted(voter: Voter): Promise<Boolean> {
    const hasVoted = await this.voterRepository.findOne(voter);

    const result: Boolean = hasVoted === undefined ? false : true;

    return result;
  }
}
