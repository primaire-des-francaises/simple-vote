import { Test, TestingModule } from '@nestjs/testing';
import { VotePollsController } from './vote-polls.controller';
import { VotePollsService } from './vote-polls.service';

describe('VotesController', () => {
  let controller: VotePollsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [VotePollsController],
      providers: [VotePollsService],
    }).compile();

    controller = module.get<VotePollsController>(VotePollsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
