import { Injectable, NotAcceptableException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CrudRequest } from '@nestjsx/crud';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { getEnumKeyByEnumValue } from 'src/shared/utils';
import {
  IsNull,
  LessThanOrEqual,
  MoreThanOrEqual,
  Repository,
  Not,
  Brackets,
  MoreThan,
} from 'typeorm';
import { VotePoll, VotePollStatus } from '../entities/vote-poll.entity';

export enum VotePollJoin {
  candidates = 'VotePoll.candidates',
  participates = 'VotePoll.participates',
  candidate = 'participates.candidate',
}

@Injectable()
export class VotePollsService extends TypeOrmCrudService<VotePoll> {
  constructor(
    @InjectRepository(VotePoll)
    readonly repo: Repository<VotePoll>,
  ) {
    super(repo);
  }

  // enables soft delete
  async deleteOne(crudRequest: CrudRequest) {
    const myEntity = await this.getOneOrFail(crudRequest);
    return this.repo.softRemove(myEntity);
  }

  async getVotePolls({
    select,
    userId,
    votePollId,
    join,
    where,
    visibility = 'public',
    status = [VotePollStatus.open],
    atDate = new Date(),
    orderBy,
    limit,
    skip,
  }: {
    readonly select?: any;
    readonly userId?: string;
    readonly votePollId?: string;
    readonly join?: VotePollJoin[];
    readonly where?: Brackets | Brackets[];
    readonly visibility?: 'public' | 'private' | 'any';
    readonly status?:
      | (VotePollStatus | 'any' | 'scheduleCount')
      | VotePollStatus[];
    readonly atDate?: Date;
    readonly orderBy?: {
      sort: string;
      order?: 'ASC' | 'DESC';
      nulls?: 'NULLS FIRST' | 'NULLS LAST';
    }[];
    readonly limit?: number;
    readonly skip?: number;
  }): Promise<VotePoll[]> {
    const atTimestamp = atDate.toISOString().split('T').join(' '); // Sqlite doesn't like the T

    if (status === 'scheduleCount') {
      // ScheduleVotePollsSubscriber needs to list votePolls 'any' but doesn't provide a userId
      // TODO: find a solution for internal calls to bypass auth requirement
    } else if (['private', 'any'].includes(visibility) && !userId) {
      // if asking for private VotePolls we need a userId
      throw new NotAcceptableException(
        'getVotePoll including private visibility requires a userId',
      );
    }

    const qb = this.repo.createQueryBuilder('VotePoll');

    if (select) {
      qb.select(select);
    } else {
      qb.select();
    }

    if (where) {
      qb.where(where);
    } else {
      qb.where('TRUE');
    }

    if (votePollId) {
      qb.andWhere({ id: votePollId });
    }

    if (join) {
      for (const j of join) {
        if (Object.values(VotePollJoin).includes(j)) {
          qb.leftJoinAndSelect(j, getEnumKeyByEnumValue(VotePollJoin, j));
        } else {
          throw new NotAcceptableException('VotePollJoin not acceptable');
        }
      }
    }

    switch (visibility) {
      case null:
      case undefined:
      case 'public':
        qb.andWhere({
          publiclyVisibleAt: LessThanOrEqual(atTimestamp),
        });
        break;
      case 'private':
        qb.andWhere({
          publiclyVisibleAt: IsNull(),
        });
        break;
      case 'any':
        break;
      default:
        throw new NotAcceptableException(
          `votePollsService.getOpenVotePolls() not acceptable argument visibility: ${visibility}`,
        );
    }

    if (status) {
      const statusOrWhere = [];
      const statusArray = Array.isArray(status) ? status : [status];

      for (const state of statusArray) {
        statusOrWhere.push(whereVotePollStatus(state));
      }

      if (statusOrWhere.length) {
        qb.andWhere(
          new Brackets((qbOr) => {
            qbOr.where('FALSE');

            for (const where of statusOrWhere) {
              qbOr.orWhere(where);
            }
          }),
        );
      }
    }

    /**
     * Check user rights to see this votePoll
     */
    if (userId) {
      qb.leftJoin('VotePoll.certTokens', 'certTokens');
      qb.leftJoin('certTokens.certifications', 'certifications');
      qb.leftJoin('VotePoll.roles', 'roles');

      qb.andWhere(
        new Brackets((q) => {
          // VotePoll is public
          q.where({
            publiclyVisibleAt: LessThanOrEqual(atTimestamp),
          })

            // or user is certified
            // => relates Certification (VotePoll.certTokens.certifications.userId = :userId)
            .orWhere(
              new Brackets((qc) => {
                qc.where('certifications.userId = :userId', {
                  userId,
                })

                  // certToken hasn't expired
                  .andWhere(
                    new Brackets((qct) => {
                      qct
                        .where('certTokens.expiresAt > :atTimestamp', {
                          atTimestamp,
                        })
                        .orWhere('certTokens.expiresAt IS NULL');
                    }),
                  )

                  // and certification hasn't expired
                  .andWhere(
                    new Brackets((qce) => {
                      qce
                        .where('certifications.expiresAt > :atTimestamp', {
                          atTimestamp,
                        })
                        .orWhere('certifications.expiresAt IS NULL');
                    }),
                  );
              }),
            )

            // or has a VotePollRole
            .orWhere(
              new Brackets((qr) => {
                qr.where('roles.userId = :userId', {
                  userId,
                })

                  // VotePollRole hasn't expired
                  .andWhere(
                    new Brackets((qre) => {
                      qre
                        .where('roles.expiresAt > :atTimestamp', {
                          atTimestamp,
                        })
                        .orWhere('roles.expiresAt IS NULL');
                    }),
                  );
              }),
            );
        }),
      );
    }

    if (orderBy) {
      for (const { sort, order, nulls } of orderBy) {
        if (sort && order && nulls) {
          qb.addOrderBy(sort, order, nulls);
        } else if (sort && order) {
          qb.addOrderBy(sort, order);
        } else if (sort) {
          qb.addOrderBy(sort);
        } else {
          console.warn(`Warning:  missing orderBy.sort? Should not happen! ó`);
        }
      }
    }

    if (limit) {
      qb.limit(limit);
    }

    // as we are using joins we want to use skip and not offset
    if (skip) {
      qb.skip(skip);
    }

    return await qb.getMany();
  }
}

export const whereVotePollStatus = (
  votePollStatus: VotePollStatus | 'scheduleCount' | 'any',
  atDate: Date = new Date(),
): Brackets => {
  const atTimestamp = atDate.toISOString().split('T').join(' ');

  return new Brackets((qb) => {
    switch (votePollStatus) {
      case 'any':
        qb.where('TRUE');
        break;

      case 'scheduleCount':
        qb.where({
          ballotBoxBallotsCount: IsNull(),
          votingStartsAt: Not(IsNull()),
          votingEndsAt: Not(IsNull()),
        });
        break;

      case VotePollStatus.open:
        qb.where({
          votingStartsAt: LessThanOrEqual(atTimestamp),
        }).andWhere(
          new Brackets((andWhere) => {
            andWhere
              .where({
                votingEndsAt: MoreThanOrEqual(atTimestamp),
              })
              .orWhere({ votingEndsAt: IsNull() });
          }),
        );
        break;

      case VotePollStatus.counting:
        qb.where({
          ballotBoxBallotsCount: IsNull(),
          votingEndsAt: LessThanOrEqual(atTimestamp),
          votingStartsAt: LessThanOrEqual(atTimestamp),
        });
        break;

      case VotePollStatus.completed:
        qb.where({
          ballotBoxBallotsCount: Not(IsNull()),
          votingStartsAt: LessThanOrEqual(atTimestamp),
          votingEndsAt: LessThanOrEqual(atTimestamp),
        });
        break;

      case VotePollStatus.pending:
        qb.where({
          votingStartsAt: MoreThan(atTimestamp),
        });
        break;

      case VotePollStatus.closed:
        qb.where({ votingStartsAt: IsNull() });
        break;

      default:
        throw new NotAcceptableException(`Not acceptable VotePollStatus`);
    }
  });
};
