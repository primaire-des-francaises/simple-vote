import { Test, TestingModule } from '@nestjs/testing';
import { VotePollsService } from './vote-polls.service';

describe('VotePollsService', () => {
  let service: VotePollsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [VotePollsService],
    }).compile();

    service = module.get<VotePollsService>(VotePollsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
