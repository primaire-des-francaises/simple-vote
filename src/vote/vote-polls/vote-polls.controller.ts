import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  SerializeOptions,
} from '@nestjs/common';
import { VotePollsService } from './vote-polls.service';

import { isAdmin } from 'src/shared/utils';
import { SCondition } from '@nestjsx/crud-request';
import { UserEntity } from 'src/users/entities/user.entity';
import { AppGuards } from 'src/app.guards';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { VotePoll } from '../entities/vote-poll.entity';
import { CreateVotePollDto } from './dto/create-vote-poll.dto';
import { UpdateVotePollDto } from './dto/update-vote-poll.dto';
import { Crud, CrudController, Feature } from '@nestjsx/crud';

@Controller('vote/polls')
@Feature('vote-polls')
@ApiTags('vote')
// this can be useful to hide internals
// need to check it works on entire controller
@SerializeOptions({
  excludePrefixes: ['_'],
  groups: ['admin'],
})
@Crud({
  model: {
    type: VotePoll,
  },
  dto: {
    create: CreateVotePollDto,
    update: UpdateVotePollDto,
  },
  params: {
    id: {
      field: 'id',
      type: 'uuid',
      primary: true,
    },
  },
  query: {
    join: {
      voteFptp: {
        eager: true,
      },
      candidates: {},
      participates: {},
      'participates.candidate': {},
    },
  },
})
// @CrudAuth({
//   property: 'user',
//   filter: (user: UserEntity): SCondition => {
//     // filter: (req: any): SCondition => {
//     console.log('user', user);
//     if (user && isAdmin(user)) {
//       return;
//     }

//     // const userHasPrivilege = await getRepository().findOne

//     return;
//     // TODO: serialize according to rights
//     // return {
//     //   id: user.id,
//     //   deletedAt: null,
//     // };
//   },
// })
@ApiBearerAuth('access-token')
@UseGuards(...AppGuards)
export class VotePollsController implements CrudController<VotePoll> {
  constructor(public readonly service: VotePollsService) {}

  get base(): CrudController<VotePoll> {
    return this;
  }

  // @Post()
  // create(@Body() createVoteDto: CreateVoteDto) {
  //   return this.votesService.create(createVoteDto);
  // }

  // @Get()
  // findAll() {
  //   return this.votesService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.votesService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateVoteDto: UpdateVoteDto) {
  //   return this.votesService.update(+id, updateVoteDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.votesService.remove(+id);
  // }
}
