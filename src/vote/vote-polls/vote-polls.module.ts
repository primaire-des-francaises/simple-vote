import { Module } from '@nestjs/common';
import { VotePollsService } from './vote-polls.service';
import { VotePollsController } from './vote-polls.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BallotBoxesModule } from '../ballot-boxes/ballot-boxes.module';
import { CandidateRolesModule } from '../candidate-roles/candidate-roles.module';
import { CandidatesModule } from '../candidates/candidates.module';
import { VotePoll } from '../entities/vote-poll.entity';
import { ParticipatesModule } from '../participates/participates.module';
import { VotersModule } from '../voters/voters.module';
import { VotePollFptpsModule } from 'src/vote-poll-fptps/vote-poll-fptps.module';

@Module({
  controllers: [VotePollsController],
  providers: [VotePollsService],
  imports: [
    TypeOrmModule.forFeature([VotePoll]),
    CandidatesModule,
    CandidateRolesModule,
    ParticipatesModule,
    VotersModule,
    BallotBoxesModule,
    VotePollFptpsModule,
  ],
  exports: [VotePollsService],
})
export class VotePollsModule {}
