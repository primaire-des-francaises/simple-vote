import { PartialType } from '@nestjs/swagger';
import { CreateVotePollDto } from './create-vote-poll.dto';

export class UpdateVotePollDto extends PartialType(CreateVotePollDto) {}
