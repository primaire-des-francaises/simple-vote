import { PartialType } from '@nestjs/swagger';
import { VotePoll } from 'src/vote/entities/vote-poll.entity';

export class CreateVotePollDto extends PartialType(VotePoll) {}
