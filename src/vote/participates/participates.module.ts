import { Module } from '@nestjs/common';
import { ParticipatesService } from './participates.service';
import { ParticipatesController } from './participates.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Participate } from '../entities/participate.entity';

@Module({
  controllers: [ParticipatesController],
  providers: [ParticipatesService],
  imports: [TypeOrmModule.forFeature([Participate])],
  exports: [ParticipatesService],
})
export class ParticipatesModule {}
