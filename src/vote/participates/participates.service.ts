import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CrudRequest } from '@nestjsx/crud';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Repository } from 'typeorm';
import { Participate } from '../entities/participate.entity';
import { CreateParticipateDto } from './dto/create-participate.dto';
import { UpdateParticipateDto } from './dto/update-participate.dto';

@Injectable()
export class ParticipatesService extends TypeOrmCrudService<Participate> {
  constructor(
    @InjectRepository(Participate)
    readonly repo: Repository<Participate>,
  ) {
    super(repo);
  }

  async deleteOne(crudRequest: CrudRequest) {
    const myEntity = await this.getOneOrFail(crudRequest);
    return this.repo.softRemove(myEntity);
  }

  // create(createParticipateDto: CreateParticipateDto) {
  //   return 'This action adds a new participate';
  // }

  // findAll() {
  //   return `This action returns all participates`;
  // }

  // findOne(id: number) {
  //   return `This action returns a #${id} participate`;
  // }

  // update(id: number, updateParticipateDto: UpdateParticipateDto) {
  //   return `This action updates a #${id} participate`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} participate`;
  // }
}
