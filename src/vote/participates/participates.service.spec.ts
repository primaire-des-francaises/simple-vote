import { Test, TestingModule } from '@nestjs/testing';
import { ParticipatesService } from './participates.service';

describe('ParticipatesService', () => {
  let service: ParticipatesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ParticipatesService],
    }).compile();

    service = module.get<ParticipatesService>(ParticipatesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
