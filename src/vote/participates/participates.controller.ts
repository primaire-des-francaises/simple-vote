import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { ParticipatesService } from './participates.service';
import { CreateParticipateDto } from './dto/create-participate.dto';
import { UpdateParticipateDto } from './dto/update-participate.dto';
import {
  CreateManyDto,
  Crud,
  CrudController,
  CrudRequest,
  Feature,
  GetManyDefaultResponse,
} from '@nestjsx/crud';
import { Participate } from '../entities/participate.entity';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AppGuards } from 'src/app.guards';

// @Controller('votes')
// @Feature('votes')
// @Crud({
//   model: {
//     type: Vote,
//   },
//   query: {
//     join: {
//       voteFptp: {},
//     },
//   },
// })
// export class VotesController implements CrudController<Vote> {
//   constructor(public readonly service: VotesService) {}

//   get base(): CrudController<Vote> {
//     return this;
//   }
// }

@ApiTags('vote')
@Crud({
  model: {
    type: Participate,
  },
  query: {
    join: {
      candidate: {},
      vote: {},
    },
  },
  params: {
    candidateId: {
      field: 'candidateId',
      type: 'uuid',
      primary: true,
    },
    votePollId: {
      field: 'votePollId',
      type: 'uuid',
      primary: true,
    },
  },
})
@Controller('vote/participates')
@Feature('participates')
@ApiBearerAuth('access-token')
@UseGuards(...AppGuards)
export class ParticipatesController implements CrudController<Participate> {
  constructor(public readonly service: ParticipatesService) {}

  get base(): CrudController<Participate> {
    return this;
  }

  // @Post()
  // create(@Body() createParticipateDto: CreateParticipateDto) {
  //   return this.participatesService.create(createParticipateDto);
  // }

  // @Get()
  // findAll() {
  //   return this.participatesService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.participatesService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateParticipateDto: UpdateParticipateDto) {
  //   return this.participatesService.update(+id, updateParticipateDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.participatesService.remove(+id);
  // }
}
