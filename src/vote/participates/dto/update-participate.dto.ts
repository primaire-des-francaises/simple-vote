import { PartialType } from '@nestjs/swagger';
import { CreateParticipateDto } from './create-participate.dto';

export class UpdateParticipateDto extends PartialType(CreateParticipateDto) {}
