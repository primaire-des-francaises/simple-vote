import { Test, TestingModule } from '@nestjs/testing';
import { ParticipatesController } from './participates.controller';
import { ParticipatesService } from './participates.service';

describe('ParticipatesController', () => {
  let controller: ParticipatesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ParticipatesController],
      providers: [ParticipatesService],
    }).compile();

    controller = module.get<ParticipatesController>(ParticipatesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
