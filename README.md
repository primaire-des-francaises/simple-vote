# Simple Voting REST api

## Story bord

Basé sur le prétexte du besoin d'une solution permettant un vote pour des primaires, faire un prototype de plateforme de votation.

Ceci est bien un prototype. Il n'a pas pour objectif de fournir une solution totalement infalsifiable, ou du moins dans un premier temps. L'objectif premier étant d'avoir une solution toutefois fiable mais rapidement opérationnelle. Donc une solution simple comme son nom l'indique.

- facile à mettre en oeuvre et qu'elle puisse fonctionner de base avec un minimum de prérequis.

  D'où le choix qu'il fonctionne avec SqLite _ou Postgresql, pour être un minimum sérieux ;)_

  SqLite facilite le développement mais aussi le mise en oeuvre, cette solution est tout à fait capable de gérer une votation à l'échelle du million et serait bien suffisante pour de nombreuses situations en permettant un déploiement simple avec pour seule prérequis nodejs. (Ex: une entreprise, une clinique, un hospital ou une commune peut le faire tourner sur l'ordinateur de la secrétaire. On peut imaginer exporter un bundle basé sur electronjs.org et n'avoir que ça à installer pour pouvoir s'en servir.)

- sûre

  - garantir l'anonymisation des votes. Prérequis absolut, qu'il ne soit en aucun cas possible de savoir qui a voté quoi.

  cela n'empêche pas de rajouter des modules qui eux en le garantissent pas, mais que le _core_ de l'application ne stocke pas le contenu du vote. Le _core_ est en charge de s'assurer que un utilisateur _(User)_ ne peut voter qu'une seule fois. L'authentification de l'utilisateur est basée du la librairie Passport qui permet de facilement s'authentifier auprès d'un tiers (Facebook, Google, FranceConnect, etc..) mais aussi pour des utilisateur dit locaux, _(càd sans nécessiter un tiers)_. On notera que le le core nécessite uniquement un _username_ donc pas d'email obligatoire. Certains modules pourraient demander un email ou un numéro de téléphone comme base de validation.

  L'authentification de la connection est basée sur le standard Bearer (Javascript Web Token) qui permet entre autre de pouvoir vérifier l'ip de l'utilisateur sans jamais la stocker à aucun moment coté serveur.

```bash
# create a demo user
curl -X POST -H "Content-Type: application/json" --data '{"username":"demo123", "password":"demo456"}' http://localhost:3000/auth/register

# login
curl -X POST -H "Content-Type: application/json" --data '{"username":"demo123", "password":"demo456"}' http://localhost:3000/auth/login
```

```json
{
  "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOnsidG9rZW5JZCI6IjQ4OTg5MmIwLTBmMDMtNDcyNC04MzNhLTllY2NmYjkxZjRkMSIsInVzZXJJZCI6ImE0MmZmZjhmLTNkNjUtNDc4ZC05ZTkwLTNlZWZiYjJjNGQzYSIsImlwIjoiOjpmZmZmOjEyNy4wLjAuMSJ9LCJpYXQiOjE2MzMwNTU1ODEsImV4cCI6MTYzMzA1Njc4MSwiYXVkIjoibG9jYWxob3N0IiwiaXNzIjoibG9jYWxob3N0In0.Wfjh6lso9A0PJJYFHAS5pdvZ73bMfvcPfwppK2-elcY",
  "createdAt": "2021-10-01T02:33:01.332Z",
  "expiresIn": 1200,
  "expiresAt": "1970-01-19T21:37:36.781Z"
}
```

```bash
# this will give you a non expire token by setting expiresIn = null
export TOKEN=$(curl -s -X POST -H "Content-Type: application/json" --data '{"username":"demo123", "password":"demo456", "expiresIn": null}' http://localhost:3000/auth/login | grep -oP '(?<="accessToken":")[^"]*')
echo $TOKEN
```

```bash
# authenticate
curl -X GET -H "Authorization: Bearer $TOKEN" http://localhost:3000/users
```

```bash
# ping aka extend session
curl -X POST -H "Authorization: Bearer $TOKEN" http://localhost:3000/auth/ping
```

```bash
# logout
curl -X POST -H "Authorization: Bearer $TOKEN" http://localhost:3000/auth/logout
```

To request on CRUD endpoints from frontend you want to look at this

```js
import { RequestQueryBuilder } from '@nestjsx/crud-request';

const queryString = RequestQueryBuilder.create({
  fields: ['name', 'email'],
  search: { isActive: true },
  join: [{ field: 'company' }],
  sort: [{ field: 'id', order: 'DESC' }],
  page: 1,
  limit: 25,
  resetCache: true,
}).query();
```

https://github.com/nestjsx/crud/wiki/Requests

## Quick start

```bash
git clone https://gitlab.com/primaire-des-francaises/simple-vote.git
# alternatively use ssh
# git clone git@gitlab.com:primaire-des-francaises/simple-vote.git

cd simple-vote
npm i

cp src/jwtconfig.example.ts src/jwtconfig.ts
cp ormconfig.example.dev.js ormconfig.js

npm run start:dev
```

then checkout api documentation (swagger)

http://localhost:3000/api/

## DB Schema

### Condensed

<img src="./doc/images/simple-vote-db.png">

---

### Detailed

<img src="./doc/images/simple-vote-db-full.png">

```bash
# generated using schemaspy https://schemaspy.org/
java -jar ~/schemaspy-6.1.0.jar -t pgsql11 -s public -vizjs -dp ~/postgresql-42.2.18.jar -db simple_vote -host localhost -u dbUser -p dbPassword -o ./tmp/schemaspy
```

---

<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest

  <p align="center">A progressive <a href="http://nodejs.org" target="_blank">Node.js</a> framework for building efficient and scalable server-side applications.</p>
    <p align="center">
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/v/@nestjs/core.svg" alt="NPM Version" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/l/@nestjs/core.svg" alt="Package License" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/dm/@nestjs/common.svg" alt="NPM Downloads" /></a>
<a href="https://circleci.com/gh/nestjs/nest" target="_blank"><img src="https://img.shields.io/circleci/build/github/nestjs/nest/master" alt="CircleCI" /></a>
<a href="https://coveralls.io/github/nestjs/nest?branch=master" target="_blank"><img src="https://coveralls.io/repos/github/nestjs/nest/badge.svg?branch=master#9" alt="Coverage" /></a>
<a href="https://discord.gg/G7Qnnhy" target="_blank"><img src="https://img.shields.io/badge/discord-online-brightgreen.svg" alt="Discord"/></a>
<a href="https://opencollective.com/nest#backer" target="_blank"><img src="https://opencollective.com/nest/backers/badge.svg" alt="Backers on Open Collective" /></a>
<a href="https://opencollective.com/nest#sponsor" target="_blank"><img src="https://opencollective.com/nest/sponsors/badge.svg" alt="Sponsors on Open Collective" /></a>
  <a href="https://paypal.me/kamilmysliwiec" target="_blank"><img src="https://img.shields.io/badge/Donate-PayPal-ff3f59.svg"/></a>
    <a href="https://opencollective.com/nest#sponsor"  target="_blank"><img src="https://img.shields.io/badge/Support%20us-Open%20Collective-41B883.svg" alt="Support us"></a>
  <a href="https://twitter.com/nestframework" target="_blank"><img src="https://img.shields.io/twitter/follow/nestframework.svg?style=social&label=Follow"></a>
</p>
  <!--[![Backers on Open Collective](https://opencollective.com/nest/backers/badge.svg)](https://opencollective.com/nest#backer)
  [![Sponsors on Open Collective](https://opencollective.com/nest/sponsors/badge.svg)](https://opencollective.com/nest#sponsor)-->

## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Kamil Myśliwiec](https://kamilmysliwiec.com)
- Website - [https://nestjs.com](https://nestjs.com/)
- Twitter - [@nestframework](https://twitter.com/nestframework)

## License

Nest is [MIT licensed](LICENSE).
