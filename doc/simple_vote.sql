--
-- PostgreSQL database dump
--

-- Dumped from database version 13.4 (Ubuntu 13.4-1.pgdg20.04+1)
-- Dumped by pg_dump version 13.4 (Ubuntu 13.4-1.pgdg20.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


--
-- Name: candidate_role_role_enum; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.candidate_role_role_enum AS ENUM (
    'owner',
    'administrator',
    'manager',
    'editor'
);


--
-- Name: cert_role_role_enum; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.cert_role_role_enum AS ENUM (
    'owner',
    'administrator',
    'certifier',
    'none'
);


--
-- Name: user_role_enum; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.user_role_enum AS ENUM (
    'user',
    'admin',
    'superAdmin'
);


--
-- Name: vote_poll_role_role_enum; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.vote_poll_role_role_enum AS ENUM (
    'owner',
    'administrator',
    'manager',
    'editor',
    'voter'
);


--
-- Name: vote_poll_votingmodule_enum; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.vote_poll_votingmodule_enum AS ENUM (
    'fptp'
);


--
-- Name: vote_poll_votingscheme_enum; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.vote_poll_votingscheme_enum AS ENUM (
    'single',
    'singleWeighed3',
    'singleWeighed5',
    'singleWeighed7',
    'multiWeighed3',
    'multiWeighed5',
    'multiWeighed7'
);


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: auth_token; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.auth_token (
    "createdAt" timestamp with time zone DEFAULT now() NOT NULL,
    "updatedAt" timestamp with time zone DEFAULT now() NOT NULL,
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "userId" uuid NOT NULL,
    "userAgent" json NOT NULL,
    "createdFromIp" character varying NOT NULL,
    "expiresAt" timestamp with time zone
);


--
-- Name: ballot_box; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ballot_box (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "votePollId" uuid NOT NULL,
    "candidateId" uuid NOT NULL,
    weight integer,
    "createdDate" date DEFAULT now() NOT NULL,
    "updatedAt" timestamp with time zone
);


--
-- Name: candidate; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.candidate (
    "createdAt" timestamp with time zone DEFAULT now() NOT NULL,
    "updatedAt" timestamp with time zone DEFAULT now() NOT NULL,
    "deletedAt" timestamp with time zone,
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "createdByUserId" uuid NOT NULL,
    slug character varying NOT NULL,
    title character varying NOT NULL,
    description text,
    content text,
    image character varying
);


--
-- Name: candidate_role; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.candidate_role (
    "candidateId" uuid NOT NULL,
    "userId" uuid NOT NULL,
    "createdAt" timestamp with time zone DEFAULT now() NOT NULL,
    "updatedAt" timestamp with time zone DEFAULT now() NOT NULL,
    "createdByUserId" uuid NOT NULL,
    "expiresAt" timestamp with time zone,
    role public.candidate_role_role_enum DEFAULT 'owner'::public.candidate_role_role_enum NOT NULL
);


--
-- Name: cert_role; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.cert_role (
    "userId" uuid NOT NULL,
    "certTokenId" uuid NOT NULL,
    "createdAt" timestamp with time zone DEFAULT now() NOT NULL,
    "updatedAt" timestamp with time zone DEFAULT now() NOT NULL,
    "createdByUserId" uuid NOT NULL,
    role public.cert_role_role_enum DEFAULT 'none'::public.cert_role_role_enum NOT NULL
);


--
-- Name: cert_token; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.cert_token (
    "createdAt" timestamp with time zone DEFAULT now() NOT NULL,
    "updatedAt" timestamp with time zone DEFAULT now() NOT NULL,
    "deletedAt" timestamp with time zone,
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "expiresAt" timestamp with time zone,
    "usableCount" integer DEFAULT 1,
    salt character varying NOT NULL,
    "isRevocable" boolean DEFAULT true NOT NULL,
    "isEditable" boolean DEFAULT true NOT NULL,
    "certificateIsRevocable" boolean DEFAULT true NOT NULL,
    "createdByUserId" uuid NOT NULL
);


--
-- Name: cert_token_closure; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.cert_token_closure (
    "ancestorCertTokenId" uuid NOT NULL,
    "descendantCertTokenId" uuid NOT NULL
);


--
-- Name: certification; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.certification (
    "userId" uuid NOT NULL,
    "certTokenId" uuid NOT NULL,
    "createdAt" timestamp with time zone DEFAULT now() NOT NULL,
    "updatedAt" timestamp with time zone DEFAULT now() NOT NULL,
    "createdByUserId" uuid NOT NULL,
    "expiresAt" timestamp with time zone
);


--
-- Name: participate; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.participate (
    "votePollId" uuid NOT NULL,
    "candidateId" uuid NOT NULL,
    "createdAt" timestamp with time zone DEFAULT now() NOT NULL,
    "updatedAt" timestamp with time zone DEFAULT now() NOT NULL,
    "deletedAt" timestamp with time zone,
    "createdByUserId" uuid NOT NULL,
    title character varying NOT NULL,
    slug character varying NOT NULL,
    image character varying,
    description text,
    content text,
    "votesCount" integer,
    "sumBallotsWeight" integer,
    "avgBallotsWeight" integer,
    "medianBallotsWeight" integer
);


--
-- Name: user; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."user" (
    "createdAt" timestamp with time zone DEFAULT now() NOT NULL,
    "updatedAt" timestamp with time zone DEFAULT now() NOT NULL,
    "deletedAt" timestamp with time zone,
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    username character varying NOT NULL,
    email character varying,
    role public.user_role_enum DEFAULT 'user'::public.user_role_enum NOT NULL,
    "isActive" boolean DEFAULT true NOT NULL,
    password character varying NOT NULL
);


--
-- Name: user_profile; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_profile (
    "createdAt" timestamp with time zone DEFAULT now() NOT NULL,
    "updatedAt" timestamp with time zone DEFAULT now() NOT NULL,
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "userId" uuid NOT NULL,
    name character varying NOT NULL
);


--
-- Name: vote_poll; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.vote_poll (
    "createdAt" timestamp with time zone DEFAULT now() NOT NULL,
    "updatedAt" timestamp with time zone DEFAULT now() NOT NULL,
    "deletedAt" timestamp with time zone,
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    "createdByUserId" uuid NOT NULL,
    "winnerCandidateId" uuid,
    title character varying NOT NULL,
    slug character varying NOT NULL,
    "votingModule" public.vote_poll_votingmodule_enum DEFAULT 'fptp'::public.vote_poll_votingmodule_enum NOT NULL,
    "votingModuleSetting" json,
    "votingScheme" public.vote_poll_votingscheme_enum DEFAULT 'single'::public.vote_poll_votingscheme_enum NOT NULL,
    "publiclyVisibleAt" timestamp with time zone,
    "votingStartsAt" timestamp with time zone,
    "votingEndsAt" timestamp with time zone,
    description text,
    content text,
    image character varying,
    "minWeight" integer,
    "maxWeight" integer,
    "_votedCount" integer DEFAULT 0 NOT NULL,
    "votedCountIsPublic" boolean DEFAULT true NOT NULL,
    "ballotBoxBallotsCount" integer,
    "ballotBoxBlankBallotsCount" integer,
    "ballotBoxSumBallotsWeight" integer,
    "ballotBoxAvgBallotsWeight" integer,
    "ballotBoxMedianBallotsWeight" integer
);


--
-- Name: vote_poll_cert; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.vote_poll_cert (
    "votePollId" uuid NOT NULL,
    "certTokenId" uuid NOT NULL,
    "createdAt" timestamp with time zone DEFAULT now() NOT NULL,
    "updatedAt" timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: vote_poll_fptp; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.vote_poll_fptp (
    "createdAt" timestamp with time zone DEFAULT now() NOT NULL,
    "updatedAt" timestamp with time zone DEFAULT now() NOT NULL,
    "deletedAt" timestamp with time zone,
    "votePollId" uuid NOT NULL
);


--
-- Name: vote_poll_role; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.vote_poll_role (
    "votePollId" uuid NOT NULL,
    "userId" uuid NOT NULL,
    "createdAt" timestamp with time zone DEFAULT now() NOT NULL,
    "updatedAt" timestamp with time zone DEFAULT now() NOT NULL,
    "createdByUserId" uuid NOT NULL,
    "expiresAt" timestamp with time zone,
    role public.vote_poll_role_role_enum DEFAULT 'voter'::public.vote_poll_role_role_enum NOT NULL
);


--
-- Name: voter; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.voter (
    "userId" uuid NOT NULL,
    "votePollId" uuid NOT NULL
);


--
-- Name: vote_poll PK_0d7459852150cf964af26adcf63; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.vote_poll
    ADD CONSTRAINT "PK_0d7459852150cf964af26adcf63" PRIMARY KEY (id);


--
-- Name: candidate_role PK_17c2cfe49770e1d1ee04ede66ea; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.candidate_role
    ADD CONSTRAINT "PK_17c2cfe49770e1d1ee04ede66ea" PRIMARY KEY ("candidateId", "userId");


--
-- Name: vote_poll_cert PK_2a7de2f7ee79e45bf009aff7f93; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.vote_poll_cert
    ADD CONSTRAINT "PK_2a7de2f7ee79e45bf009aff7f93" PRIMARY KEY ("votePollId", "certTokenId");


--
-- Name: participate PK_3706444338ea98f6bba91c9a2ed; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.participate
    ADD CONSTRAINT "PK_3706444338ea98f6bba91c9a2ed" PRIMARY KEY ("votePollId", "candidateId");


--
-- Name: auth_token PK_4572ff5d1264c4a523f01aa86a0; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.auth_token
    ADD CONSTRAINT "PK_4572ff5d1264c4a523f01aa86a0" PRIMARY KEY (id);


--
-- Name: cert_token PK_6cd824c9df7fe9ae47108e05451; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cert_token
    ADD CONSTRAINT "PK_6cd824c9df7fe9ae47108e05451" PRIMARY KEY (id);


--
-- Name: vote_poll_fptp PK_7368456ebd29df19d4fc909ee23; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.vote_poll_fptp
    ADD CONSTRAINT "PK_7368456ebd29df19d4fc909ee23" PRIMARY KEY ("votePollId");


--
-- Name: candidate PK_b0ddec158a9a60fbc785281581b; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.candidate
    ADD CONSTRAINT "PK_b0ddec158a9a60fbc785281581b" PRIMARY KEY (id);


--
-- Name: voter PK_be8c947c07a8351e06b09cb5b39; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.voter
    ADD CONSTRAINT "PK_be8c947c07a8351e06b09cb5b39" PRIMARY KEY ("userId", "votePollId");


--
-- Name: cert_role PK_c0504e0504cc316edee81704a59; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cert_role
    ADD CONSTRAINT "PK_c0504e0504cc316edee81704a59" PRIMARY KEY ("userId", "certTokenId");


--
-- Name: user PK_cace4a159ff9f2512dd42373760; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY (id);


--
-- Name: cert_token_closure PK_e47ebcaa07308abbbab8f0e49cc; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cert_token_closure
    ADD CONSTRAINT "PK_e47ebcaa07308abbbab8f0e49cc" PRIMARY KEY ("ancestorCertTokenId", "descendantCertTokenId");


--
-- Name: certification PK_ec24206cfd948d43fdc238ad4c5; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.certification
    ADD CONSTRAINT "PK_ec24206cfd948d43fdc238ad4c5" PRIMARY KEY ("userId", "certTokenId");


--
-- Name: ballot_box PK_f36646e818660ffc80d3fee69c5; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ballot_box
    ADD CONSTRAINT "PK_f36646e818660ffc80d3fee69c5" PRIMARY KEY (id);


--
-- Name: user_profile PK_f44d0cd18cfd80b0fed7806c3b7; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_profile
    ADD CONSTRAINT "PK_f44d0cd18cfd80b0fed7806c3b7" PRIMARY KEY (id);


--
-- Name: vote_poll_role PK_fc6a459b8b61d28151d686d97e8; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.vote_poll_role
    ADD CONSTRAINT "PK_fc6a459b8b61d28151d686d97e8" PRIMARY KEY ("votePollId", "userId");


--
-- Name: user UQ_78a916df40e02a9deb1c4b75edb; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "UQ_78a916df40e02a9deb1c4b75edb" UNIQUE (username);


--
-- Name: user UQ_e12875dfb3b1d92d7d7c5377e22; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE (email);


--
-- Name: IDX_13a13c546de40c5b2cb42cdda3; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IDX_13a13c546de40c5b2cb42cdda3" ON public.participate USING btree ("candidateId");


--
-- Name: IDX_1c79aa5394045515fb4cdd50ec; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX "IDX_1c79aa5394045515fb4cdd50ec" ON public.user_profile USING btree (name);


--
-- Name: IDX_308a9c20d213d4aea362fccf1f; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX "IDX_308a9c20d213d4aea362fccf1f" ON public.vote_poll USING btree (title);


--
-- Name: IDX_393de5c0cdcc3e3f7e9d06e7d4; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX "IDX_393de5c0cdcc3e3f7e9d06e7d4" ON public.candidate USING btree (slug);


--
-- Name: IDX_4d38206cce48141ddb4fa4f54f; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IDX_4d38206cce48141ddb4fa4f54f" ON public.candidate_role USING btree ("userId");


--
-- Name: IDX_55d6337cebb1b658ee64ef1cca; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IDX_55d6337cebb1b658ee64ef1cca" ON public.cert_token_closure USING btree ("ancestorCertTokenId");


--
-- Name: IDX_5c5c0c2ada704c0c54ba635fdf; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IDX_5c5c0c2ada704c0c54ba635fdf" ON public.cert_role USING btree ("userId");


--
-- Name: IDX_5c8aa0b948082342bc49d1bcfd; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IDX_5c8aa0b948082342bc49d1bcfd" ON public.certification USING btree ("userId");


--
-- Name: IDX_5fb450575c106c7c9d4cff333b; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IDX_5fb450575c106c7c9d4cff333b" ON public.certification USING btree ("certTokenId");


--
-- Name: IDX_6e413fcadaea62a6c14a7d4c20; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IDX_6e413fcadaea62a6c14a7d4c20" ON public.vote_poll_cert USING btree ("votePollId");


--
-- Name: IDX_7f3d847f7b865d053e0be87434; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IDX_7f3d847f7b865d053e0be87434" ON public.vote_poll USING btree ("publiclyVisibleAt");


--
-- Name: IDX_803462500e09a96cad5d6a74cf; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IDX_803462500e09a96cad5d6a74cf" ON public.candidate_role USING btree ("candidateId");


--
-- Name: IDX_886179b52cef2c7b39c2a6cde6; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IDX_886179b52cef2c7b39c2a6cde6" ON public.cert_role USING btree ("certTokenId");


--
-- Name: IDX_a8b8eb86e303e2896eb812a21a; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IDX_a8b8eb86e303e2896eb812a21a" ON public.vote_poll_role USING btree ("userId");


--
-- Name: IDX_b6a3557076b565c888eb23f830; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IDX_b6a3557076b565c888eb23f830" ON public.voter USING btree ("userId");


--
-- Name: IDX_bcb38cbe33a101dd35c618fcd0; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX "IDX_bcb38cbe33a101dd35c618fcd0" ON public.vote_poll USING btree (slug);


--
-- Name: IDX_c23cdd23d881e84b4a2254a987; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IDX_c23cdd23d881e84b4a2254a987" ON public.participate USING btree ("votePollId");


--
-- Name: IDX_cf10700b35c488c55cd71b3482; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IDX_cf10700b35c488c55cd71b3482" ON public.vote_poll_cert USING btree ("certTokenId");


--
-- Name: IDX_cf6dc12b2b41fa9367228f1bc9; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IDX_cf6dc12b2b41fa9367228f1bc9" ON public.cert_token_closure USING btree ("descendantCertTokenId");


--
-- Name: IDX_d75f9ea73b15c1ae6329301b3c; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IDX_d75f9ea73b15c1ae6329301b3c" ON public.voter USING btree ("votePollId");


--
-- Name: IDX_e6d56425baaade1cff9d7e8467; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX "IDX_e6d56425baaade1cff9d7e8467" ON public.participate USING btree (slug);


--
-- Name: IDX_f1c08a9e001c00f3ac259b4401; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX "IDX_f1c08a9e001c00f3ac259b4401" ON public.vote_poll_role USING btree ("votePollId");


--
-- Name: candidate FK_0e065bc1c8782a955c2d1190299; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.candidate
    ADD CONSTRAINT "FK_0e065bc1c8782a955c2d1190299" FOREIGN KEY ("createdByUserId") REFERENCES public."user"(id);


--
-- Name: participate FK_13a13c546de40c5b2cb42cdda39; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.participate
    ADD CONSTRAINT "FK_13a13c546de40c5b2cb42cdda39" FOREIGN KEY ("candidateId") REFERENCES public.candidate(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: certification FK_14a2d4ad0ef43fe1b77372d2756; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.certification
    ADD CONSTRAINT "FK_14a2d4ad0ef43fe1b77372d2756" FOREIGN KEY ("createdByUserId") REFERENCES public."user"(id) ON UPDATE CASCADE;


--
-- Name: ballot_box FK_244f55eeb6081634e9e139c9d16; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ballot_box
    ADD CONSTRAINT "FK_244f55eeb6081634e9e139c9d16" FOREIGN KEY ("votePollId") REFERENCES public.vote_poll(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: vote_poll_role FK_3a5ab8f3232215e1ae0c4e009fd; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.vote_poll_role
    ADD CONSTRAINT "FK_3a5ab8f3232215e1ae0c4e009fd" FOREIGN KEY ("createdByUserId") REFERENCES public."user"(id);


--
-- Name: participate FK_49a9bcf2718ff984f0782c79d7d; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.participate
    ADD CONSTRAINT "FK_49a9bcf2718ff984f0782c79d7d" FOREIGN KEY ("createdByUserId") REFERENCES public."user"(id);


--
-- Name: candidate_role FK_4d38206cce48141ddb4fa4f54f3; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.candidate_role
    ADD CONSTRAINT "FK_4d38206cce48141ddb4fa4f54f3" FOREIGN KEY ("userId") REFERENCES public."user"(id);


--
-- Name: user_profile FK_51cb79b5555effaf7d69ba1cff9; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_profile
    ADD CONSTRAINT "FK_51cb79b5555effaf7d69ba1cff9" FOREIGN KEY ("userId") REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: cert_token_closure FK_55d6337cebb1b658ee64ef1ccad; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cert_token_closure
    ADD CONSTRAINT "FK_55d6337cebb1b658ee64ef1ccad" FOREIGN KEY ("ancestorCertTokenId") REFERENCES public.cert_token(id) ON DELETE CASCADE;


--
-- Name: auth_token FK_5a326267f11b44c0d62526bc718; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.auth_token
    ADD CONSTRAINT "FK_5a326267f11b44c0d62526bc718" FOREIGN KEY ("userId") REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: cert_role FK_5c5c0c2ada704c0c54ba635fdfb; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cert_role
    ADD CONSTRAINT "FK_5c5c0c2ada704c0c54ba635fdfb" FOREIGN KEY ("userId") REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: certification FK_5c8aa0b948082342bc49d1bcfd9; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.certification
    ADD CONSTRAINT "FK_5c8aa0b948082342bc49d1bcfd9" FOREIGN KEY ("userId") REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: certification FK_5fb450575c106c7c9d4cff333b5; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.certification
    ADD CONSTRAINT "FK_5fb450575c106c7c9d4cff333b5" FOREIGN KEY ("certTokenId") REFERENCES public.cert_token(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: vote_poll_cert FK_6e413fcadaea62a6c14a7d4c207; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.vote_poll_cert
    ADD CONSTRAINT "FK_6e413fcadaea62a6c14a7d4c207" FOREIGN KEY ("votePollId") REFERENCES public.vote_poll(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: vote_poll_fptp FK_7368456ebd29df19d4fc909ee23; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.vote_poll_fptp
    ADD CONSTRAINT "FK_7368456ebd29df19d4fc909ee23" FOREIGN KEY ("votePollId") REFERENCES public.vote_poll(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: cert_role FK_79aee3c6cb9b6ab928e5f0199ab; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cert_role
    ADD CONSTRAINT "FK_79aee3c6cb9b6ab928e5f0199ab" FOREIGN KEY ("createdByUserId") REFERENCES public."user"(id) ON UPDATE CASCADE;


--
-- Name: candidate_role FK_803462500e09a96cad5d6a74cfd; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.candidate_role
    ADD CONSTRAINT "FK_803462500e09a96cad5d6a74cfd" FOREIGN KEY ("candidateId") REFERENCES public.candidate(id);


--
-- Name: cert_token FK_81d48d3798afdd52103470f4c11; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cert_token
    ADD CONSTRAINT "FK_81d48d3798afdd52103470f4c11" FOREIGN KEY ("createdByUserId") REFERENCES public."user"(id) ON UPDATE CASCADE;


--
-- Name: cert_role FK_886179b52cef2c7b39c2a6cde6d; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cert_role
    ADD CONSTRAINT "FK_886179b52cef2c7b39c2a6cde6d" FOREIGN KEY ("certTokenId") REFERENCES public.cert_token(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: vote_poll FK_a6ac87a4575badb9a314c66b939; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.vote_poll
    ADD CONSTRAINT "FK_a6ac87a4575badb9a314c66b939" FOREIGN KEY ("winnerCandidateId") REFERENCES public.candidate(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: vote_poll_role FK_a8b8eb86e303e2896eb812a21a7; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.vote_poll_role
    ADD CONSTRAINT "FK_a8b8eb86e303e2896eb812a21a7" FOREIGN KEY ("userId") REFERENCES public."user"(id);


--
-- Name: voter FK_b6a3557076b565c888eb23f8308; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.voter
    ADD CONSTRAINT "FK_b6a3557076b565c888eb23f8308" FOREIGN KEY ("userId") REFERENCES public."user"(id) ON UPDATE CASCADE;


--
-- Name: ballot_box FK_baac747fe223c5d28c6cf6da820; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ballot_box
    ADD CONSTRAINT "FK_baac747fe223c5d28c6cf6da820" FOREIGN KEY ("candidateId") REFERENCES public.candidate(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: vote_poll FK_c115560c44d909b25ab9f9d12cd; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.vote_poll
    ADD CONSTRAINT "FK_c115560c44d909b25ab9f9d12cd" FOREIGN KEY ("createdByUserId") REFERENCES public."user"(id);


--
-- Name: participate FK_c23cdd23d881e84b4a2254a987f; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.participate
    ADD CONSTRAINT "FK_c23cdd23d881e84b4a2254a987f" FOREIGN KEY ("votePollId") REFERENCES public.vote_poll(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: vote_poll_cert FK_cf10700b35c488c55cd71b34823; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.vote_poll_cert
    ADD CONSTRAINT "FK_cf10700b35c488c55cd71b34823" FOREIGN KEY ("certTokenId") REFERENCES public.cert_token(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: cert_token_closure FK_cf6dc12b2b41fa9367228f1bc96; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cert_token_closure
    ADD CONSTRAINT "FK_cf6dc12b2b41fa9367228f1bc96" FOREIGN KEY ("descendantCertTokenId") REFERENCES public.cert_token(id) ON DELETE CASCADE;


--
-- Name: voter FK_d75f9ea73b15c1ae6329301b3c6; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.voter
    ADD CONSTRAINT "FK_d75f9ea73b15c1ae6329301b3c6" FOREIGN KEY ("votePollId") REFERENCES public.vote_poll(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: candidate_role FK_dca81b140c22e60ae9378e41f05; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.candidate_role
    ADD CONSTRAINT "FK_dca81b140c22e60ae9378e41f05" FOREIGN KEY ("createdByUserId") REFERENCES public."user"(id);


--
-- Name: vote_poll_role FK_f1c08a9e001c00f3ac259b44014; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.vote_poll_role
    ADD CONSTRAINT "FK_f1c08a9e001c00f3ac259b44014" FOREIGN KEY ("votePollId") REFERENCES public.vote_poll(id);


--
-- PostgreSQL database dump complete
--

