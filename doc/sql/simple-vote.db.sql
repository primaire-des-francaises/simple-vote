BEGIN TRANSACTION;
DROP TABLE IF EXISTS "cert_token_closure";
CREATE TABLE IF NOT EXISTS "cert_token_closure" (
	"ancestorCertTokenId"	varchar NOT NULL,
	"descendantCertTokenId"	varchar NOT NULL,
	PRIMARY KEY("ancestorCertTokenId","descendantCertTokenId"),
	CONSTRAINT "FK_cf6dc12b2b41fa9367228f1bc96" FOREIGN KEY("descendantCertTokenId") REFERENCES "cert_token"("id") ON DELETE CASCADE ON UPDATE NO ACTION,
	CONSTRAINT "FK_55d6337cebb1b658ee64ef1ccad" FOREIGN KEY("ancestorCertTokenId") REFERENCES "cert_token"("id") ON DELETE CASCADE ON UPDATE NO ACTION
);
DROP TABLE IF EXISTS "certification";
CREATE TABLE IF NOT EXISTS "certification" (
	"userId"	varchar NOT NULL,
	"certTokenId"	varchar NOT NULL,
	"createdAt"	datetime NOT NULL DEFAULT (datetime('now')),
	"updatedAt"	datetime NOT NULL DEFAULT (datetime('now')),
	"createdByUserId"	varchar NOT NULL,
	"expiresAt"	datetime,
	PRIMARY KEY("userId","certTokenId"),
	CONSTRAINT "FK_5c8aa0b948082342bc49d1bcfd9" FOREIGN KEY("userId") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT "FK_5fb450575c106c7c9d4cff333b5" FOREIGN KEY("certTokenId") REFERENCES "cert_token"("id") ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT "FK_14a2d4ad0ef43fe1b77372d2756" FOREIGN KEY("createdByUserId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE CASCADE
);
DROP TABLE IF EXISTS "cert_role";
CREATE TABLE IF NOT EXISTS "cert_role" (
	"userId"	varchar NOT NULL,
	"certTokenId"	varchar NOT NULL,
	"createdAt"	datetime NOT NULL DEFAULT (datetime('now')),
	"updatedAt"	datetime NOT NULL DEFAULT (datetime('now')),
	"createdByUserId"	varchar NOT NULL,
	"role"	varchar NOT NULL DEFAULT ('none') CHECK(role IN ('owner','administrator','certifier','none')),
	PRIMARY KEY("userId","certTokenId"),
	CONSTRAINT "FK_5c5c0c2ada704c0c54ba635fdfb" FOREIGN KEY("userId") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT "FK_886179b52cef2c7b39c2a6cde6d" FOREIGN KEY("certTokenId") REFERENCES "cert_token"("id") ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT "FK_79aee3c6cb9b6ab928e5f0199ab" FOREIGN KEY("createdByUserId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE CASCADE
);
DROP TABLE IF EXISTS "user_profile";
CREATE TABLE IF NOT EXISTS "user_profile" (
	"createdAt"	datetime NOT NULL DEFAULT (datetime('now')),
	"updatedAt"	datetime NOT NULL DEFAULT (datetime('now')),
	"id"	varchar NOT NULL,
	"userId"	varchar NOT NULL,
	"name"	varchar NOT NULL,
	CONSTRAINT "FK_51cb79b5555effaf7d69ba1cff9" FOREIGN KEY("userId") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE CASCADE,
	PRIMARY KEY("id")
);
DROP TABLE IF EXISTS "auth_token";
CREATE TABLE IF NOT EXISTS "auth_token" (
	"createdAt"	datetime NOT NULL DEFAULT (datetime('now')),
	"updatedAt"	datetime NOT NULL DEFAULT (datetime('now')),
	"id"	varchar NOT NULL,
	"userId"	varchar NOT NULL,
	"userAgent"	text NOT NULL,
	"createdFromIp"	varchar NOT NULL,
	"expiresAt"	datetime,
	CONSTRAINT "FK_5a326267f11b44c0d62526bc718" FOREIGN KEY("userId") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE CASCADE,
	PRIMARY KEY("id")
);
DROP TABLE IF EXISTS "voter";
CREATE TABLE IF NOT EXISTS "voter" (
	"userId"	varchar NOT NULL,
	"votePollId"	varchar NOT NULL,
	PRIMARY KEY("userId","votePollId"),
	CONSTRAINT "FK_d75f9ea73b15c1ae6329301b3c6" FOREIGN KEY("votePollId") REFERENCES "vote_poll"("id") ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT "FK_b6a3557076b565c888eb23f8308" FOREIGN KEY("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE CASCADE
);
DROP TABLE IF EXISTS "vote_poll";
CREATE TABLE IF NOT EXISTS "vote_poll" (
	"createdAt"	datetime NOT NULL DEFAULT (datetime('now')),
	"updatedAt"	datetime NOT NULL DEFAULT (datetime('now')),
	"deletedAt"	datetime,
	"id"	varchar NOT NULL,
	"createdByUserId"	varchar NOT NULL,
	"winnerCandidateId"	varchar,
	"title"	varchar NOT NULL,
	"slug"	varchar NOT NULL,
	"votingModule"	varchar NOT NULL DEFAULT ('fptp') CHECK(votingModule IN ('fptp')),
	"votingModuleSetting"	text,
	"votingScheme"	varchar NOT NULL DEFAULT ('single') CHECK(votingScheme IN ('single','singleWeighed3','singleWeighed5','singleWeighed7','multiWeighed3','multiWeighed5','multiWeighed7')),
	"publiclyVisibleAt"	datetime,
	"votingStartsAt"	datetime,
	"votingEndsAt"	datetime,
	"description"	text,
	"content"	text,
	"image"	varchar,
	"minWeight"	integer,
	"maxWeight"	integer,
	"_votedCount"	integer NOT NULL DEFAULT (0),
	"votedCountIsPublic"	boolean NOT NULL DEFAULT (1),
	"ballotBoxBallotsCount"	integer,
	"ballotBoxBlankBallotsCount"	integer,
	"ballotBoxSumBallotsWeight"	integer,
	"ballotBoxAvgBallotsWeight"	integer,
	"ballotBoxMedianBallotsWeight"	integer,
	CONSTRAINT "FK_a6ac87a4575badb9a314c66b939" FOREIGN KEY("winnerCandidateId") REFERENCES "candidate"("id") ON DELETE RESTRICT ON UPDATE CASCADE,
	CONSTRAINT "FK_c115560c44d909b25ab9f9d12cd" FOREIGN KEY("createdByUserId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION,
	PRIMARY KEY("id")
);
DROP TABLE IF EXISTS "vote_poll_role";
CREATE TABLE IF NOT EXISTS "vote_poll_role" (
	"votePollId"	varchar NOT NULL,
	"userId"	varchar NOT NULL,
	"createdAt"	datetime NOT NULL DEFAULT (datetime('now')),
	"updatedAt"	datetime NOT NULL DEFAULT (datetime('now')),
	"createdByUserId"	varchar NOT NULL,
	"expiresAt"	datetime,
	"role"	varchar NOT NULL DEFAULT ('voter') CHECK(role IN ('owner','administrator','manager','editor','voter')),
	PRIMARY KEY("votePollId","userId"),
	CONSTRAINT "FK_a8b8eb86e303e2896eb812a21a7" FOREIGN KEY("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION,
	CONSTRAINT "FK_f1c08a9e001c00f3ac259b44014" FOREIGN KEY("votePollId") REFERENCES "vote_poll"("id") ON DELETE NO ACTION ON UPDATE NO ACTION,
	CONSTRAINT "FK_3a5ab8f3232215e1ae0c4e009fd" FOREIGN KEY("createdByUserId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
);
DROP TABLE IF EXISTS "candidate";
CREATE TABLE IF NOT EXISTS "candidate" (
	"createdAt"	datetime NOT NULL DEFAULT (datetime('now')),
	"updatedAt"	datetime NOT NULL DEFAULT (datetime('now')),
	"deletedAt"	datetime,
	"id"	varchar NOT NULL,
	"createdByUserId"	varchar NOT NULL,
	"slug"	varchar NOT NULL,
	"title"	varchar NOT NULL,
	"description"	text,
	"content"	text,
	"image"	varchar,
	CONSTRAINT "FK_0e065bc1c8782a955c2d1190299" FOREIGN KEY("createdByUserId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION,
	PRIMARY KEY("id")
);
DROP TABLE IF EXISTS "candidate_role";
CREATE TABLE IF NOT EXISTS "candidate_role" (
	"candidateId"	varchar NOT NULL,
	"userId"	varchar NOT NULL,
	"createdAt"	datetime NOT NULL DEFAULT (datetime('now')),
	"updatedAt"	datetime NOT NULL DEFAULT (datetime('now')),
	"createdByUserId"	varchar NOT NULL,
	"expiresAt"	datetime,
	"role"	varchar NOT NULL DEFAULT ('owner') CHECK(role IN ('owner','administrator','manager','editor')),
	PRIMARY KEY("candidateId","userId"),
	CONSTRAINT "FK_4d38206cce48141ddb4fa4f54f3" FOREIGN KEY("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION,
	CONSTRAINT "FK_803462500e09a96cad5d6a74cfd" FOREIGN KEY("candidateId") REFERENCES "candidate"("id") ON DELETE NO ACTION ON UPDATE NO ACTION,
	CONSTRAINT "FK_dca81b140c22e60ae9378e41f05" FOREIGN KEY("createdByUserId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
);
DROP TABLE IF EXISTS "ballot_box";
CREATE TABLE IF NOT EXISTS "ballot_box" (
	"id"	varchar NOT NULL,
	"votePollId"	varchar NOT NULL,
	"candidateId"	varchar NOT NULL,
	"weight"	integer,
	"createdDate"	date NOT NULL DEFAULT (datetime('now')),
	"updatedAt"	datetime,
	CONSTRAINT "FK_baac747fe223c5d28c6cf6da820" FOREIGN KEY("candidateId") REFERENCES "candidate"("id") ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT "FK_244f55eeb6081634e9e139c9d16" FOREIGN KEY("votePollId") REFERENCES "vote_poll"("id") ON DELETE CASCADE ON UPDATE CASCADE,
	PRIMARY KEY("id")
);
DROP TABLE IF EXISTS "participate";
CREATE TABLE IF NOT EXISTS "participate" (
	"votePollId"	varchar NOT NULL,
	"candidateId"	varchar NOT NULL,
	"createdAt"	datetime NOT NULL DEFAULT (datetime('now')),
	"updatedAt"	datetime NOT NULL DEFAULT (datetime('now')),
	"deletedAt"	datetime,
	"createdByUserId"	varchar NOT NULL,
	"title"	varchar NOT NULL,
	"slug"	varchar NOT NULL,
	"image"	varchar,
	"description"	text,
	"content"	text,
	"votesCount"	integer,
	"sumBallotsWeight"	integer,
	"avgBallotsWeight"	integer,
	"medianBallotsWeight"	integer,
	CONSTRAINT "FK_13a13c546de40c5b2cb42cdda39" FOREIGN KEY("candidateId") REFERENCES "candidate"("id") ON DELETE RESTRICT ON UPDATE CASCADE,
	PRIMARY KEY("votePollId","candidateId"),
	CONSTRAINT "FK_c23cdd23d881e84b4a2254a987f" FOREIGN KEY("votePollId") REFERENCES "vote_poll"("id") ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT "FK_49a9bcf2718ff984f0782c79d7d" FOREIGN KEY("createdByUserId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
);
DROP TABLE IF EXISTS "vote_poll_fptp";
CREATE TABLE IF NOT EXISTS "vote_poll_fptp" (
	"createdAt"	datetime NOT NULL DEFAULT (datetime('now')),
	"updatedAt"	datetime NOT NULL DEFAULT (datetime('now')),
	"deletedAt"	datetime,
	"votePollId"	varchar NOT NULL,
	CONSTRAINT "FK_7368456ebd29df19d4fc909ee23" FOREIGN KEY("votePollId") REFERENCES "vote_poll"("id") ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT "REL_7368456ebd29df19d4fc909ee2" UNIQUE("votePollId"),
	PRIMARY KEY("votePollId")
);
DROP TABLE IF EXISTS "cert_token";
CREATE TABLE IF NOT EXISTS "cert_token" (
	"createdAt"	datetime NOT NULL DEFAULT (datetime('now')),
	"updatedAt"	datetime NOT NULL DEFAULT (datetime('now')),
	"deletedAt"	datetime,
	"id"	varchar NOT NULL,
	"expiresAt"	datetime,
	"usableCount"	integer DEFAULT (1),
	"salt"	varchar NOT NULL,
	"isRevocable"	boolean NOT NULL DEFAULT (1),
	"isEditable"	boolean NOT NULL DEFAULT (1),
	"certificateIsRevocable"	boolean NOT NULL DEFAULT (1),
	"createdByUserId"	varchar NOT NULL,
	CONSTRAINT "FK_81d48d3798afdd52103470f4c11" FOREIGN KEY("createdByUserId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE CASCADE,
	PRIMARY KEY("id")
);
DROP TABLE IF EXISTS "vote_poll_cert";
CREATE TABLE IF NOT EXISTS "vote_poll_cert" (
	"votePollId"	varchar NOT NULL,
	"certTokenId"	varchar NOT NULL,
	"createdAt"	datetime NOT NULL DEFAULT (datetime('now')),
	"updatedAt"	datetime NOT NULL DEFAULT (datetime('now')),
	PRIMARY KEY("votePollId","certTokenId"),
	CONSTRAINT "FK_cf10700b35c488c55cd71b34823" FOREIGN KEY("certTokenId") REFERENCES "cert_token"("id") ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT "FK_6e413fcadaea62a6c14a7d4c207" FOREIGN KEY("votePollId") REFERENCES "vote_poll"("id") ON DELETE CASCADE ON UPDATE CASCADE
);
DROP TABLE IF EXISTS "user";
CREATE TABLE IF NOT EXISTS "user" (
	"createdAt"	datetime NOT NULL DEFAULT (datetime('now')),
	"updatedAt"	datetime NOT NULL DEFAULT (datetime('now')),
	"deletedAt"	datetime,
	"id"	varchar NOT NULL,
	"username"	varchar NOT NULL,
	"email"	varchar,
	"role"	varchar NOT NULL DEFAULT ('user') CHECK(role IN ('user','admin','superAdmin')),
	"isActive"	boolean NOT NULL DEFAULT (1),
	"password"	varchar NOT NULL,
	CONSTRAINT "UQ_78a916df40e02a9deb1c4b75edb" UNIQUE("username"),
	CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE("email"),
	PRIMARY KEY("id")
);
DROP INDEX IF EXISTS "IDX_cf6dc12b2b41fa9367228f1bc9";
CREATE INDEX IF NOT EXISTS "IDX_cf6dc12b2b41fa9367228f1bc9" ON "cert_token_closure" (
	"descendantCertTokenId"
);
DROP INDEX IF EXISTS "IDX_55d6337cebb1b658ee64ef1cca";
CREATE INDEX IF NOT EXISTS "IDX_55d6337cebb1b658ee64ef1cca" ON "cert_token_closure" (
	"ancestorCertTokenId"
);
DROP INDEX IF EXISTS "IDX_5c8aa0b948082342bc49d1bcfd";
CREATE INDEX IF NOT EXISTS "IDX_5c8aa0b948082342bc49d1bcfd" ON "certification" (
	"userId"
);
DROP INDEX IF EXISTS "IDX_5fb450575c106c7c9d4cff333b";
CREATE INDEX IF NOT EXISTS "IDX_5fb450575c106c7c9d4cff333b" ON "certification" (
	"certTokenId"
);
DROP INDEX IF EXISTS "IDX_5c5c0c2ada704c0c54ba635fdf";
CREATE INDEX IF NOT EXISTS "IDX_5c5c0c2ada704c0c54ba635fdf" ON "cert_role" (
	"userId"
);
DROP INDEX IF EXISTS "IDX_886179b52cef2c7b39c2a6cde6";
CREATE INDEX IF NOT EXISTS "IDX_886179b52cef2c7b39c2a6cde6" ON "cert_role" (
	"certTokenId"
);
DROP INDEX IF EXISTS "IDX_1c79aa5394045515fb4cdd50ec";
CREATE UNIQUE INDEX IF NOT EXISTS "IDX_1c79aa5394045515fb4cdd50ec" ON "user_profile" (
	"name"
);
DROP INDEX IF EXISTS "IDX_b6a3557076b565c888eb23f830";
CREATE INDEX IF NOT EXISTS "IDX_b6a3557076b565c888eb23f830" ON "voter" (
	"userId"
);
DROP INDEX IF EXISTS "IDX_d75f9ea73b15c1ae6329301b3c";
CREATE INDEX IF NOT EXISTS "IDX_d75f9ea73b15c1ae6329301b3c" ON "voter" (
	"votePollId"
);
DROP INDEX IF EXISTS "IDX_7f3d847f7b865d053e0be87434";
CREATE INDEX IF NOT EXISTS "IDX_7f3d847f7b865d053e0be87434" ON "vote_poll" (
	"publiclyVisibleAt"
);
DROP INDEX IF EXISTS "IDX_bcb38cbe33a101dd35c618fcd0";
CREATE UNIQUE INDEX IF NOT EXISTS "IDX_bcb38cbe33a101dd35c618fcd0" ON "vote_poll" (
	"slug"
);
DROP INDEX IF EXISTS "IDX_308a9c20d213d4aea362fccf1f";
CREATE UNIQUE INDEX IF NOT EXISTS "IDX_308a9c20d213d4aea362fccf1f" ON "vote_poll" (
	"title"
);
DROP INDEX IF EXISTS "IDX_a8b8eb86e303e2896eb812a21a";
CREATE INDEX IF NOT EXISTS "IDX_a8b8eb86e303e2896eb812a21a" ON "vote_poll_role" (
	"userId"
);
DROP INDEX IF EXISTS "IDX_f1c08a9e001c00f3ac259b4401";
CREATE INDEX IF NOT EXISTS "IDX_f1c08a9e001c00f3ac259b4401" ON "vote_poll_role" (
	"votePollId"
);
DROP INDEX IF EXISTS "IDX_393de5c0cdcc3e3f7e9d06e7d4";
CREATE UNIQUE INDEX IF NOT EXISTS "IDX_393de5c0cdcc3e3f7e9d06e7d4" ON "candidate" (
	"slug"
);
DROP INDEX IF EXISTS "IDX_4d38206cce48141ddb4fa4f54f";
CREATE INDEX IF NOT EXISTS "IDX_4d38206cce48141ddb4fa4f54f" ON "candidate_role" (
	"userId"
);
DROP INDEX IF EXISTS "IDX_803462500e09a96cad5d6a74cf";
CREATE INDEX IF NOT EXISTS "IDX_803462500e09a96cad5d6a74cf" ON "candidate_role" (
	"candidateId"
);
DROP INDEX IF EXISTS "IDX_13a13c546de40c5b2cb42cdda3";
CREATE INDEX IF NOT EXISTS "IDX_13a13c546de40c5b2cb42cdda3" ON "participate" (
	"candidateId"
);
DROP INDEX IF EXISTS "IDX_c23cdd23d881e84b4a2254a987";
CREATE INDEX IF NOT EXISTS "IDX_c23cdd23d881e84b4a2254a987" ON "participate" (
	"votePollId"
);
DROP INDEX IF EXISTS "IDX_e6d56425baaade1cff9d7e8467";
CREATE UNIQUE INDEX IF NOT EXISTS "IDX_e6d56425baaade1cff9d7e8467" ON "participate" (
	"slug"
);
DROP INDEX IF EXISTS "IDX_cf10700b35c488c55cd71b3482";
CREATE INDEX IF NOT EXISTS "IDX_cf10700b35c488c55cd71b3482" ON "vote_poll_cert" (
	"certTokenId"
);
DROP INDEX IF EXISTS "IDX_6e413fcadaea62a6c14a7d4c20";
CREATE INDEX IF NOT EXISTS "IDX_6e413fcadaea62a6c14a7d4c20" ON "vote_poll_cert" (
	"votePollId"
);
COMMIT;
