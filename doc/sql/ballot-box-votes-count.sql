SELECT "votePoll"."id" AS "votePoll_id",
       "votePoll"."title" AS "votePoll_title",
       "votePoll"."votingStartsAt" AS "votePoll_votingStartsAt",
       "votePoll"."votingEndsAt" AS "votePoll_votingEndsAt",
       "votePoll"."_votedCount" AS "votePoll__votedCount",
       "votePoll"."ballotBoxBallotsCount" AS "votePoll_ballotBoxBallotsCount",
       "votePoll"."ballotBoxBlankBallotsCount" AS "votePoll_ballotBoxBlankBallotsCount",
       "votePoll"."ballotBoxSumBallotsWeight" AS "votePoll_ballotBoxSumBallotsWeight",
       "votePoll"."ballotBoxAvgBallotsWeight" AS "votePoll_ballotBoxAvgBallotsWeight",
       "votePoll"."ballotBoxMedianBallotsWeight" AS "votePoll_ballotBoxMedianBallotsWeight",
       "participates"."votePollId" AS "participates_votePollId",
       "participates"."candidateId" AS "participates_candidateId",
       "participates"."votesCount" AS "participates_votesCount",
       "participates"."sumBallotsWeight" AS "participates_sumBallotsWeight",
       "participates"."avgBallotsWeight" AS "participates_avgBallotsWeight",
       "participates"."medianBallotsWeight" AS "participates_medianBallotsWeight",
       COUNT("allVotes"."votePollId") AS "votePoll_ballotBoxBallotsCount",
       COUNT("blankBallots"."votePollId") AS "votePoll_ballotBoxBlankBallotsCount",
       SUM("allVotes"."weight") AS "votePoll_ballotBoxSumBallotsWeight",
       AVG("allVotes"."weight") AS "votePoll_ballotBoxAvgBallotsWeight",
       COUNT("participates_ballotBoxes"."candidateId") AS "participates_votesCount",
       SUM("participates_ballotBoxes"."weight") AS "participates_sumBallotsWeight",
       AVG("participates_ballotBoxes"."weight") AS "participates_avgBallotsWeight"

FROM "vote_poll" "votePoll"
  INNER JOIN "participate" "participates"
    ON "participates"."votePollId"="votePoll"."id" AND ("participates"."deletedAt" IS NULL)
  LEFT JOIN "ballot_box" "blankBallots"
    ON "blankBallots"."votePollId" = "votePoll"."id" AND "blankBallots"."candidateId" IS NULL
  LEFT JOIN "ballot_box" "allVotes"
    ON "allVotes"."votePollId" = "votePoll"."id"
  LEFT JOIN "ballot_box" "participates_ballotBoxes"
    ON "participates_ballotBoxes"."candidateId"="participates"."candidateId"
      AND "participates_ballotBoxes"."votePollId"="participates"."votePollId"

WHERE ("votePoll"."id" = ?)
  AND ("votePoll"."deletedAt" IS NULL)

GROUP BY "votePoll"."id",
         "votePoll"."title",
         "votePoll"."_votedCount",
         "votePoll"."votingStartsAt",
         "votePoll"."votingEndsAt",
         "participates"."candidateId",
         "participates"."votePollId"
         